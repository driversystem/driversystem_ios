//
//  PopUpEventHelloViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/16/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class PopUpEventHelloViewController: UIViewController {
    
    @IBOutlet weak var email: UILabel! {
        didSet {
            email.text = emailOfSocket
        }
    }
    
    var socket = SocketIOManager1()
    
    @IBOutlet weak var viewContainer: UIView! {
        didSet {
            viewContainer.layer.cornerRadius = 10
        }
    }
    
//    var receiveID: String?
    
    @IBOutlet weak var subviewComtainer: UIView! {
        didSet {
            subviewComtainer.layer.cornerRadius = 10
            subviewComtainer.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            subviewComtainer.addBorder(side: .bottom, thickness: 1, color: .white)
        }
    }
    
    
    @IBOutlet weak var helloBack: UIButton! {
        didSet {
            helloBack.layer.cornerRadius = 10
        }
    }
    
    @IBOutlet weak var anotherCommunicate: UIButton! {
        didSet {
            anotherCommunicate.layer.cornerRadius = 10
        }
    }
    
    @IBAction func onHelloBack(_ sender: Any) {
        var user = loadUserFromUserDefault()
        print("nhanj khong")
        print(receiveID)
        socket.onSayHello(email: (user?.email)!, name: user!.name, receive_id: receiveID, msg: "hello")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.dismiss(animated: false)
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
