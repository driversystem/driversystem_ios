//
//  PopUpEventOtherDriverViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/16/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class PopUpEventOtherDriverViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.dismiss(animated: false)
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
