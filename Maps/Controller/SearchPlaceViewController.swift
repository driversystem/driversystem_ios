//
//  SearchPlaceViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/30/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GooglePlaces


var placeLat:Double?
var placeLng:Double?
var placeNameOfAppointment = ""
let mySentKey = "com.bobthedeveloper.notificationKey"

class SearchPlaceViewController: UIViewController, GMSAutocompleteResultsViewControllerDelegate, UISearchBarDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
            searchController?.isActive = false
            // Do something with the selected place.
//            print("Place name: \(place.name)")
//            print("Place address: \(place.formattedAddress)")
//            print("Place attributions: \(place.attributions)")
        
        let alert:UIAlertController = UIAlertController(title: "Bạn có muốn chọn nơi này làm địa điểm cuộc hẹn ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
        let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
            placeLat = place.coordinate.latitude
            placeLng = place.coordinate.longitude
            placeNameOfAppointment = place.name!
            NotificationCenter.default.post(name: Notification.Name(rawValue: mySentKey), object: self)
            self.dismiss(animated: true)
        }
        let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
            print("No")
            alert.dismiss(animated: true)
        }
        alert.addAction(btnYes)
        alert.addAction(btnNo)
        self.present(alert, animated: true, completion: nil)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
            print("Error: ", error.localizedDescription)
    }
    var placesClient : GMSPlacesClient?

    var groupID:String?

    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        placesClient?.autocompleteQuery(searchText, bounds: nil, filter: filter, callback: { (results, error) in
            if let error = error {
              print("Autocomplete error \(error)")
              return
            }
            if let results = results {
                 for result in results {
                    print(result.attributedFullText)
                 }
            }
        })
    }
    
    @IBOutlet weak var myResultView: UITableView!
    
    @IBOutlet weak var mySearchBar: UISearchBar!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
//        searchController?.searchBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        
//        let topConstraint = NSLayoutConstraint(item: searchController, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)

        
        searchController?.searchResultsUpdater = resultsViewController

        
        let subView = UIView(frame: CGRect(x: 0, y: 20.0, width: 200.0, height: 45.0))

        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        view.bringSubviewToFront(searchController!.searchBar)
//        view.addConstraint(topConstraint)

        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false

        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
        // Do any additional setup after loading the view.

        NotificationCenter.default.addObserver(self,
        selector: #selector(doWhenSendData),
        name: NSNotification.Name(rawValue: mySentKey),
        object: nil)
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: myNotificationKey), object: self)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @objc func doWhenSendData() {
    }
    
    
    @IBAction func turnback(_ sender: Any) {
        dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

