//
//  ListOfInvitationsViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ListOfInvitationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.invitation == nil) {
            return 0
        }
        else {
            return self.invitation!.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListOfInvitationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listOfInvitations") as! ListOfInvitationTableViewCell
        cell.nameOfGroup.text = invitation![indexPath.row].groupName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert:UIAlertController = UIAlertController(title: "Bạn có muốn vào nhóm này không ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
        
        let alertAfterAccept:UIAlertController = UIAlertController(title: "Đã vào nhóm, quay lại trang trước để xem", message: nil, preferredStyle: .alert)
        
        let btnYesAfterAccept:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
            DispatchQueue.main.async {
                self.invitation?.remove(at: indexPath.row)
                alertAfterAccept.dismiss(animated: true)
                self.listOfInvitations.reloadData()
            }
        }
        
        alertAfterAccept.addAction(btnYesAfterAccept)
        
        let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/invitation/accept?id="+self.invitation![indexPath.row].id
            
            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "PUT"
            let userToken = loadUserTokenFromUserDefault()
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        do {
                            let jsonData = Data(dataString.utf8)
                            let responseMessage = try JSONDecoder().decode(ResponseMessage.self, from: jsonData)
                            if(responseMessage.success == true) {
                                DispatchQueue.main.sync {
                                    self.present(alertAfterAccept, animated: true, completion: nil)
                                }
                            }
                        }
                        catch {
                            
                        }
                     }
                 }
            }
            task.resume()
            alert.dismiss(animated: true)
        }
        
        let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/invitation/deny?id="+self.invitation![indexPath.row].id
            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "PUT"
            let userToken = loadUserTokenFromUserDefault()
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let _ = String(data: data, encoding: .utf8) {
                        DispatchQueue.main.async {
                            self.invitation?.remove(at: indexPath.row)
                            self.listOfInvitations.reloadData()
                        }
                     }
                 }
            }
            task.resume()
            alert.dismiss(animated: true)
        }
        
        
        
        alert.addAction(btnYes)
        alert.addAction(btnNo)
        self.present(alert, animated: true, completion: nil)
    }
    
    var invitation:[Invitation]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listOfInvitations.delegate = self
        listOfInvitations.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllInvitation(userCompletionHandler: {int, error in
            if let int = int {
                print(int)
            }
        }) 
    }
    
    @IBOutlet weak var header: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @IBOutlet weak var listOfInvitations: UITableView! {
        didSet {
            listOfInvitations.backgroundColor = .white
        }
    }
    
    @IBAction func turnback(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func getAllInvitation(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
        
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/invitation"
                 
        let userToken = loadUserTokenFromUserDefault()
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print("123123123")
                        print(dataString)
                        let jsonData = Data(dataString.utf8)
                        let responseGetInvitation = try JSONDecoder().decode(ResponseGetInvitation.self, from: jsonData)

                        self.invitation = responseGetInvitation.invitations
//                        userCompletionHandler(self.invitation?.count,nil)
                        DispatchQueue.main.async {
                            print("jskdafnaskjdf")
                            self.listOfInvitations.reloadData()
                        }
                        
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
