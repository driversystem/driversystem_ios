//
//  DetailGroupAddAppointmentViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import FSCalendar
import GoogleMaps
import GooglePlaces
import GoogleMapsUtils
import CoreLocation


class DetailGroupAddAppointmentViewController: UIViewController {
    
    let urlCreateURL = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/appointment")!
    
    var groupID:String?
    
    var latitudeAppointment:Double?
    
    var longitudeAppointment:Double?
    
    var nameOfAppointment:String?
    
    var addressOfAppointment:String?
    
    var coordinateOfAppointment:CLLocationCoordinate2D?
    
    @IBOutlet weak var calendar: FSCalendar!
    
    @IBOutlet weak var time: UIDatePicker!
    
    @IBOutlet weak var choosePlace: UIButton! {
        didSet {
            choosePlace.layer.borderWidth = 2
            choosePlace.layer.cornerRadius = 10
            choosePlace.clipsToBounds = true

        }
    }
    @IBOutlet weak var groupTravelMode: UISwitch!
    
    @IBOutlet weak var chooseFromCollection: UIButton! {
        didSet {
            chooseFromCollection.layer.cornerRadius = 10
            chooseFromCollection.clipsToBounds = true
        }
    }
    
//    @IBOutlet weak var confirmCreate: UIButton! {
//        didSet {
//            confirmCreate.layer.cornerRadius = 10
//            confirmCreate.clipsToBounds = true
//        }
//    }
    
    @IBOutlet weak var confirmCreate: LoadingButton! {
        didSet {
            confirmCreate.layer.cornerRadius = 10
            confirmCreate.clipsToBounds = true
        }
    }
    
    
    @IBOutlet weak var cancel: UIButton! {
        didSet {
            cancel.layer.cornerRadius = 10
            cancel.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var prompt: UITextField! {
        didSet {
            prompt.layer.borderWidth = 2
            prompt.layer.cornerRadius = 5
            prompt.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(doAfterChoosePlace),
        name: NSNotification.Name(rawValue: mySentKey),
        object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var header: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @objc func doAfterChoosePlace() {
        print("I've been notified")
        print(placeLat!)
        print(placeLng!)
        print(placeNameOfAppointment)
        
        choosePlace.setTitle(placeNameOfAppointment, for: .normal)
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func createGroup(_ sender: Any) {
        confirmCreate.showLoading()
        let dateOfCalendar = calendar.selectedDate
        if(dateOfCalendar != nil) {
            let timeOfDatePicker = time.date
                    
            let year = Calendar.current.component(.year, from: dateOfCalendar!)
            let month = Calendar.current.component(.month, from: dateOfCalendar!)
            let day =  Calendar.current.component(.day, from: dateOfCalendar!)
            let hour = Calendar.current.component(.hour, from: timeOfDatePicker)
            let minute = Calendar.current.component(.minute, from: timeOfDatePicker)

            var time = DateComponents()
            time.year = year
            time.month = month
            time.day = day
            time.timeZone = TimeZone(secondsFromGMT: 7)
            time.hour = hour
            time.minute = minute
            
            var ReminderTime = DateComponents()
            ReminderTime.year = year
            ReminderTime.month = month
            ReminderTime.day = day
            ReminderTime.timeZone = TimeZone(secondsFromGMT: 7)
            ReminderTime.hour = hour - 1
            time.minute = minute
            
            let timeGroup = Calendar.current.date(from: time)!
            let reminderTime = Calendar.current.date(from: ReminderTime)!

            let timeGroupString = String(describing: timeGroup)
            let reminderTimeString = String(describing: reminderTime)
        
            let jsoncoordinate:[String:[Double]] = [
                "coordinates" : [longitudeAppointment!,latitudeAppointment!]
            ]
            
            let properties:[String:Bool] = [
                "groupTravel":groupTravelMode.isOn
            ]

            let reminderMsg: String = prompt.text!

            let appointment = [
                "groupID": groupID!,
                "time": timeGroupString,
                "reminderTime": reminderTimeString,
                "reminderMsg": reminderMsg,
                "location": jsoncoordinate,
                "locationName": nameOfAppointment,
                "locationAddress": addressOfAppointment,
                "properties": properties
            ] as [String : Any]
            var request = URLRequest(url: urlCreateURL)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]

            let dataJson = try! JSONSerialization.data(withJSONObject: appointment, options: .prettyPrinted)
            let userToken = loadUserTokenFromUserDefault()

            request.setValue(userToken, forHTTPHeaderField: "x-access-token")
            let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                if error != nil {
                    print("/error")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        do {
                            print(dataString)
                            let jsonData = Data(dataString.utf8)
                            let responseCreateAppointment = try JSONDecoder().decode(ResponseCreateAppointment.self, from: jsonData)
                            DispatchQueue.main.async {
                                if(responseCreateAppointment.success == true) {
                                    let alert:UIAlertController = UIAlertController(title: "Tạo Thành Công", message: nil, preferredStyle: .alert)
                                    let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                        alert.dismiss(animated: true)
                                        DispatchQueue.main.async {
                                            self.dismiss(animated: true)
                                        }
                                    }
                                    alert.addAction(btnYes)
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else {
                                    let alert:UIAlertController = UIAlertController(title: "Có lỗi gì đó xảy ra", message: "Vui lòng tạo lại", preferredStyle: .alert)
                                    let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                        self.confirmCreate.hideLoading()
                                        alert.dismiss(animated: true)
                                    }
                                    alert.addAction(btnYes)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        catch {
                            print("can not decode data")
                        }
                    }
                }
            }
            task.resume()
    }
        else {
            let alert:UIAlertController = UIAlertController(title: "Vui lòng điền đầy đủ thông tin", message: "", preferredStyle: .alert)
            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                self.confirmCreate.hideLoading()
                alert.dismiss(animated: true)
            }
            alert.addAction(btnYes)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func choosePlaceToMeet(_ sender: Any) {
//        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
//        let scr = storyboard.instantiateViewController(identifier: "searchPlace") as! SearchPlaceViewController
//        scr.groupID = groupID
//        scr.modalPresentationStyle = .overFullScreen
//        scr.modalTransitionStyle = .crossDissolve
//        self.present(scr, animated: true, completion: nil)
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    @IBAction func turnBack(_ sender: Any) {
        dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DetailGroupAddAppointmentViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
//    textField.text = place.name
    latitudeAppointment = place.coordinate.latitude
    longitudeAppointment = place.coordinate.longitude
    nameOfAppointment = place.name
    addressOfAppointment = place.formattedAddress
    choosePlace.titleLabel?.text = place.name
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
