//
//  DetailGroupViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/19/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import FirebaseStorage
import HMSegmentedControl


class DetailGroupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate & UINavigationControllerDelegate  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(detailGroup == nil) {
            return 0
        }
        else {
            return (detailGroup?.photos?.count)!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listOfAvatar", for: indexPath) as! DetailGroupListOfAvatarCollectionViewCell
        let urlString:String?  = self.detailGroup?.photos![indexPath.row]
        let imageUrl = URL(string: urlString!)
        do {
            let imgData:Data = try Data(contentsOf: imageUrl!)
            cell.imageOfEachPhoto.image = UIImage(data: imgData)
        }
        catch {
            print("khong load dc anh")
        }
        return cell
    }
    
    var dialogImage = UIImageView()

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentCell = collectionView.cellForItem(at: indexPath) as! DetailGroupListOfAvatarCollectionViewCell
        dialogImage = UIImageView(image: currentCell.imageOfEachPhoto.image)
        dialogImage.frame = UIScreen.main.bounds
        dialogImage.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dialogImage.contentMode = .scaleAspectFit
        dialogImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        dialogImage.addGestureRecognizer(tap)
        self.view.addSubview(dialogImage)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        dialogImage.removeFromSuperview()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (viewShowDetail.frame.width-4)/3, height: (viewShowDetail.frame.width-4)/3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(detailGroup == nil) {
            return 0
        }
        else {
            let numberOfOwner = 1
            let numberOfMember = (detailGroup?.members!.count)! as Int
            let numberOfModerator = (detailGroup?.groupModerators!.count)! as Int
            let totalOfMember = numberOfOwner + numberOfMember + numberOfModerator
            let numberOfAppointment = (detailGroup?.appointments?.count)! as Int
            return tableView == ListOfMember ? totalOfMember:numberOfAppointment
        }
//        return tableView == ListOfMember ? numberOfMember:numberOfAppointment
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.ListOfMember) {
            let cell:DetailGroupMemberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listOfMember") as! DetailGroupMemberTableViewCell
            var arrayAllMember = [detailGroup?.groupOwner!]
            arrayAllMember = arrayAllMember + (detailGroup?.groupModerators)! + (detailGroup?.members)!
            let numberOfModerator = (detailGroup?.groupModerators!.count)! as Int
            if(indexPath.row == 0) {
                if((arrayAllMember[indexPath.row]?.avatar) != nil) {
                        let urlString:String?  = arrayAllMember[indexPath.row]?.avatar
                        let imageUrl = URL(string: urlString!)
                        do {
                            let imgData:Data = try Data(contentsOf: imageUrl!)
                            cell.avatarOfMember!.image = UIImage(data: imgData)
                        }
                        catch {
                            print("khong load dc anh")
                        }
                }
                cell.nameOfMember.text = arrayAllMember[indexPath.row]?.name
                cell.roleOfMember.text = "Chủ nhóm"
                cell.phone.text = arrayAllMember[indexPath.row]?.phoneNumber
                cell.member = arrayAllMember[indexPath.row]
            }
            if(numberOfModerator == 0 && indexPath.row > 0 ) {
                if((arrayAllMember[indexPath.row]?.avatar) != nil) {
                        let urlString:String?  = arrayAllMember[indexPath.row]?.avatar
                        let imageUrl = URL(string: urlString!)
                        do {
                            let imgData:Data = try Data(contentsOf: imageUrl!)
                            cell.avatarOfMember!.image = UIImage(data: imgData)
                        }
                        catch {
                            print("khong load dc anh")
                        }
                }
                cell.nameOfMember.text = arrayAllMember[indexPath.row]?.name
                cell.roleOfMember.text = "Thành viên"
                cell.phone.text = arrayAllMember[indexPath.row]?.phoneNumber
                cell.member = arrayAllMember[indexPath.row]
            }
            else {
                if(indexPath.row > 0 && indexPath.row < numberOfModerator + 1 ) {
                    if((arrayAllMember[indexPath.row]?.avatar) != nil) {
                            let urlString:String?  = arrayAllMember[indexPath.row]?.avatar
                            let imageUrl = URL(string: urlString!)
                            do {
                                let imgData:Data = try Data(contentsOf: imageUrl!)
                                cell.avatarOfMember!.image = UIImage(data: imgData)
                            }
                            catch {
                                print("khong load dc anh")
                            }
                    }
                    cell.nameOfMember.text = arrayAllMember[indexPath.row]?.name
                    cell.roleOfMember.text = "Quản trị viên"
                    cell.phone.text = arrayAllMember[indexPath.row]?.phoneNumber
                    cell.member = arrayAllMember[indexPath.row]
                }
                if(indexPath.row >= numberOfModerator + 1) {
                    if((arrayAllMember[indexPath.row]?.avatar) != nil) {
                        let urlString:String?  = arrayAllMember[indexPath.row]?.avatar
                        let imageUrl = URL(string: urlString!)
                        do {
                            let imgData:Data = try Data(contentsOf: imageUrl!)
                            cell.avatarOfMember!.image = UIImage(data: imgData)
                        }
                        catch {
                            print("khong load dc anh")
                        }

                    }
                    cell.nameOfMember.text = arrayAllMember[indexPath.row]?.name
                    cell.roleOfMember.text = "Thành viên"
                    cell.phone.text = arrayAllMember[indexPath.row]?.phoneNumber
                    cell.member = arrayAllMember[indexPath.row]
                }
            }
            return cell
        }
        else {
            let cell:DetailGroupAppointmentsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listAppointmentOfGroup") as! DetailGroupAppointmentsTableViewCell
            
            let time = (detailGroup?.appointments?[indexPath.row].time)!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 7)
            let datetime = formatter.date(from: time)
            
            let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)

            let dayOfAppointment = String(day.day!)
            let monthOfAppointment = String(day.month!)
            let hourOfAppointment = String(day.hour!)
            let minOfAppointment = String(day.minute!)
            
            let monthOfAppointmentInt = Int(monthOfAppointment)
            
            switch monthOfAppointmentInt {
                case 1:
                    cell.month.text = "JANUARY"
                    break;
                case 2:
                    cell.month.text = "FEBRUARY"
                    break;
                case 3:
                    cell.month.text = "MARCH"
                    break;
                case 4:
                    cell.month.text = "APRIL"
                    break;
                case 5:
                    cell.month.text = "MAY"
                    break;
                case 6:
                    cell.month.text = "JUNE"
                    break;
                case 7:
                    cell.month.text = "JULY"
                    break;
                case 8:
                    cell.month.text = "AUGUST"
                    break;
                case 9:
                    cell.month.text = "SEPTEMBER"
                    break;
                case 10:
                    cell.month.text = "OCTOBER"
                    break;
                case 11:
                    cell.month.text = "NOVEMBER"
                    break;
                default:
                    cell.month.text = "DECEMBER"
                    break;
            }
            switch day.weekday {
                case 1:
                    cell.dayOfWeek.text = "SUNDAY"
                    break;
                case 2:
                    cell.dayOfWeek.text = "MONDAY"
                    break;
                case 3:
                    cell.dayOfWeek.text = "TUESDAY"
                    break;
                case 4:
                    cell.dayOfWeek.text = "WEDNESDAY"
                    break;
                case 5:
                    cell.dayOfWeek.text = "THURSDAY"
                    break;
                case 6:
                    cell.dayOfWeek.text = "FRIDAY"
                    break;
                default:
                    cell.dayOfWeek.text = "SATURDAY"
                    break;
            }
            
            let now = NSDate()
            let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
            
            var nowTime = DateComponents()
            nowTime.year = nowD.year
            nowTime.month = nowD.month
            nowTime.day = nowD.day
            nowTime.timeZone = TimeZone(secondsFromGMT: 7)
            nowTime.hour = nowD.hour
            nowTime.minute = nowD.minute
            let nowTimeDate = Calendar.current.date(from: nowTime)!
            
            var appointTime = DateComponents()
            appointTime.year = day.year
            appointTime.month = day.month
            appointTime.day = day.day
            appointTime.timeZone = TimeZone(secondsFromGMT: 7)
            appointTime.hour = day.hour
            appointTime.minute = day.minute
            let timeApp = Calendar.current.date(from: appointTime)!
            
            let distanceTime = nowTimeDate.distance(to: timeApp)
            if(distanceTime<0) {
                cell.timeCooldown.text = "Đã quá hạn"
            }
            else {
                let distanceDay = appointTime.day! - nowTime.day!
                if(distanceDay > 0) {
                    cell.timeCooldown.text = "Còn "+String(distanceDay)+" ngày"
                }
                else {
                    let distanceHour = appointTime.hour! - nowTime.hour!
                    if(distanceHour > 0) {
                        cell.timeCooldown.text = "Còn "+String(distanceHour)+" giờ"
                    }
                    else {
                        let distanceMin = appointTime.minute! - nowTime.minute!
                        cell.timeCooldown.text = "Còn "+String(distanceMin)+" phút"
                    }
                }
            }
            
            cell.locationName.text = detailGroup?.appointments?[indexPath.row].locationName
            print(detailGroup?.appointments?[indexPath.row].locationName)
            cell.placeAppointment.text = detailGroup?.appointments?[indexPath.row].locationAddress
            print(detailGroup?.appointments?[indexPath.row].locationAddress)
            cell.date.text = dayOfAppointment
            cell.time.text = hourOfAppointment+":"+minOfAppointment
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if(tableView == self.ListOfMember) {
            let user = loadUserFromUserDefault()
            let userID = user?.id
            let role = self.detailGroup?.members?.contains(where: { (element) -> Bool in
                if case userID = element.id {
                    return true
                }
                else {
                    return false
                }
            })
            
            if(role == false) {
                    let currentCell = tableView.cellForRow(at: indexPath) as! DetailGroupMemberTableViewCell
                    if(currentCell.roleOfMember.text == "Thành viên") {
                        let moderator = UIContextualAction(style: .normal, title: "Add-moderator") { (action, view, nil) in
                            let alert:UIAlertController = UIAlertController(title: "Bạn có muốn chỉ định người này làm quản trị viên ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
                            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/moderator/add?id="+currentCell.member!.id
                                
                                let url = URL(string: subUrl)
                                var request = URLRequest(url: url!)
                                request.httpMethod = "PUT"
                                let userToken = loadUserTokenFromUserDefault()
                                request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                                let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                                    if error != nil {
                                        print("/error")
                                    }
                                    else {
                                        if let response = response as? HTTPURLResponse {
                                            print("statusCode: \(response.statusCode)")
                                        }
                                        if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                            print(dataString)
                                            let numberOfModerator = (self.detailGroup?.groupModerators!.count)! as Int
                                            let currentMem = self.detailGroup?.members![indexPath.row - numberOfModerator - 1]
                                            self.detailGroup?.groupModerators?.append(currentMem!)
                                            self.detailGroup?.members!.remove(at: indexPath.row - numberOfModerator - 1)

                                                DispatchQueue.main.async {
                                                    self.ListOfMember.reloadData()
                                                }
                                         }
                                     }
                                }
                                task.resume()
                            }
                            let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                                print("Không xóa")
                                alert.dismiss(animated: true)
                            }
                            alert.addAction(btnYes)
                            alert.addAction(btnNo)
                            self.present(alert, animated: true, completion: nil)
                        }
                        return UISwipeActionsConfiguration(actions: [moderator])
                    }
            }
            else {
                return UISwipeActionsConfiguration()
            }
        }
        else {
            return UISwipeActionsConfiguration()
        }
        
        return UISwipeActionsConfiguration()
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if(tableView == self.ListOfMember) {
            let user = loadUserFromUserDefault()
                   let userID = user?.id
                   let role = self.detailGroup?.members?.contains(where: { (element) -> Bool in
                       if case userID = element.id {
                           return true
                       }
                       else {
                           return false
                       }
                   })
                   
                   if(role == false ) {
                       if tableView == self.ListOfMember {
                           let currentCell = tableView.cellForRow(at: indexPath) as! DetailGroupMemberTableViewCell
                           
                           let roleMod = self.detailGroup?.groupModerators?.contains(where: { (element) -> Bool in
                               if case userID = element.id {
                                   return true
                               }
                               else {
                                   return false
                               }
                           })
                           
                           if(currentCell.roleOfMember.text=="Thành viên") {
                               let numberOfModerator = (detailGroup?.groupModerators!.count)! as Int
                               let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view, nil) in
                                   let alert:UIAlertController = UIAlertController(title: "Bạn có muốn xóa người này khỏi nhóm ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
                                   let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                       let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/member/remove?&id="+currentCell.member!.id
                                       
                                       let url = URL(string: subUrl)
                                       var request = URLRequest(url: url!)
                                       request.httpMethod = "PUT"
                                       let userToken = loadUserTokenFromUserDefault()
                                       request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                                       let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                                           if error != nil {
                                               print("/error")
                                               print("co loi gi khong ")
                                           }
                                           else {
                                               if let response = response as? HTTPURLResponse {
                                                   print("statusCode: \(response.statusCode)")
                                               }
                                               if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                   print("hello")
                                                   print(dataString)
                                                   self.detailGroup?.members?.remove(at: indexPath.row - 1 - numberOfModerator)
                                                   DispatchQueue.main.async {
                                                       self.ListOfMember.reloadData()
                                                   }
                                                }
                                            }
                                       }
                                       task.resume()
                                   }
                                   let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                                       alert.dismiss(animated: true)
                                   }
                                   alert.addAction(btnYes)
                                   alert.addAction(btnNo)
                                   self.present(alert, animated: true, completion: nil)
                               }
                               return UISwipeActionsConfiguration(actions: [delete])
                           }

                           if (roleMod == false) {
                               if(currentCell.roleOfMember.text=="Quản trị viên") {
                                   let deleteModerator = UIContextualAction(style: .normal, title: "Delete Moderator") { (action, view, nil) in
                                       let alert:UIAlertController = UIAlertController(title: "Bạn có muốn xóa người này khỏi nhóm ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
                                       let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                           let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/moderator/remove?&id="+currentCell.member!.id
                                                            
                                            let url = URL(string: subUrl)
                                            var request = URLRequest(url: url!)
                                            request.httpMethod = "PUT"
                                            let userToken = loadUserTokenFromUserDefault()
                                            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                                            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                                                if error != nil {
                                                    print("/error")
                                                    print("co loi gi khong ")
                                                }
                                                else {
                                                    if let response = response as? HTTPURLResponse {
                                                        print("statusCode: \(response.statusCode)")
                                                    }
                                                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                        print("hello")
                                                        print(dataString)
                                                        self.detailGroup?.groupModerators?.remove(at: indexPath.row - 1)
                                                        DispatchQueue.main.async {
                                                            self.ListOfMember.reloadData()
                                                        }
                                                     }
                                                 }
                                            }
                                            task.resume()
                                       }
                                       let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                                           alert.dismiss(animated: true)
                                       }
                                       alert.addAction(btnYes)
                                       alert.addAction(btnNo)
                                       self.present(alert, animated: true, completion: nil)
                                   }
                                   return UISwipeActionsConfiguration(actions: [deleteModerator])

                               }
                           }
                           else {
                               return UISwipeActionsConfiguration()
                           }
                           
                           
                       }
                   }
                   else {
                       return UISwipeActionsConfiguration()
                   }
                   
        }
        else {
            
                let user = loadUserFromUserDefault()
                let userID = user?.id
                let role = self.detailGroup?.members?.contains(where: { (element) -> Bool in
                    if case userID = element.id {
                        return true
                    }
                    else {
                        return false
                    }
                })
                if(role==false) {
                    let deleteAppointment = UIContextualAction(style: .normal, title: "remove") { (action, view, nil) in
                        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/appointment/"+(self.detailGroup?.appointments?[indexPath.row].id)!
                        let url = URL(string: subUrl)
                        var request = URLRequest(url: url!)
                        request.httpMethod = "DELETE"
                        let userToken = loadUserTokenFromUserDefault()
                        request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                            if error != nil {
                                print("/error")
                            }
                            else {
                                if let response = response as? HTTPURLResponse {
                                    print("statusCode: \(response.statusCode)")
                                }
                                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                    print(dataString)
                                    self.detailGroup?.appointments?.remove(at: indexPath.row)
                                    DispatchQueue.main.async {
                                        self.ListOfMember.reloadData()
                                        self.ListOfAppointment.reloadData()
                                    }
                                 }
                             }
                        }
                        task.resume()
                    }
                    return UISwipeActionsConfiguration(actions: [deleteAppointment])

                }
                else {
                    return UISwipeActionsConfiguration()
                }
        }
        
        return UISwipeActionsConfiguration()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.ListOfAppointment {
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "detailAppointment") as! DetailAppointmentViewController
            scr.appointmentID = self.detailGroup?.appointments?[indexPath.row].id
            scr.nameOfGroup = self.detailGroup?.groupName
            scr.groupID = self.detailGroupId
            scr.modalPresentationStyle = .fullScreen
            scr.modalTransitionStyle = .crossDissolve
            self.present(scr, animated: true, completion: nil)
        }
        else {
            let currentCell = tableView.cellForRow(at: indexPath) as! DetailGroupMemberTableViewCell
            if(currentCell.member?.phoneNumber != nil) {
                guard let number = URL(string: "tel://" + (currentCell.member?.phoneNumber)!) else { return }
                UIApplication.shared.open(number)
            }
        }
    }
    
    
    @IBOutlet weak var ListOfMember: UITableView! {
        didSet {
            ListOfMember.backgroundColor? = .white
        }
    }
        
    @IBOutlet weak var ListOfAvatar: UICollectionView! {
        didSet {
            ListOfAvatar.backgroundColor? = .white
        }
    }
    
    @IBOutlet weak var ListOfAppointment: UITableView! {
        didSet {
            ListOfAppointment.backgroundColor? = .white
        }
    }
    
//    @IBOutlet weak var ChosedOption: UISegmentedControl!
    @IBOutlet weak var ChosedOption: HMSegmentedControl! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)

            ChosedOption.sectionTitles = ["Thành viên","Ảnh","Lịch hẹn"]
            ChosedOption.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.gray]
            ChosedOption.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor : colorBorder]
            
            ChosedOption.selectionStyle = .fullWidthStripe
            ChosedOption.selectionIndicatorLocation = .top
            
//            selectGroupOrAppointment.isVerticalDividerEnabled = true
//            selectGroupOrAppointment.verticalDividerWidth = 2
//            selectGroupOrAppointment.verticalDividerColor = colorBorder
            
    //        sc.backgroundColor = UIColor.white
            ChosedOption.selectionIndicatorColor = colorBorder
        }
    }
    
    @IBOutlet weak var viewShowDetail: UIView!
    
    @IBOutlet weak var bannerView: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            bannerView.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    
    @IBOutlet weak var groupName: UILabel! {
        didSet {
        }
    }
    
    var detailGroup:Group?
    
    var detailGroupId:String?
    
    let pickerAvatarGroup = UIImagePickerController()
    
    let pickerPhotoGroup = UIImagePickerController()
    
    @IBAction func chose(_ sender: Any) {
        switch ChosedOption.selectedSegmentIndex {
        case 0:
            iconAdd.image = UIImage(named: "addMember")
            ListOfMember.isHidden = false
            ListOfAvatar.isHidden = true
            ListOfAppointment.isHidden = true
            break;
        case 1:
            iconAdd.image = UIImage(named: "addImage")
            ListOfMember.isHidden = true
            ListOfAvatar.isHidden = false
            ListOfAppointment.isHidden = true
            break;
        default:
            iconAdd.image = UIImage(named: "addAppointment")
            ListOfMember.isHidden = true
            ListOfAvatar.isHidden = true
            ListOfAppointment.isHidden = false
            break;
        }
    }
    //    @IBAction func choose(_ sender: UISegmentedControl) {
//        switch ChosedOption.selectedSegmentIndex {
//        case 0:
//            iconAdd.image = UIImage(named: "addMember")
//            ListOfMember.isHidden = false
//            ListOfAvatar.isHidden = true
//            ListOfAppointment.isHidden = true
//            break;
//        case 1:
//            iconAdd.image = UIImage(named: "addImage")
//            ListOfMember.isHidden = true
//            ListOfAvatar.isHidden = false
//            ListOfAppointment.isHidden = true
//            break;
//        default:
//            iconAdd.image = UIImage(named: "addAppointment")
//            ListOfMember.isHidden = true
//            ListOfAvatar.isHidden = true
//            ListOfAppointment.isHidden = false
//            break;
//        }
//    }
    
    @IBOutlet weak var groupAvatar: UIImageView! {
        didSet {
            groupAvatar.layer.cornerRadius = groupAvatar.frame.size.width/2
            groupAvatar.clipsToBounds = true

        }
    }
    
    @IBOutlet weak var groupDescription: UITextView!
    
    @IBOutlet weak var btnChangeGroupAvatar: UIButton! {
        didSet {
            btnChangeGroupAvatar.layer.cornerRadius = 5
            btnChangeGroupAvatar.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnRemoveOrDelete: UIButton! {
        didSet {
            btnRemoveOrDelete.layer.cornerRadius = 5
            btnRemoveOrDelete.clipsToBounds = true
        }
    }
    
    var pending =  UIAlertController()

    @IBOutlet weak var iconAdd: UIImageView! {
        didSet {
            iconAdd.layer.cornerRadius = iconAdd.layer.frame.height/2
            iconAdd.clipsToBounds = true
            iconAdd.layer.borderWidth = 1
        }
    }
    

    private let refreshControlListOfMember = UIRefreshControl()
    
    private let refreshControlListOfAppointment = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        ListOfMember.delegate = self
        ListOfMember.dataSource = self
        ListOfAvatar.dataSource = self
        ListOfAvatar.delegate = self
        ListOfAppointment.dataSource = self
        ListOfAppointment.delegate = self
        
        ListOfMember.refreshControl = refreshControlListOfMember
        ListOfAppointment.refreshControl = refreshControlListOfAppointment
        
        self.refreshControlListOfMember.addTarget(self, action: #selector(updateDataMember), for: .valueChanged)
        self.refreshControlListOfAppointment.addTarget(self, action: #selector(updateDataAppointment), for: .valueChanged)

        activityIndicator()
        indicator.startAnimating()
        indicator.backgroundColor = .white
        
        // Do any additional setup after loading the view.
    }
    
    var indicator = UIActivityIndicatorView()
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }

    override func viewWillAppear(_ animated: Bool) {        
        getDetailOfGroup(userCompletionHandler: {int, error in
                   if let int = int {
                       print(int)
                   }
        })
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @objc private func updateDataMember() {
        getDetailOfGroup(userCompletionHandler: {int, error in
        })
        self.refreshControlListOfMember.endRefreshing()
    }
    
    @objc private func updateDataAppointment() {
        getDetailOfGroup(userCompletionHandler: {int, error in
        })
        self.refreshControlListOfAppointment.endRefreshing()
    }
    
    @IBAction func turnBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButton(_ sender: Any) {
        switch ChosedOption.selectedSegmentIndex {
        case 0:
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "detailgroupAddMember") as! DetailGroupAddMemberViewController
            scr.groupID = detailGroupId
            scr.modalPresentationStyle = .fullScreen
            scr.modalTransitionStyle = .crossDissolve
            self.present(scr, animated: true, completion: nil)
            break;
        case 1:
            print("Anh")
            let alert:UIAlertController = UIAlertController(title: "Chọn ?", message: "", preferredStyle: .actionSheet)
            let btnTakePhoto:UIAlertAction = UIAlertAction(title: "Chụp ảnh", style: .destructive) { (btn) in
                self.pickerPhotoGroup.allowsEditing = true
                self.pickerPhotoGroup.delegate = self
                self.pickerPhotoGroup.sourceType = .camera
                self.present(self.pickerPhotoGroup, animated: true)
            }
            let btnChooseFromLibrary:UIAlertAction = UIAlertAction(title: "Bộ sưu tập", style: .destructive) { (btn) in
                self.pickerPhotoGroup.allowsEditing = true
                self.pickerPhotoGroup.delegate = self
                self.pickerPhotoGroup.sourceType = .photoLibrary
                self.present(self.pickerPhotoGroup, animated: true)
            }
            let btnHuy:UIAlertAction = UIAlertAction(title: "Hủy", style: .destructive) { (btn) in
                alert.dismiss(animated: true)
            }
            
            alert.addAction(btnTakePhoto)
            alert.addAction(btnChooseFromLibrary)
            alert.addAction(btnHuy)

            present(alert, animated: true, completion: nil)
            break;
        default:
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "addAppointment") as! DetailGroupAddAppointmentViewController
            scr.groupID = detailGroupId
            scr.modalPresentationStyle = .fullScreen
            scr.modalTransitionStyle = .crossDissolve
            self.present(scr, animated: true, completion: nil)
            break;
        }
    }
    
    func getDetailOfGroup(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!
        
        let userToken = loadUserTokenFromUserDefault()
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print("-----------------")
                        print(dataString)
                        print("-----------------")
                        let jsonData = Data(dataString.utf8)
                                                
                        let responseGroup = try JSONDecoder().decode(ResponseGetDetailGroupSpecified.self, from: jsonData)
                        self.detailGroup = responseGroup.group
                    
                        DispatchQueue.main.async {
                            let user = loadUserFromUserDefault()
                            let userID = user?.id
                            self.groupName.text = self.detailGroup?.groupName
                            self.groupDescription.text = self.detailGroup?.description
                            if(userID == self.detailGroup?.groupOwner?.id) {
                                self.btnRemoveOrDelete.setTitle("Xóa nhóm", for: .normal)
                            }
                            else {
                                self.btnRemoveOrDelete.setTitle("Rời nhóm", for: .normal)
                            }
                            self.ListOfMember.reloadData()
                            self.ListOfAppointment.reloadData()
//                            print(self.detailGroup?.avatar)
                            let urlString:String?  = self.detailGroup?.avatar
                            if(urlString != nil) {
                                let imageUrl = URL(string: urlString!)
                                if(imageUrl != nil) {
                                    do {
                                        let imgData:Data = try Data(contentsOf: imageUrl!)
                                        self.groupAvatar.image = UIImage(data: imgData)
                                    }
                                    catch {
                                        print("khong load dc anh")
                                    }
                                }
                            }
                            
                            self.indicator.stopAnimating()
                            self.indicator.hidesWhenStopped = true

                            
                            self.ListOfAvatar.reloadData()
                        }
                        
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }
    
    @IBAction func removeOrDelete(_ sender: Any) {
        if(btnRemoveOrDelete.titleLabel?.text == "Rời nhóm") {
            
            let alert:UIAlertController = UIAlertController(title: "Bạn có muốn rời nhóm ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/leave"
                let userToken = loadUserTokenFromUserDefault()
                let url = URL(string: subUrl)
                var request = URLRequest(url: url!)
                request.httpMethod = "POST"
                request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        print(dataString)
                        DispatchQueue.main.async {
                            alert.dismiss(animated: true)
                            self.dismiss(animated: true)
                        }
                     }
                 }
                }
                task.resume()
            }
            let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                alert.dismiss(animated: true)
            }
            alert.addAction(btnYes)
            alert.addAction(btnNo)
            self.present(alert, animated: true, completion: nil)

        }
        else {
            let alert:UIAlertController = UIAlertController(title: "Bạn có muốn xóa nhóm ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!
                let userToken = loadUserTokenFromUserDefault()
                let url = URL(string: subUrl)
                var request = URLRequest(url: url!)
                request.httpMethod = "DELETE"
                request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                    if error != nil {
                        print("/error")
                        print("co loi gi khong ")
                    }
                    else {
                        if let response = response as? HTTPURLResponse {
                            print("statusCode: \(response.statusCode)")
                        }
                        if let data = data, let dataString = String(data: data, encoding: .utf8) {
                            do {
                                let jsonData = Data(dataString.utf8)
                                let responseMessage = try JSONDecoder().decode(ResponseMessage.self, from: jsonData)
                                if (responseMessage.success == true) {
                                    DispatchQueue.main.sync {
                                        alert.dismiss(animated: true)
                                        self.dismiss(animated: true)
                                    }
                                }
                                else {
                                    DispatchQueue.main.sync {
                                        alert.dismiss(animated: true)
                                        self.dismiss(animated: true)
                                    }
                                }
                                
                            }
                            catch {
                                print("can not decode data ")
                            }
                         }
                     }
                }
                task.resume()
            }
            let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                alert.dismiss(animated: true)
            }
            alert.addAction(btnYes)
            alert.addAction(btnNo)
            self.present(alert, animated: true, completion: nil)
        }
    }
        
    @IBAction func changeAvatarGroup(_ sender: Any) {
        let user = loadUserFromUserDefault()
        let role = self.detailGroup?.members?.contains(where: { (element) -> Bool in
            if case user?.id = element.id {
                return true
            }
            else {
                return false
            }
        })
        if(role == true) {
           let alert:UIAlertController = UIAlertController(title: "Bạn không có quyền thay đổi ảnh nhóm ?", message: "Cảm ơn", preferredStyle: .alert)
            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                alert.dismiss(animated: true)
            }
            alert.addAction(btnYes)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            pickerAvatarGroup.allowsEditing = true
            pickerAvatarGroup.delegate = self
            present(pickerAvatarGroup, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }

        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)

        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            try? jpegData.write(to: imagePath)
        }

        let imageFile = image.jpegData(compressionQuality: 0.8)

        if(picker == pickerAvatarGroup) {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let imagePathOnFirebase = "images/Group/GroupAvatar/"+detailGroupId!
            let riversRef = storageRef.child(imagePathOnFirebase)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            _ = riversRef.putData(imageFile!, metadata: metadata) { (metadata, error) in
                guard let metadata = metadata else {
                  // Uh-oh, an error occurred!
                  return
                }
                // Metadata contains file metadata such as size, content-type.
                _ = metadata.size
                // You can also access to download URL after upload.
                riversRef.downloadURL { (url, error) in
                    guard url != nil else {
                    // Uh-oh, an error occurred!
                    return
                    }
                    let imageUrl = url
                    print(url!)
                    do {
                        let imageData:Data = try Data(contentsOf: imageUrl!)
                        self.groupAvatar.image = UIImage(data: imageData)
                    }
                    catch {
                        print("khong load dc anh")
                    }
                        
                    let urlString = url!.absoluteString
//                    urlString = urlString.stringByAddingPercentEncodingForRFC3986()!
                    print(urlString)
                    let jsonData:[String:String] = [
                         "avatar" : urlString
                     ]
                    let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
                    
                    let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/profile/picture"
                    print(subUrl)
                    let userToken = loadUserTokenFromUserDefault()
                    let url1 = URL(string: subUrl)
                    var request = URLRequest(url: url1!)
                    request.httpMethod = "POST"
                    request.setValue(userToken, forHTTPHeaderField: "x-access-token")
                    request.allHTTPHeaderFields = [
                       "Content-Type": "application/json",
                       "Accept": "application/json"
                    ]

                    let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                        if error != nil {
                            print("/error")
                            print("co loi gi khong ")
                        }
                        else {
                            if let response = response as? HTTPURLResponse {
                                print("statusCode: \(response.statusCode)")
                            }
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    print("up anh dc chua")
                                    print(dataString)

                                }
                                catch {
                                     print("can not decode data")
                                }
                             }
                         }
                    }
                    task.resume()
                }
            }
        }
        else {
            
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let imagePathOnFirebase = "images/Group/GroupPhotos/"+detailGroupId!+"/"+imageName
            let riversRef = storageRef.child(imagePathOnFirebase)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            _ = riversRef.putData(imageFile!, metadata: metadata) { (metadata, error) in
                guard let metadata = metadata else {
                  // Uh-oh, an error occurred!
                  return
                }
                // Metadata contains file metadata such as size, content-type.
                _ = metadata.size
                // You can also access to download URL after upload.
                riversRef.downloadURL { (url, error) in
                    guard url != nil else {
                    // Uh-oh, an error occurred!
                    return
                    }
                        
                    var urlString = url!.absoluteString
//                    urlString = urlString.stringByAddingPercentEncodingForRFC3986()!
                    print(urlString)

                    let jsonData:[String:String] = [
                        "photos" : urlString
                    ]
                    let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
                    let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.detailGroupId!+"/photo/add"
                    let userToken = loadUserTokenFromUserDefault()
                    let url1 = URL(string: subUrl)
                    var request = URLRequest(url: url1!)
                    request.httpMethod = "PUT"
                    request.allHTTPHeaderFields = [
                       "Content-Type": "application/json",
                       "Accept": "application/json"
                   ]
                    request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                    let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                        if error != nil {
                            print("/error")
                            print("co loi gi khong ")
                        }
                        else {
                            if let response = response as? HTTPURLResponse {
                                print("statusCode: \(response.statusCode)")
                            }
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    print("up anh dc chua")
                                    print(dataString)

                                }
                                catch {
                                     print("can not decode data")
                                }
                             }
                         }
                    }
                    task.resume()
                }
            }
        }
//          guard let image = info[.editedImage] as? UIImage else { return }
//
//          let imageName = UUID().uuidString
//          let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
//
//          if let jpegData = image.jpegData(compressionQuality: 0.8) {
//              try? jpegData.write(to: imagePath)
//          }
//
//          let imageFile = image.jpegData(compressionQuality: 0.8)
//
//
//
//
//            }
//          }
          dismiss(animated: true)
      }
      
      func getDocumentsDirectory() -> URL {
          let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
          return paths[0]
      }
        
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
