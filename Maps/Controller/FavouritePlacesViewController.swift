//
//  FavouritePlacesViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/18/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class FavouritePlacesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var containerPopup: UIView! {
        didSet {
            containerPopup.addBorder(side: .bottom, thickness: 1, color: .green)
            containerPopup.addBorder(side: .top, thickness: 1, color: .green)
            containerPopup.addBorder(side: .left, thickness: 1, color: .green)
            containerPopup.addBorder(side: .right, thickness: 1, color: .green)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
