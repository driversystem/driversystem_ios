//
//  RegisterViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var RegisterEmail: UITextField! {
        didSet {
            RegisterEmail.tintColor = UIColor.lightGray
//            RegisterEmail.setIcon(#imageLiteral(resourceName: "mail"))
//            RegisterEmail.layer.cornerRadius = 0.5
            RegisterEmail.layer.cornerRadius = RegisterEmail.frame.size.height/2
            RegisterEmail.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var RegisterPassword: UITextField! {
        didSet {
            RegisterPassword.tintColor = UIColor.lightGray
            RegisterPassword.layer.cornerRadius = RegisterPassword.frame.size.height/2
            RegisterPassword.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var RegisterFullname: UITextField! {
        didSet {
            RegisterFullname.tintColor = UIColor.lightGray
            RegisterFullname.layer.cornerRadius = RegisterFullname.frame.size.height/2
            RegisterFullname.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnRegister: UIButton! {
        didSet {
            btnRegister.layer.cornerRadius = btnRegister.frame.size.height/2
            btnRegister.clipsToBounds = true
        }
    }
    @IBOutlet weak var RegisterPhoneNumber: UITextField! {
        didSet {
            RegisterPhoneNumber.tintColor = UIColor.lightGray
            RegisterPhoneNumber.layer.cornerRadius = RegisterPhoneNumber.frame.size.height/2
            RegisterPhoneNumber.clipsToBounds = true
        }
    }
    
    let url = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/register/email/validate")!
//    https://driversystemapp.herokuapp.com/api-user/v1/group
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        let emailInput: String  = RegisterEmail.text!
        let passwordInput: String = RegisterPassword.text!
        let fullnameInput: String = RegisterFullname.text!
        let phoneInput: String = RegisterPhoneNumber.text!
        let date = "1996-01-01T00:00:00.000Z"
        
        let registerUser = "email="+emailInput+"&password="+passwordInput+"&name="+fullnameInput+"&birthDate="+date+"&phoneNumber="+phoneInput
    
        var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = registerUser.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        do {
                            print(dataString)
                            let jsonData = Data(dataString.utf8)
                            let user = try JSONDecoder().decode(User.self, from: jsonData)
                            
                            saveUserToUserDefault(User: user)
//                            saveUserTokenToUserDefault(token: user)
                            
                            DispatchQueue.main.async {
                                let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
                                let scr = storyboard.instantiateViewController(identifier: "loginScreen") as! LoginViewController
                                scr.modalPresentationStyle = .fullScreen
                                scr.modalTransitionStyle = .crossDissolve
                                self.present(scr, animated: true, completion: nil)
                            }
                        }
                        catch {
                            
                        }
                    }
                }
            }
            task.resume()
        
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
