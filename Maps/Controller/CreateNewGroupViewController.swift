//
//  CreateNewGroupViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 5/25/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import FirebaseStorage

class CreateNewGroupViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

//    @IBOutlet weak var nameGroup: UITextField!
    let urlCreateURL = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/group")!
    
    @IBOutlet weak var groupName: UITextField! {
        didSet {
            groupName.layer.cornerRadius = 20
//            groupName.layer.borderWidth = 2
//            groupName.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @IBOutlet weak var groupDescription: UITextField! {
        didSet {
            groupDescription.layer.cornerRadius = 20
//            groupDescription.clipsToBounds = true
//                groupDescription.layer.borderWidth = 2
//                groupDescription.layer.borderColor = UIColor.black.cgColor
        }
    }
        
//    @IBOutlet weak var btnCreate: UIButton! {
//        didSet {
//            btnCreate.layer.cornerRadius = btnCreate.frame.size.height/2
//            btnCreate.clipsToBounds = true
//        }
//    }
    
    @IBOutlet weak var btnCreate: LoadingButton! {
        didSet {
            btnCreate.layer.cornerRadius = btnCreate.frame.size.height/2
            btnCreate.clipsToBounds = true
        }
    }
    
    var urlImage:String!
    
    @IBOutlet weak var header: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    
    let url = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/group")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CreateNewGroupViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createGroup(_ sender: Any) {
        let nameGroup: String  = groupName.text!
        let description: String = groupDescription.text!
        btnCreate.showLoading()
        print(nameGroup)
        print(description)
        let group = [
            "groupName": nameGroup,
            "description": description,
            "avatar": "https://firebasestorage.googleapis.com/v0/b/super-map-4556d.appspot.com/o/images%2FGroup%2Fic_avatar.jpg?alt=media&token=786b54a4-4454-4a5b-96a2-6dc6ac36cfa8"
            ] as [String : Any]
        var request = URLRequest(url: urlCreateURL)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let dataJson = try! JSONSerialization.data(withJSONObject: group, options: .prettyPrinted)
        let userToken = loadUserTokenFromUserDefault()
        
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        let jsonData = Data(dataString.utf8)
                        let responseCreateGroup = try JSONDecoder().decode(ResponseCreateGroup.self, from: jsonData)
                        DispatchQueue.main.async {
                            if(responseCreateGroup.success == true) {
                                let alert:UIAlertController = UIAlertController(title: "Tạo Thành Công", message: nil, preferredStyle: .alert)
                                let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                    alert.dismiss(animated: true)
                                    DispatchQueue.main.async {
                                        self.dismiss(animated: true)
                                    }
                                }
                                alert.addAction(btnYes)
                                self.present(alert, animated: true, completion: nil)
                            }
                            else {
                                let alert:UIAlertController = UIAlertController(title: "Có lỗi gì đó xảy ra", message: "Vui lòng tạo lại", preferredStyle: .alert)
                                let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                    self.btnCreate.hideLoading()
                                    alert.dismiss(animated: true)
                                }
                                alert.addAction(btnYes)
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    catch {
                        print("Can not decode data")
                    }
                }
            }
        }
        task.resume()
        
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
