//
//  UserNearlyDisplayViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import CoreLocation

let myShowUserCommunityKey = "myShowUserCommunityKey"

var receiveSocketId = ""
var url = ""
var nameUser = ""
var userLocation = CLLocationCoordinate2D()

class UserNearlyDisplayViewController: UIViewController {
    
    var listUser:ListUsers?
    
    let locationManager = CLLocationManager()
    
    var listSocketUser:[String]?
    
    var socket = SocketIOManager1()
    
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(listUser==nil) {
//            return 0
//        }
//        return (listUser?.count)!
//    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell:UserNearlyDisplayTableViewCell  = tableView.dequeueReusableCell(withIdentifier: " userNearlyCell") as! UserNearlyDisplayTableViewCell
//        cell.nameUser.text = listUser![indexPath.row].name
//        if(listUser![indexPath.row].avatar != nil) {
//            let imageUrlll = URL(string: (listUser![indexPath.row].avatar!))
//            if(imageUrlll != nil) {
//                do {
//                    let imageData:Data = try Data(contentsOf: imageUrlll!)
//                    cell.imageUser.image = UIImage(data: imageData)
//                }
//                catch {
//                    print("khong load dc anh")
//                }
//            }
//        }
//        cell.socketId = listUser![indexPath.row].socketID
//        return cell
//    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        dismiss(animated: true)
//        receiveSocketId = listUser![indexPath.row].socketID!
//        if(listUser![indexPath.row].avatar != nil) {
//            url = listUser![indexPath.row].avatar!
//        }
//        else {
//
//        }
//        nameUser = listUser![indexPath.row].name
//        NotificationCenter.default.post(name: Notification.Name(rawValue: myShowUserCommunityKey), object: nil)
//    }

    @IBOutlet weak var ContainerPopup: UIView! {
        didSet {
            ContainerPopup.addBorder(side: .bottom, thickness: 1, color: .green)
            ContainerPopup.addBorder(side: .top, thickness: 1, color: .green)
            ContainerPopup.addBorder(side: .left, thickness: 1, color: .green)
            ContainerPopup.addBorder(side: .right, thickness: 1, color: .green)
            ContainerPopup.layer.cornerRadius = 10
            ContainerPopup.clipsToBounds = true
        }
    }
    
//    @IBOutlet weak var btnChoseAll: UIButton! {
//        didSet {
//            btnChoseAll.layer.cornerRadius = 10
//            btnChoseAll.clipsToBounds = true
//        }
//    }
    
    @IBOutlet weak var containerMiddle: UIView! {
        didSet {
//            containerMiddle.layer.borderWidth = 1
            containerMiddle.addBorder(side: .top, thickness: 1, color: .gray)
            containerMiddle.addBorder(side: .bottom, thickness: 1, color: .gray)
        }
    }
    
    
//    @IBOutlet weak var listUserNearly: UITableView! {
//        didSet {
//            listUserNearly.backgroundColor = .white
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        listUser = loadListUserNearlyFromUserDefault()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
//        listUserNearly.delegate = self
//        listUserNearly.dataSource = self
        self.view.addSubview(ContainerPopup)
        getUserNearly(userCompletionHandler: {int, error in
            if let int = int {
                print(int)
            }
        })
        
        closeWhenTappedAround()
        activityIndicator()
        indicator.startAnimating()
        indicator.backgroundColor = .white

        // Do any additional setup after loading the view.
    }
    
    var indicator = UIActivityIndicatorView()
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    func getUserNearly(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
        let latUser = locationManager.location?.coordinate.latitude
        let lngUser = locationManager.location?.coordinate.longitude
        
        let latitudeNum = Double(latUser!)
        let longitudeNum = Double(lngUser!)
        
        let userToken = loadUserTokenFromUserDefault()
        let latitudeString = String(latitudeNum)
        let longitudeString = String(longitudeNum)
        let urlNearly = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/user/nearby?"+"lng="+longitudeString+"&lat="+latitudeString+"&radius=50000")!
        var requestNearly = URLRequest(url: urlNearly)
        requestNearly.httpMethod = "GET"
        requestNearly.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let taskUser = URLSession.shared.dataTask(with: requestNearly) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print(dataString)
                        let responseGetUserNearlyData = Data(dataString.utf8)
                        let responseGetUserNearly = try JSONDecoder().decode(ResponseGetUserNearly.self, from: responseGetUserNearlyData)
                        self.listUser = responseGetUserNearly.users
                        DispatchQueue.main.async {
                            self.indicator.stopAnimating()
                        }
                    }
                    catch {
                        print("can not decode data")
                    }
                }
            }
        }
        taskUser.resume()

    }

    func closeWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserNearlyDisplayViewController.close))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func close() {
        dismiss(animated: true)
    }
    
    
    @IBAction func greetingAll(_ sender: Any) {
        let currentUser = loadUserFromUserDefault()
        for user in listUser! {
            if (user.socketID != nil) {
                socket.onSayHello(email: (currentUser?.email)!, name: currentUser!.name, receive_id: user.socketID!, msg: "hello")
            }
        }
        self.dismiss(animated: true)
    }
    
    @IBAction func turndownLight(_ sender: Any) {
        let currentUser = loadUserFromUserDefault()
        for user in listUser! {
            if (user.socketID != nil) {
                socket.onWarnStrongLight(email: (currentUser?.email)!, name: currentUser!.name, receive_id: user.socketID!, msg: "turn down light")
            }
        }
        self.dismiss(animated: true)
    }
    
    @IBAction func turndownSpeed(_ sender: Any) {
        let currentUser = loadUserFromUserDefault()
        for user in listUser! {
            if (user.socketID != nil) {
                socket.onWarnSlowDown(email: (currentUser?.email)!, name: currentUser!.name, receive_id: user.socketID!, msg: "turn down light")
            }
        }
        self.dismiss(animated: true)
    }
    
    
//
//    @IBAction func closePopup(_ sender: Any) {
//        dismiss(animated: true)
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
