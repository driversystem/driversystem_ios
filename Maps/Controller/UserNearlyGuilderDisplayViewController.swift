//
//  UserNearlyGuilderDisplayViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class UserNearlyGuilderDisplayViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        labelHaDenSang.numberOfLines = 0
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBOutlet weak var imageHadenpha: UIImageView! {
        didSet {
            imageHadenpha.addBorder(side: .left, thickness: 2, color: .black)
        }
    }
    
    @IBOutlet weak var imageGiamtocdo: UIImageView! {
        didSet {
            imageGiamtocdo.addBorder(side: .left, thickness: 2, color: .black)
        }
    }
    
    @IBOutlet weak var imageCogiamsat: UIImageView! {
        didSet {
            imageCogiamsat.addBorder(side: .left, thickness: 2, color: .black)
        }
    }
    
    @IBOutlet weak var imageQuaydauxe: UIImageView! {
        didSet {
            imageQuaydauxe.addBorder(side: .left, thickness: 2, color: .black)
        }
    }
    
    @IBOutlet weak var imageChaohoi: UIImageView! {
        didSet {
            imageChaohoi.addBorder(side: .left, thickness: 2, color: .black)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
