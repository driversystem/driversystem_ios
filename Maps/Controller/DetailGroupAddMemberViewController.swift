//
//  DetailGroupAddMemberViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

extension String {
  func replace(string:String, replacement:String) -> String {
      return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
  }

  func removeWhitespace() -> String {
      return self.replace(string: " ", replacement: "")
  }
}

class DetailGroupAddMemberViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        let querySearch = searchMember.text!.removeWhitespace()
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/search?q="+querySearch+"&excludeGroup="+groupID!
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print("hello")
                        print(dataString)
                        let jsonData = Data(dataString.utf8)
                        let responseMember = try JSONDecoder().decode(ResponseSearchMember.self, from: jsonData)
                        self.members = responseMember.users
                        DispatchQueue.main.async {
                            self.listOfMember.reloadData()
                        }
//
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(members==nil) {
            return 0
        }
        else {
            return members!.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DetailGroupAddMemberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listOfMembersResult") as! DetailGroupAddMemberTableViewCell
        cell.name.text = members?[indexPath.row].name
        cell.email.text = members?[indexPath.row].email
        cell.detailMember = members?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! DetailGroupAddMemberTableViewCell
        
        let alert:UIAlertController = UIAlertController(title: "Bạn có muốn mời vào nhóm này không ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
        
        let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+self.groupID!+"/invite?id="+currentCell.detailMember!.id
            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "PUT"
            let userToken = loadUserTokenFromUserDefault()
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        do {
                            print("hello")
                            print(dataString)
                            let jsonData = Data(dataString.utf8)
                            let responseInvite = try JSONDecoder().decode(ResponseInviteMember.self, from: jsonData)
                            DispatchQueue.main.async {
                                if(responseInvite.message == "Mời thành công") {
                                    let alert:UIAlertController = UIAlertController(title: "Mời thành công", message: nil, preferredStyle: .alert)
                                    let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                        alert.dismiss(animated: true)
                                    }
                                    alert.addAction(btnYes)
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else {
                                    let alert:UIAlertController = UIAlertController(title: "Có lỗi gì đó xảy ra", message: "Vui lòng mời lại", preferredStyle: .alert)
                                    let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                        alert.dismiss(animated: true)
                                    }
                                    alert.addAction(btnYes)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            
                            
                        }
                        catch {
                             print("can not decode data")
                        }
                     }
                 }
            }
            task.resume()
            alert.dismiss(animated: true)
        }
        let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
            alert.dismiss(animated: true)
        }
        alert.addAction(btnYes)
        alert.addAction(btnNo)
        self.present(alert, animated: true, completion: nil)
        
        
        

    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
    }
    var members:Members?
    
    var groupID:String?
    
    @IBOutlet weak var searchMember: UISearchBar! {
        didSet {
            searchMember.searchTextField.textColor = UIColor.black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchMember.showsSearchResultsButton = true
        searchMember.delegate = self
        listOfMember.delegate = self
        listOfMember.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    @IBAction func turnBackDetailGroup(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBOutlet weak var listOfMember: UITableView! {
        didSet {
            listOfMember.backgroundColor = .white
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
