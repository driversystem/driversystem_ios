//
//  UserNearlyCommunityDisplayViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class UserNearlyCommunityDisplayViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    let socket = SocketIOManager1()
    
    var receiveId: String!
    
    @IBOutlet weak var btnGreeting: UIButton! {
        didSet {
            btnGreeting.layer.cornerRadius = 10
            btnGreeting.clipsToBounds = true
        }
    }
    @IBAction func close(_ sender: Any) {
        dismiss(animated: false)
    }
    
    
    
    @IBAction func sayHelloWithOther(_ sender: Any) {
        let user = loadUserFromUserDefault()
        print(receiveSocketId)
        print("my socket")
        print(user?.socketID!)
//        socket.onSayHello(email: user!.email!, name: user!.name, socket_id: user!.socketID!, receive_id: receiveSocketId, msg: "hello")
        self.dismiss(animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
