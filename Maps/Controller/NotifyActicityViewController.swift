//
//  NotifyActicityViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class NotifyActicityViewController: UIViewController {

    var stringIcon: String?
    
    @IBOutlet weak var iconTrafficJam: UIImageView! {
        didSet {
            iconTrafficJam.backgroundColor = UIColor(white: 1, alpha: 0)
            iconTrafficJam.backgroundColor = UIColor.clear
            iconTrafficJam.isOpaque = false
        }
    }
    
    @IBOutlet weak var iconCarCrash: UIImageView! {
        didSet {
            iconCarCrash.backgroundColor = UIColor(white: 1, alpha: 0)
            iconCarCrash.backgroundColor = UIColor.clear
            iconCarCrash.isOpaque = false
        }
    }
    
    @IBOutlet weak var iconHazzard: UIImageView! {
        didSet {
            iconHazzard.backgroundColor = UIColor(white: 1, alpha: 0)
            iconHazzard.backgroundColor = UIColor.clear
            iconHazzard.isOpaque = false
        }
    }
    
    @IBOutlet weak var iconHelp: UIImageView! {
        didSet {
            iconHelp.backgroundColor = UIColor(white: 1, alpha: 0)
            iconHelp.backgroundColor = UIColor.clear
            iconHelp.isOpaque = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        DispatchQueue.main.asyncAfter(deadline: .now() + 7) {
//            self.dismiss(animated: false)
//        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .darkContent
    }
    
    @IBAction func car_crash(_ sender: Any) {
        print("crash")
        stringIcon = "crash"
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "postReport") as! PostReportViewController
        scr.stringIcon = stringIcon
        scr.modalPresentationStyle = .fullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: true, completion: nil)
    }
    
    @IBAction func hazard(_ sender: Any) {
        print("hazard")
        stringIcon = "hazard"
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "postReport") as! PostReportViewController
        scr.stringIcon = stringIcon
        scr.modalPresentationStyle = .fullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: true, completion: nil)
    }
    
    @IBAction func help(_ sender: Any) {
        print("help")
        stringIcon = "help"
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "postReport") as! PostReportViewController
        scr.stringIcon = stringIcon
        scr.modalPresentationStyle = .fullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: true, completion: nil)
    }
    
    @IBAction func trafficjam(_ sender: Any) {
        print("traffic")
        stringIcon = "traffic"
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "postReport") as! PostReportViewController
        scr.stringIcon = stringIcon
        scr.modalPresentationStyle = .fullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
