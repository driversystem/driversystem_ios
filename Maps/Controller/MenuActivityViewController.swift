//
//  MenuActivityViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseStorage

class MenuActivityViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let defaults = UserDefaults.standard

    @IBOutlet weak var switchSignal: UISwitch!
    
    @IBOutlet weak var switchOtherDriver: UISwitch!
    
    @IBOutlet weak var userAvatar: UIImageView! {
        didSet {
            userAvatar.layer.cornerRadius = 45
            userAvatar.clipsToBounds = true
            let user = loadUserFromUserDefault()
            let imageUrl = URL(string: (user?.avatar)!)
            print("anh la : ")
            do {
                let imageData:Data = try Data(contentsOf: imageUrl!)
                userAvatar.image = UIImage(data: imageData)
            }
            catch {
                print("khong load dc anh")
            }
            
        }
    }
    
    @IBOutlet weak var btnLogout: UIButton! {
        didSet {
            btnLogout.layer.cornerRadius = 10
            btnLogout.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var userName: UILabel! {
        didSet {
            let user = loadUserFromUserDefault()
            userName.text = user?.name
        }
    }
    
    @IBOutlet weak var userEmail: UILabel! {
        didSet {
            let user = loadUserFromUserDefault()
            userEmail.text = user?.email
        }
    }
    
    @IBAction func changeAvatar(_ sender: Any) {
         let picker = UIImagePickerController()
         picker.allowsEditing = true
         picker.delegate = self
         picker.sourceType = .camera
         present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }

        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)

        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            try? jpegData.write(to: imagePath)
        }
        print("hinh anh la ")
        print(imageName)
        print(imagePath)
        let imageFile = image.jpegData(compressionQuality: 0.8)
        let storage = Storage.storage()

            // Create a storage reference from our storage service
        let storageRef = storage.reference()
        let user = loadUserFromUserDefault()
//        let imageData = image.pngData()
        
        let imagePathOnFirebase = "images/User/UserAvatar/"+user!.id
        let riversRef = storageRef.child(imagePathOnFirebase)
        
        _ = riversRef.putData(imageFile!, metadata: nil) { (metadata, error) in
          guard let metadata = metadata else {
            // Uh-oh, an error occurred!
            return
          }
          // Metadata contains file metadata such as size, content-type.
            _ = metadata.size
          // You can also access to download URL after upload.
          riversRef.downloadURL { (url, error) in
            guard url != nil else {
              // Uh-oh, an error occurred!
              return
            }
            var urlString = url!.absoluteString
            let jsonData:[String:String] = [
                 "avatar" : urlString
             ]
            let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
            
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/profile/picture"
            
            let userToken = loadUserTokenFromUserDefault()
            let url1 = URL(string: subUrl)
            var request = URLRequest(url: url1!)
            request.httpMethod = "POST"
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")
            request.allHTTPHeaderFields = [
               "Content-Type": "application/json",
               "Accept": "application/json"
            ]
            
            let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        do {
                            print("up anh dc chua")
                            print(dataString)
                        }
                        catch {
                             print("can not decode data")
                        }
                     }
                 }
            }
            task.resume()
          }
        }
        
        userAvatar.image = image
        dismiss(animated: true)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        

        initSwitchSignal()
        initSwitchOtherDriver()
        // Do any additional setup after loading the view.
    }
    
    func initSwitchOtherDriver() {
        let isOtherDriver = defaults.bool(forKey: "switchOtherDriver")

        if isOtherDriver {
            switchOtherDriver.isOn = true
        } else {
            switchOtherDriver.isOn = false
        }
    }

    func initSwitchSignal() {
        let isSignal = defaults.bool(forKey: "switchShowSignal")

        if isSignal {
            switchSignal.isOn = true
        } else {
            switchSignal.isOn = false
        }
    }

    @IBAction func onOtherDriver(_ sender: Any) {
        if switchOtherDriver.isOn {
            defaults.set(true, forKey: "switchOtherDriver")
        } else {
            defaults.set(false, forKey: "switchOtherDriver")
        }
    }

    @IBAction func onSignal(_ sender: Any) {
        if switchSignal.isOn {
            defaults.set(true, forKey: "switchShowSignal")
        } else {
            defaults.set(false, forKey: "switchShowSignal")
        }
    }
 
    @IBAction func logOut(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "User")
        defaults.removeObject(forKey: "User-Token")
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "loadingScreen") as! LoadingAppViewController

//        scr2.navigationController?.popToViewController(scr, animated: true)
        
        self.present(scr, animated: true, completion: nil)
//        self.show(scr, sender: nil)
    }
    
    @IBAction func hideMenuBar(_ sender: Any) {
        print("123")
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "navigation") as! UINavigationController
        let transition = CATransition()
        transition.duration = 0.5
        scr.modalPresentationStyle = .fullScreen
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func showBottomSheetDialog(_ sender: Any) {
        let alert:UIAlertController = UIAlertController(title: "Chọn ?", message: "", preferredStyle: .actionSheet)
        let btnTakePhoto:UIAlertAction = UIAlertAction(title: "Chụp ảnh", style: .destructive) { (btn) in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true)
        }
        let btnChooseFromLibrary:UIAlertAction = UIAlertAction(title: "Bộ sưu tập", style: .destructive) { (btn) in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true)
        }
        let btnHuy:UIAlertAction = UIAlertAction(title: "Hủy", style: .destructive) { (btn) in
            alert.dismiss(animated: true)
        }
        
        alert.addAction(btnTakePhoto)
        alert.addAction(btnChooseFromLibrary)
        alert.addAction(btnHuy)

        present(alert, animated: true, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
