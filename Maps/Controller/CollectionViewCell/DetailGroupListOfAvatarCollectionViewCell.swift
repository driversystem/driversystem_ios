//
//  DetailGroupListOfAvatarCollectionViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/23/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class DetailGroupListOfAvatarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageOfEachPhoto: UIImageView!
    
}
