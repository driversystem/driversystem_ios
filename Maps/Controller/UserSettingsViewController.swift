//
//  UserSettingsViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/18/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class UserSettingsViewController: UIViewController {

    
    let defaults = UserDefaults.standard
    
    let socket = SocketIOManager1()


    @IBOutlet weak var switchAnonymous: UISwitch!
    
    @IBOutlet weak var radiusOtherDriver: UISlider! {
        didSet {
            let radius = defaults.value(forKey: "radiusOtherDriver")

            if (radius != nil) {
                radiusOtherDriver.value = radius as! Float

            } else {
                radiusOtherDriver.value = 2
            }
        }
    }
    
    @IBOutlet weak var radiusSignalMarker: UISlider! {
        didSet {
            let radius = defaults.value(forKey: "radiusSignalMarker")

            if (radius != nil) {
                radiusSignalMarker.value = radius as! Float

            } else {
                radiusSignalMarker.value = 2
            }
        }
    }
    
    var userToken:String!
    
    let urlUpdateStatus = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/user/status")!
    
    var status = true
    
    @IBAction func changeStatus(_ sender: Any) {
//        print(switchAnonymous.isOn)
//        if(switchAnonymous.isOn==true) {
//            status = true
//        }
//        else {
//            status = false
//        }
//
//        var request = URLRequest(url: urlUpdateStatus)
//        request.httpMethod = "PUT"
//        request.allHTTPHeaderFields = [
//            "Content-Type": "application/json",
//            "Accept": "application/json"
//        ]
//        let data = ["status": true]
//        let dataJson = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
//
//        request.setValue(userToken, forHTTPHeaderField: "x-access-token")
//        let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
//            if error != nil {
//                print("/error")
//            }
//            else {
//                if let response = response as? HTTPURLResponse {
//                    print("statusCode: \(response.statusCode)")
//                }
//                if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                    print(dataString)
//                }
//            }
//        }
//        task.resume()
        if switchAnonymous.isOn {
            socket.establishConnection()
        } else {
            socket.closeConnection()
        }
    }
    
    @IBOutlet weak var header: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    //    @IBAction func changeRadiusOtherDriver(_ sender: Any) {
//        print(radiusOtherDriver.value)
//    }
    @IBAction func changeRadiusOtherDriver(_ sender: Any) {
        defaults.setValue(radiusOtherDriver.value, forKey: "radiusOtherDriver")
    }
    
    @IBAction func changeRadiusSignalMarker(_ sender: Any) {
        defaults.setValue(radiusSignalMarker.value, forKey: "radiusSignalMarker")
    }
    
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userToken = loadUserTokenFromUserDefault()
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
