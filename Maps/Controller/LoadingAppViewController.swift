//
//  LoadingAppViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/21/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoadingAppViewController: UIViewController {
    
    let NSUserDefault = UserDefaults.standard
    
    var isLogedIn:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isLogedIn = loadUserFromUserDefault()
//        GIDSignIn.sharedInstance()?.presentingViewController = self
                // Automatically sign in the user.
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if(self.isLogedIn==nil) {
                DispatchQueue.main.async {
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
                    let scr = storyboard.instantiateViewController(identifier: "loginOptions") as! LoginOptionsViewController
                    scr.modalPresentationStyle = .fullScreen
                    scr.modalTransitionStyle = .crossDissolve
                    self.present(scr, animated: true, completion: nil)
                }
            }
            else {
                DispatchQueue.main.async {
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
                    let scr = storyboard.instantiateViewController(identifier: "mainActivity") as! ViewController
                    scr.modalPresentationStyle = .fullScreen
                    scr.modalTransitionStyle = .crossDissolve
                    self.present(scr, animated: true, completion: nil)
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    

    // MARK: - Navigation

//     In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//    }
}
