//
//  ManageGroupViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/19/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import SWSegmentedControl
import HMSegmentedControl

class ManageGroupViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
//    @IBOutlet weak var selectGroupOrAppointment: UISegmentedControl! {
//        didSet {
//            selectGroupOrAppointment.layer.cornerRadius = 0
////            selectGroupOrAppointment.
//        }
//    }
    @IBOutlet weak var selectGroupOrAppointment: HMSegmentedControl! {
        didSet {
            
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)

            selectGroupOrAppointment.sectionTitles = ["NHÓM","LỊCH HẸN"]

            selectGroupOrAppointment.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.gray]
            selectGroupOrAppointment.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor : colorBorder]


            
            selectGroupOrAppointment.selectionStyle = .fullWidthStripe
            selectGroupOrAppointment.selectionIndicatorLocation = .bottom
            
//            selectGroupOrAppointment.isVerticalDividerEnabled = true
//            selectGroupOrAppointment.verticalDividerWidth = 2
//            selectGroupOrAppointment.verticalDividerColor = colorBorder
            
    //        sc.backgroundColor = UIColor.white
            selectGroupOrAppointment.selectionIndicatorColor = colorBorder
        }
    }
    
    @IBAction func changeList(_ sender: Any) {
        switch selectGroupOrAppointment.selectedSegmentIndex {
        case 0:
            listOfGroup.isHidden = false
            titleOfHeader.text = "DANH SÁCH NHÓM"
            listOfAppointment.isHidden = true
            break;
        default:
            listOfGroup.isHidden = true
            titleOfHeader.text = "DANH SÁCH LỊCH HẸN"
            listOfAppointment.isHidden = false
            break;
        }
    }
    var userToken:String!
    
    var groups:Groups?
    
    var appointments:[Appointment]?
    
    @IBOutlet weak var header: UIView! {
        didSet {
//            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
//            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    
    @IBOutlet weak var titleOfHeader: UILabel!
    
    @IBOutlet weak var listOfGroup: UITableView! {
        didSet {
            listOfGroup.backgroundColor? = .white
        }
    }
    
    @IBOutlet weak var listOfAppointment: UITableView! {
        didSet {
            listOfAppointment.backgroundColor? = .white
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(groups == nil && appointments == nil) {
            return tableView == listOfGroup ? 0:0
        }
        else {
            if(appointments != nil && groups == nil) {
                let numberOfAppointment = appointments!.count as Int
                return tableView == listOfGroup ? 0:numberOfAppointment
            }
            if(appointments == nil && groups != nil) {
                let number = groups!.count as Int
                return tableView == listOfGroup ? number:0
            }
            if(appointments != nil && groups != nil) {
                let number = groups!.count as Int
                let numberOfAppointment = appointments!.count as Int
                return tableView == listOfGroup ? number:numberOfAppointment
            }
        }
        return tableView == listOfGroup ? 0:0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.listOfGroup {
            let cell:ManageGroupListGroupTableViewCell = tableView.dequeueReusableCell(withIdentifier: "groupCell") as! ManageGroupListGroupTableViewCell
            cell.groupName.text = groups?[indexPath.row].groupName
            cell.group = groups?[indexPath.row]
            if(groups?[indexPath.row].avatar != nil) {
                let imageUrlll = URL(string: (groups?[indexPath.row].avatar!)!)
                if(imageUrlll != nil) {
                    do {
                        let imageData:Data = try Data(contentsOf: imageUrlll!)
                        cell.groupAvatar.image = UIImage(data: imageData)
                    }
                    catch {
                        print("khong load dc anh")
                    }
                }
            }
            return cell
        }
        if tableView == self.listOfAppointment {
            let cell:ManageGroupListAppointmentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listOfAppointment") as! ManageGroupListAppointmentTableViewCell
            cell.address.text = appointments?[indexPath.row].locationAddress
            
            let time = appointments?[indexPath.row].time
            cell.locationName.text = appointments?[indexPath.row].locationName
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 7)
            let datetime = formatter.date(from: time!)
            
            let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)

            let dayOfAppointment = String(day.day!)
            let monthOfAppointment = String(day.month!)
            let hourOfAppointment = String(day.hour!)
            let minOfAppointment = String(day.minute!)
            let monthOfAppointmentInt = Int(monthOfAppointment)
            
            let now = NSDate()
            let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
            
            var nowTime = DateComponents()
            nowTime.year = nowD.year
            nowTime.month = nowD.month
            nowTime.day = nowD.day
            nowTime.timeZone = TimeZone(secondsFromGMT: 7)
            nowTime.hour = nowD.hour
            nowTime.minute = nowD.minute
            let nowTimeDate = Calendar.current.date(from: nowTime)!
            
            var appointTime = DateComponents()
            appointTime.year = day.year
            appointTime.month = day.month
            appointTime.day = day.day
            appointTime.timeZone = TimeZone(secondsFromGMT: 7)
            appointTime.hour = day.hour
            appointTime.minute = day.minute
            let timeApp = Calendar.current.date(from: appointTime)!
            
            let distanceTime = nowTimeDate.distance(to: timeApp)
            if(distanceTime<0) {
                cell.timeCooldown.text = "Đã quá hạn"
            }
            else {
                let distanceDay = appointTime.day! - nowTime.day!
                if(distanceDay > 0) {
                    cell.timeCooldown.text = "Còn "+String(distanceDay)+" ngày"
                }
                else {
                    let distanceHour = appointTime.hour! - nowTime.hour!
                    if(distanceHour > 0) {
                        cell.timeCooldown.text = "Còn "+String(distanceHour)+" giờ"
                    }
                    else {
                        let distanceMin = appointTime.minute! - nowTime.minute!
                        cell.timeCooldown.text = "Còn "+String(distanceMin)+" phút"
                    }
                }
            }
            
            switch monthOfAppointmentInt {
                case 1:
                    cell.monthAndDay.text = "JANUARY"
                    break;
                case 2:
                    cell.monthAndDay.text = "FEBRUARY"
                    break;
                case 3:
                    cell.monthAndDay.text = "MARCH"
                    break;
                case 4:
                    cell.monthAndDay.text = "APRIL"
                    break;
                case 5:
                    cell.monthAndDay.text = "MAY"
                    break;
                case 6:
                    cell.monthAndDay.text = "JUNE"
                    break;
                case 7:
                    cell.monthAndDay.text = "JULY"
                    break;
                case 8:
                    cell.monthAndDay.text = "AUGUST"
                    break;
                case 9:
                    cell.monthAndDay.text = "SEPTEMBER"
                    break;
                case 10:
                    cell.monthAndDay.text = "OCTOBER"
                    break;
                case 11:
                    cell.monthAndDay.text = "NOVEMBER"
                    break;
                default:
                    cell.monthAndDay.text = "DECEMBER"
                    break;
            }
            
            switch day.weekday {
                case 1:
                    cell.dayOfWeek.text = "SUNDAY"
                    break;
                case 2:
                    cell.dayOfWeek.text = "MONDAY"
                    break;
                case 3:
                    cell.dayOfWeek.text = "TUESDAY"
                    break;
                case 4:
                    cell.dayOfWeek.text = "WEDNESDAY"
                    break;
                case 5:
                    cell.dayOfWeek.text = "THURSDAY"
                    break;
                case 6:
                    cell.dayOfWeek.text = "FRIDAY"
                    break;
                default:
                    cell.dayOfWeek.text = "SATURDAY"
                    break;
            }
            
            cell.dayOfMonth.text = dayOfAppointment
            cell.time.text = hourOfAppointment + " : " + minOfAppointment
            
            
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == self.listOfGroup {
            
            let delete = UIContextualAction(style: .normal, title: "Delete") { (action, view1, nil) in
                
                action.backgroundColor = UIColor.red
                view1.backgroundColor = UIColor.red
                
                
                let alert:UIAlertController = UIAlertController(title: "Bạn có muốn xóa nhóm ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
                
                let alertAfterDelete:UIAlertController = UIAlertController(title: "Xóa Thành Công", message: nil, preferredStyle: .alert)
                
                let alertAfterDeleteFail:UIAlertController = UIAlertController(title: "Xóa thất bại!", message: "Bạn không đủ quyền", preferredStyle: .alert)
                
                let btnOKAfterDelete:UIAlertAction = UIAlertAction(title: "OK", style: .destructive) { (btn) in
                    DispatchQueue.main.async {
                        alertAfterDelete.dismiss(animated: true)
                        self.groups?.remove(at: indexPath.row)
                        self.listOfGroup.reloadData()
                    }
                }
                
                let btnOKAfterDeleteFail:UIAlertAction = UIAlertAction(title: "OK", style: .destructive) { (btn) in
                    alertAfterDeleteFail.dismiss(animated: true)

                }
                
                alertAfterDelete.addAction(btnOKAfterDelete)
                alertAfterDeleteFail.addAction(btnOKAfterDeleteFail)
                
                let btnYes:UIAlertAction = UIAlertAction(title: "Xóa", style: .destructive) { (btn) in
                    let currentCell = tableView.cellForRow(at: indexPath) as! ManageGroupListGroupTableViewCell
                    let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+(currentCell.group?.id)!
                    let userToken = loadUserTokenFromUserDefault()
                    let url = URL(string: subUrl)
                    var request = URLRequest(url: url!)
                    request.httpMethod = "DELETE"
                    request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                    let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                        if error != nil {
                            print("/error")
                            print("co loi gi khong ")
                        }
                        else {
                            if let response = response as? HTTPURLResponse {
                                print("statusCode: \(response.statusCode)")
                            }
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    let jsonData = Data(dataString.utf8)
                                    let responseMessage = try JSONDecoder().decode(ResponseMessage.self, from: jsonData)
                                    if (responseMessage.success == true) {
                                        DispatchQueue.main.sync {
                                            self.present(alertAfterDelete, animated: true, completion: nil)
                                        }
                                    }
                                    else {
                                        DispatchQueue.main.sync {
                                            self.present(alertAfterDeleteFail, animated: true, completion: nil)
                                        }
                                    }
                                    
                                }
                                catch {
                                    print("can not decode data ")
                                }
                             }
                         }
                    }
                    task.resume()
                }
                
                let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                    print("Không xóa")
                    alert.dismiss(animated: true)
                }
            
                alert.addAction(btnYes)
                alert.addAction(btnNo)
                self.present(alert, animated: true, completion: nil)
        }
            delete.backgroundColor = UIColor.red
            delete.image = UIImage(named: "trash")
        return UISwipeActionsConfiguration(actions: [delete])
            
        }
        else {
            return UISwipeActionsConfiguration()
        }
//        return UISwipeActionsConfiguration()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == listOfGroup {
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "detailGroup") as! DetailGroupViewController
            let currentCell = tableView.cellForRow(at: indexPath) as! ManageGroupListGroupTableViewCell
            scr.detailGroupId = currentCell.group?.id
            scr.modalPresentationStyle = .fullScreen
            scr.modalTransitionStyle = .crossDissolve
            self.present(scr, animated: true, completion: nil)
        }
        if tableView == listOfAppointment {
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "detailAppointment") as! DetailAppointmentViewController
            scr.appointmentID = self.appointments?[indexPath.row].id
            scr.groupID = self.appointments?[indexPath.row].group
            scr.modalPresentationStyle = .fullScreen
            scr.modalTransitionStyle = .crossDissolve
            self.present(scr, animated: true, completion: nil)
        }
    }
    
//    @IBAction func changeList(_ sender: Any) {
//        switch selectGroupOrAppointment.selectedSegmentIndex {
//        case 0:
//            listOfGroup.isHidden = false
//            listOfAppointment.isHidden = true
//            break;
//        default:
//            listOfGroup.isHidden = true
//            listOfAppointment.isHidden = false
//            break;
//        }
//    }
    
    private let refreshControlListOfGroup = UIRefreshControl()
    
    private let refreshControlListOfAppointment = UIRefreshControl()
    
//    var pending =  UIAlertController()
    var indicator = UIActivityIndicatorView()
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userToken = loadUserTokenFromUserDefault()
        
        listOfGroup.dataSource = self
        listOfGroup.delegate = self
        listOfAppointment.dataSource = self
        listOfAppointment.delegate = self
//        getNumberOfGroupOfUser(userCompletionHandler: {int, error in
//            if let int = int {
//                print(int)
//            }
//        })
//        getNumberOfAppointmentOfUser(userCompletionHandler: {int, error in
//            if let int = int {
//                print(int)
//            }
//        })
        
        
        
        listOfGroup.refreshControl = refreshControlListOfGroup
        listOfAppointment.refreshControl = refreshControlListOfAppointment
        self.refreshControlListOfGroup.addTarget(self, action: #selector(updateDataGroup), for: .valueChanged)
        self.refreshControlListOfAppointment.addTarget(self, action: #selector(updateDataAppointment), for: .valueChanged)

        activityIndicator()
        indicator.startAnimating()
        indicator.backgroundColor = .white
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func viewWillAppear(_ animated: Bool) {
//        self.present(self.pending, animated: true, completion: nil)
//        print("ughjbhb")
        getNumberOfGroupOfUser(userCompletionHandler: {int, error in
            if int != nil {
            }
        })
        getNumberOfAppointmentOfUser(userCompletionHandler: {int, error in
            if int != nil {
            }
        })
    }
    
    @objc private func updateDataGroup() {
        getNumberOfGroupOfUser(userCompletionHandler: {int, error in
        })
        self.refreshControlListOfGroup.endRefreshing()
    }
    
    @objc private func updateDataAppointment() {
        getNumberOfAppointmentOfUser(userCompletionHandler: {int, error in
        })
        self.refreshControlListOfAppointment.endRefreshing()
    }
   
    func getNumberOfGroupOfUser(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/groups"
//        let pending = UIAlertController(title: "", message: nil, preferredStyle: .alert)
//        print("show activity")
//        let indicator = UIActivityIndicatorView(frame: pending.view.bounds)
//        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        pending.view.addSubview(indicator)
//        indicator.startAnimating()
//        self.present(pending, animated: true, completion: nil)
                                 
                let url = URL(string: subUrl)
                var request = URLRequest(url: url!)
                request.httpMethod = "GET"
                request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                    DispatchQueue.main.async {
                        if error != nil {
                            print("/error")
                            print("co loi gi khong ")
                        }
                        else {
                            if let response = response as? HTTPURLResponse {
                                print("statusCode: \(response.statusCode)")
                            }
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    print(dataString)
                                    let jsonData = Data(dataString.utf8)
                                    let responseGroups = try JSONDecoder().decode(ResponseGetGroupOfUser.self, from: jsonData)
                                    self.groups = responseGroups.groups
                                    userCompletionHandler(self.groups?.count,nil)
                                    DispatchQueue.main.async {
                                        self.indicator.stopAnimating()
                                        self.indicator.hidesWhenStopped = true
                                        self.listOfGroup.reloadData()
                                    }
                                    
                                }
                                catch {
                                     print("can not decode data")
                                }
                             }
                         }
                    }
                }
                task.resume()
    }
    
    func getNumberOfAppointmentOfUser(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/appointments"
                     
                    let url = URL(string: subUrl)
                    var request = URLRequest(url: url!)
                    request.httpMethod = "GET"
                    request.setValue(userToken, forHTTPHeaderField: "x-access-token")

                    let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                        if error != nil {
                            print("/error")
                            print("co loi gi khong ")
                        }
                        else {
                            if let response = response as? HTTPURLResponse {
                                print("statusCode: \(response.statusCode)")
                            }
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    let jsonData = Data(dataString.utf8)
                                    let responseAppointment = try JSONDecoder().decode(ResponseGetAppointmentOfUser.self, from: jsonData)
                                    self.appointments = responseAppointment.appointments
                                    for appointment in self.appointments! {
                                        saveAppointmentToUserDefault(appointment: appointment, appointmentId: appointment.id, time: appointment.time, name: "ten nhom")
                                    }
                                    print("alo alo")
                                    DispatchQueue.main.async {
                                        self.listOfAppointment.reloadData()
                                    }
                                }
                                catch {
                                     print("cant not decode date")
                                }
                             }
                         }
                    }
                    task.resume()
        }
    
    @IBAction func turnBack(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
