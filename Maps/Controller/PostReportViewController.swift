//
//  PostReportViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 5/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class PostReportViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var header: UIView!
    
    var urlImageReport:String?
    
    @IBOutlet weak var iconReport: UIImageView! {
        didSet {
            iconReport.image = UIImage(named: stringIcon!)
        }
    }
    
    @IBOutlet weak var containerImageTaken: UIView! {
        didSet {
            containerImageTaken.addBorder(side: .top, thickness: 1, color: .gray)
            containerImageTaken.addBorder(side: .bottom, thickness: 1, color: .gray)
            containerImageTaken.addBorder(side: .left, thickness: 1, color: .gray)
            containerImageTaken.addBorder(side: .right, thickness: 1, color: .gray)
        }
    }
    
    @IBOutlet weak var imageTaken: UIImageView!
    
    @IBOutlet weak var descriptionOfReport: UITextField! {
        didSet {
            descriptionOfReport.addBorder(side: .top, thickness: 1, color: .gray)
            descriptionOfReport.addBorder(side: .bottom, thickness: 1, color: .gray)
            descriptionOfReport.addBorder(side: .left, thickness: 1, color: .gray)
            descriptionOfReport.addBorder(side: .right, thickness: 1, color: .gray)
        }
    }
    
    var stringIcon:String?
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func takeAPhoto(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = .camera
        self.present(picker, animated: true)
    }
    let url = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/report/")!

    var flagImage = false
    
    @IBOutlet weak var btnPostReport: UIButton!

    
    @IBAction func inputDescriptionOfReport(_ sender: Any) {
        let stringText: String = descriptionOfReport.text!
        print(stringText.count)
        if(flagImage == true && stringText.count > 0 ) {
            btnPostReport.isEnabled = true
        }
        else {
            btnPostReport.isEnabled = false
        }
    }
    
    @IBAction func postReport(_ sender: Any) {
        print(stringIcon)
        let description: String = descriptionOfReport.text!
        let user = loadUserFromUserDefault()
        
        let jsoncoordinate:[String:[Double]] = [
            "coordinates" : [(user?.currentLocation?.coordinates![0])!,(user?.currentLocation?.coordinates![1])!]
        ]
        let report = [
            "type": stringIcon!,
            "description": description,
            "geometry": jsoncoordinate,
            "userID": (user?.id)!,
            "status": true,
            "phoneNumber": (user?.phoneNumber)!
            ] as [String : Any]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let dataJson = try! JSONSerialization.data(withJSONObject: report, options: .prettyPrinted)
        let userToken = loadUserTokenFromUserDefault()
        
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print(dataString)
                        DispatchQueue.main.async {
                            let alert:UIAlertController = UIAlertController(title: "Tạo Thành Công", message: nil, preferredStyle: .alert)
                            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                                alert.dismiss(animated: true)
                                DispatchQueue.main.async {
                                    self.dismiss(animated: true)
                                }
                            }
                            alert.addAction(btnYes)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    catch {
                        print("Can not decode data")
                    }
                }
            }
        }
        task.resume()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("33333")
        btnPostReport.isEnabled = false

        self.hideKeyboardWhenTappedAround()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
//    @IBAction func postReport(_ sender: Any) {
//        let user = loadUserFromUserDefault()
//            let report = ReportJson(type: stringIcon! as Any, subtype1: "", subtype2: "", description: decription.text!, geometry: (user!.currentLocation)!, userID: user!.id, numReport: 1, numDelete: 0, status: true, phoneNumber: user!.phoneNumber!, byteImageFile: "",byteAudioFile: "")
//        print(report)
//        do {
//            let data =  try JSONEncoder().encode(report)
//            var request = URLRequest(url: url)
//            request.httpMethod = "POST"
//            request.httpBody = data
//            request.setValue("application/json", forHTTPHeaderField: "Accept")
//            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
//                if error != nil {
//                    print("/error")
//                }
//                else {
//                    if let response = response as? HTTPURLResponse {
//                        print("statusCode: \(response.statusCode)")
//                    }
//                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                        print(dataString)
//                        DispatchQueue.main.async {
//                            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
//                            let scr = storyboard.instantiateViewController(identifier: "notifyActivity") as! NotifyActicityViewController
//                            scr.modalPresentationStyle = .fullScreen
//                            scr.modalTransitionStyle = .crossDissolve
//                            self.present(scr, animated: true, completion: nil)
//                        }
//                    }
//                }
//            }
//            task.resume()
//        }
//        catch {
//
//        }
//
//    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PostReportViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PostReportViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let image = info[.editedImage] as? UIImage else { return }

            let imageName = UUID().uuidString
            let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)

            if let jpegData = image.jpegData(compressionQuality: 0.8) {
                try? jpegData.write(to: imagePath)
            }
            print("hinh anh la ")
            print(imageName)
            print(imagePath)
//            let imageFile = image.jpegData(compressionQuality: 0.8)
//            let storage = Storage.storage()

                // Create a storage reference from our storage service
//            let storageRef = storage.reference()
//            let user = loadUserFromUserDefault()
//
//            let imagePathOnFirebase = "images/User/UserAvatar/"+user!.id
//            let riversRef = storageRef.child(imagePathOnFirebase)
//
//            _ = riversRef.putData(imageFile!, metadata: nil) { (metadata, error) in
//              guard let metadata = metadata else {
//                // Uh-oh, an error occurred!
//                return
//              }
//              // Metadata contains file metadata such as size, content-type.
//                _ = metadata.size
//              // You can also access to download URL after upload.
//              riversRef.downloadURL { (url, error) in
//                guard url != nil else {
//                  // Uh-oh, an error occurred!
//                  return
//                }
//                let urlString = url!.absoluteString
//                let jsonData:[String:String] = [
//                     "avatar" : urlString
//                 ]
//                let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
//
//                let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/profile/picture"
//
//                let userToken = loadUserTokenFromUserDefault()
//                let url1 = URL(string: subUrl)
//                var request = URLRequest(url: url1!)
//                request.httpMethod = "POST"
//                request.setValue(userToken, forHTTPHeaderField: "x-access-token")
//                request.allHTTPHeaderFields = [
//                   "Content-Type": "application/json",
//                   "Accept": "application/json"
//                ]
//
//                let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
//                    if error != nil {
//                        print("/error")
//                        print("co loi gi khong ")
//                    }
//                    else {
//                        if let response = response as? HTTPURLResponse {
//                            print("statusCode: \(response.statusCode)")
//                        }
//                        if let data = data, let _ = String(data: data, encoding: .utf8) {
//                            print("up anh dc chua")
//                         }
//                     }
//                }
//                task.resume()
//              }
//            }
            imageTaken.image = image
            let stringText: String = descriptionOfReport.text!
        flagImage = true
            if(flagImage == true && stringText.count > 0 ) {
                btnPostReport.isEnabled = true
            }
            dismiss(animated: true)

        }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

}
