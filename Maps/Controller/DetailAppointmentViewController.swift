//
//  DetailAppointmentViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/19/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GooglePlaces




class DetailAppointmentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(appointment == nil) {
            return 0
        }
        else {
            return (appointment?.accepted?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListMemberJoinAppointmentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listMemberJoinAppointment") as! ListMemberJoinAppointmentTableViewCell
        cell.nameOfMember.text = appointment?.accepted?[indexPath.row].name
        if(appointment?.accepted?[indexPath.row].avatar != nil) {
            let imageUrlll = URL(string: (appointment?.accepted?[indexPath.row].avatar)!)
            if(imageUrlll != nil) {
                do {
                    let imageData:Data = try Data(contentsOf: imageUrlll!)
                    cell.avatarOfMember.image = UIImage(data: imageData)
                }
                catch {
                    print("khong load dc anh")
                }
            }
        }
        return cell
    }
    
    @IBOutlet weak var calendarOfAppointment: UIView! {
        didSet {
            calendarOfAppointment.layer.borderWidth = 1
        }
    }
    
    @IBOutlet weak var nameGroup: UILabel!
    
    @IBOutlet weak var locationName: UITextField!
    
    @IBOutlet weak var reminderMsg: UITextField!
    
    @IBOutlet weak var btnJoinorLeave: UIButton!
    
    @IBOutlet weak var month: UILabel!
    
    @IBOutlet weak var dayOfWeek: UILabel!
    
    @IBOutlet weak var dateOfMonth: UILabel!
    
    @IBOutlet weak var timeReminder: UILabel!
    
    @IBOutlet weak var timeCooldown: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listMemberJoined.dataSource = self
        listMemberJoined.delegate = self
        getDetailOfAppointment(userCompletionHandler: {int, error in
            if let int = int {
                print(int)
            }
        })
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @IBOutlet weak var header: UIView! {
        didSet {
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            header.addBorder(side: .bottom, thickness: 2, color: colorBorder)
        }
    }
    
    @IBOutlet weak var listMemberJoined: UITableView! {
        didSet {
            listMemberJoined.backgroundColor = .white
        }
    }
    
    var appointmentID:String?
    
    var appointment:AppointmentSpecified?
    
    var nameOfGroup:String?
    
    var groupID:String?
    
    
    
    @IBAction func turnBack(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func getDetailOfAppointment(userCompletionHandler: @escaping (Int?, Error?) -> Void) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/appointment/"+self.appointmentID!
        let userToken = loadUserTokenFromUserDefault()
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print("----")
                        print(dataString)
                        print("-----")
                        let jsonData = Data(dataString.utf8)
//
                        let responseAppointment = try JSONDecoder().decode(ResponseGetDetailAppointmentSpecified.self, from: jsonData)
                        self.appointment = responseAppointment.appointment
                        
                        DispatchQueue.main.async {
                            let user = loadUserFromUserDefault()
                            let userID = user?.id
                            self.locationName.text = self.appointment?.locationName
                            self.reminderMsg.text = self.appointment?.reminderMsg
                            self.nameGroup.text = self.appointment?.group?.groupName
                            
                            let inAppoint = self.appointment?.accepted?.contains(where: { (member) -> Bool in
                                if member.id == userID {
                                    return true
                                }
                                return false
                            })
                            if(inAppoint == true) {
                                self.btnJoinorLeave.setTitle("BỎ HẸN", for: .normal)
                            }
                            else {
                                self.btnJoinorLeave.setTitle("THAM GIA", for: .normal)
                            }
                            
                            let time = self.appointment?.time
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            let datetime = formatter.date(from: time!)
                            
                            let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)
                            
                            let dayOfAppointment = String(day.day!)
                            let monthOfAppointment = String(day.month!)
                            let dayOfWeek = String(day.weekday!)
                            let hourOfAppointment = String(day.hour!)
                            let minOfAppointment = String(day.minute!)
                            
                            
                            let monthOfAppointmentInt = Int(monthOfAppointment)
                            let dayOfWeekInt = Int(dayOfWeek)
                            
                            let now = NSDate()
                            let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
                            
                            var nowTime = DateComponents()
                            nowTime.year = nowD.year
                            nowTime.month = nowD.month
                            nowTime.day = nowD.day
                            nowTime.timeZone = TimeZone(secondsFromGMT: 7)
                            nowTime.hour = nowD.hour
                            nowTime.minute = nowD.minute
                            let nowTimeDate = Calendar.current.date(from: nowTime)!
                            
                            var appointTime = DateComponents()
                            appointTime.year = day.year
                            appointTime.month = day.month
                            appointTime.day = day.day
                            appointTime.timeZone = TimeZone(secondsFromGMT: 7)
                            appointTime.hour = day.hour
                            appointTime.minute = day.minute
                            let timeApp = Calendar.current.date(from: appointTime)!
                            
                            let distanceTime = nowTimeDate.distance(to: timeApp)
                            if(distanceTime<0) {
                                self.timeCooldown.text = "Đã quá hạn"
                            }
                            else {
                                let distanceDay = appointTime.day! - nowTime.day!
                                if(distanceDay > 0) {
                                    self.timeCooldown.text = "Còn "+String(distanceDay)+" ngày"
                                }
                                else {
                                    let distanceHour = appointTime.hour! - nowTime.hour!
                                    if(distanceHour > 0) {
                                        self.timeCooldown.text = "Còn "+String(distanceHour)+" giờ"
                                    }
                                    else {
                                        let distanceMin = appointTime.minute! - nowTime.minute!
                                        self.timeCooldown.text = "Còn "+String(distanceMin)+" phút"
                                    }
                                }
                            }
                            
                            switch monthOfAppointmentInt {
                                case 1:
                                    self.month.text = "JANUARY"
                                    break;
                                case 2:
                                    self.month.text = "FEBRUARY"
                                    break;
                                case 3:
                                    self.month.text = "MARCH"
                                    break;
                                case 4:
                                    self.month.text = "APRIL"
                                    break;
                                case 5:
                                    self.month.text = "MAY"
                                    break;
                                case 6:
                                    self.month.text = "JUNE"
                                    break;
                                case 7:
                                    self.month.text = "JULY"
                                    break;
                                case 8:
                                    self.month.text = "AUGUST"
                                    break;
                                case 9:
                                    self.month.text = "SEPTEMBER"
                                    break;
                                case 10:
                                    self.month.text = "OCTOBER"
                                    break;
                                case 11:
                                    self.month.text = "NOVEMBER"
                                    break;
                                default:
                                    self.month.text = "DECEMBER"
                                    break;
                            }
                            switch dayOfWeekInt {
                                case 1:
                                    self.dayOfWeek.text = "SUNDAY"
                                    break;
                                case 2:
                                    self.dayOfWeek.text = "MONDAY"
                                    break;
                                case 3:
                                    self.dayOfWeek.text = "TUESDAY"
                                    break;
                                case 4:
                                    self.dayOfWeek.text = "WEDNESDAY"
                                    break;
                                case 5:
                                    self.dayOfWeek.text = "THURSDAY"
                                    break;
                                case 6:
                                    self.dayOfWeek.text = "FRIDAY"
                                    break;
                                default:
                                    self.dayOfWeek.text = "SATURDAY"
                                    break;
                            }
                            
                            
                            
                            self.dateOfMonth.text = dayOfAppointment
                            self.timeReminder.text = hourOfAppointment+" : "+minOfAppointment
                            
                            
                            self.listMemberJoined.reloadData()
                        }
                        
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }
    
    @IBAction func JoinOrLeave(_ sender: Any) {
        if(btnJoinorLeave.titleLabel?.text == "BỎ HẸN") {
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/appointment/"+self.appointmentID!+"/withdraw"
            let userToken = loadUserTokenFromUserDefault()
            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        print(dataString)
                        DispatchQueue.main.async {
                            self.btnJoinorLeave.setTitle("THAM GIA", for: .normal)
                            self.getDetailOfAppointment(userCompletionHandler: {int, error in
                            })
                        }
                     }
                 }
            }
            task.resume()
        }
        if(btnJoinorLeave.titleLabel?.text == "THAM GIA") {
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/appointment/"+self.appointmentID!+"/accept"
            let userToken = loadUserTokenFromUserDefault()
            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")

            let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        print(dataString)
                        DispatchQueue.main.async {
                            self.btnJoinorLeave.setTitle("BỎ HẸN", for: .normal)
                            self.getDetailOfAppointment(userCompletionHandler: {int, error in
                            })
                        }
                     }
                 }
            }
            task.resume()
        }
    }
    
    @IBAction func findLocation(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "mainActivity") as! ViewController
        scr.selectedAppointmentFromDetailAppoinment = appointment
        scr.modalPresentationStyle = .overFullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: false, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
