//
//  LoginOptionsViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/4/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginOptionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        // Automatically sign in the user.
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
           selector: #selector(afterSignInGoogle),
           name: NSNotification.Name(rawValue: signInGoogleKey),
           object: nil)
    }
    
    @objc func afterSignInGoogle() {
        DispatchQueue.main.async {
            let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
            let scr = storyboard.instantiateViewController(identifier: "mainActivity") as! ViewController
            scr.modalPresentationStyle = .overFullScreen
            self.present(scr, animated: false, completion: nil)
        }
    }
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    @IBOutlet weak var btnSignInByFacebook: UIButton! {
        didSet {
            btnSignInByFacebook.layer.cornerRadius = 5
            btnSignInByFacebook.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnSignInByEmail: UIButton! {
        didSet {
            btnSignInByEmail.layer.cornerRadius = 5
            btnSignInByEmail.clipsToBounds = true
        }
    }

    @IBAction func signInByEmail(_ sender: Any) {
//        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func signInByGoogle(_ sender: Any) {
        print("123")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
