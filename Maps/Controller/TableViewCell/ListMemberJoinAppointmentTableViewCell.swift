//
//  ListMemberJoinAppointmentTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/7/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ListMemberJoinAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOfMember: UILabel!
    
    @IBOutlet weak var avatarOfMember: UIImageView! {
        didSet {
            avatarOfMember.layer.cornerRadius = avatarOfMember.frame.height/2
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
