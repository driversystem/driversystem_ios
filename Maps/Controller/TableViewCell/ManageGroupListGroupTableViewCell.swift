//
//  ManageGroupListGroupTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/22/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ManageGroupListGroupTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var groupAvatar: UIImageView! {
        didSet {
            groupAvatar.layer.cornerRadius = groupAvatar.frame.size.width/2
            groupAvatar.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var groupDescription: UITextView! {
        didSet {
            groupDescription.backgroundColor = UIColor.white
        }
    }
    
    @IBOutlet weak var groupName: UILabel!
    
    @IBOutlet weak var cell: UIView! {
        didSet {
//            let colorLine = UIColor(red: 33, green: 150, blue: 83, alpha: 0)
            let colorBorder = UIColor(red: 0.1294117647, green: 0.58823529411, blue: 0.32549019607, alpha:  1.0)
            cell.addBorder(side: .bottom, thickness: 1, color: colorBorder)
        }
    }
    
    var group:Group?
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
