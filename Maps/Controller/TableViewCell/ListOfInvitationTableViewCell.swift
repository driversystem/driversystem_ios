//
//  ListOfInvitationTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/30/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ListOfInvitationTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var nameOfGroup: UILabel!
    
    @IBOutlet weak var avatarOfGroup: UIImageView! {
        didSet {
            avatarOfGroup.layer.cornerRadius = avatarOfGroup.frame.height/2
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
