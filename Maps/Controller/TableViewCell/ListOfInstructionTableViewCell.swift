//
//  ListOfInstructionTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/31/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ListOfInstructionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var instruction: UITextView! {
        didSet {
            instruction.backgroundColor = UIColor.white
            instruction.textColor = UIColor.black
//            instruction.font = UIFont(name: "System", size: 35)
//            instruction.style
        }
    }
    
    @IBOutlet weak var distance: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
