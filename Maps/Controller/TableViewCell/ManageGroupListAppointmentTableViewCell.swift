//
//  ManageGroupListAppointmentTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/22/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class ManageGroupListAppointmentTableViewCell: UITableViewCell {

        
//    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var address: UITextView! {
        didSet {
//            address.backgroundColor = UIColor.white
        }
    }
    //    @IBOutlet weak var locationName: UITextView!
    
    @IBOutlet weak var monthAndDay: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var locationName: UITextView!
    
    //    @IBOutlet weak var timeCooldown: UILabel!
    
    @IBOutlet weak var dayOfMonth: UILabel!
    
    @IBOutlet weak var timeCooldown: UILabel!
    
    @IBOutlet weak var dayOfWeek: UILabel!
    
    @IBOutlet weak var cell: UIView! {
        didSet {
//            cell.addBorder(side: .bottom, thickness: 1, color: .green)
//            cell.layer.borderWidth = 10.0
//            cell.layer.borderColor = UIColor.white.cgColor
//            cell.addBorder(side: .bottom, thickness: 10, color: .white)
//            cell.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
            //Round Corners
//            cell.layer.cornerRadius = 20

        }
    }
    @IBOutlet weak var viewCell: UIView! {
        didSet {
            viewCell.layer.cornerRadius = 10
        }
    }
    @IBOutlet weak var calenderView: UIView! {
        didSet {
//            calenderView.layer.borderWidth = 1
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
