//
//  DetailGroupMemberTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class DetailGroupMemberTableViewCell: UITableViewCell {

//    @IBOutlet weak var avatarOfMember: UIImageView! {
//        didSet {
////            avatarOfMember.layer.cornerRadius = avata
//            avatarOfMember.layer.cornerRadius = avatarOfMember.frame.height/2
//            avatarOfMember.clipsToBounds = true
//        }
//    }
    @IBOutlet weak var avatarOfMember: UIImageView! {
        didSet {
            avatarOfMember.layer.cornerRadius = 45
            avatarOfMember.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var nameOfMember: UILabel!
    
    @IBOutlet weak var roleOfMember: UILabel!
    
    @IBOutlet weak var phone: UILabel! {
        didSet {
        }
    }
    
    
    
    var member:Member?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
