//
//  UserNearlyDisplayTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/21/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class UserNearlyDisplayTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageUser: UIImageView! {
        didSet {
            imageUser.layer.cornerRadius = imageUser.frame.size.width/2
            imageUser.clipsToBounds = true
        }
    }
    
    
    @IBOutlet weak var nameUser: UILabel!
        
    var socketId: String!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        imageUser.layer.cornerRadius = imageUser.frame.size.width/2
//        imageUser.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
