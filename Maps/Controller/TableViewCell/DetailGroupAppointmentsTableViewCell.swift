//
//  DetailGroupAppointmentsTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class DetailGroupAppointmentsTableViewCell: UITableViewCell {

//    @IBOutlet weak var containderDateTime: UIView! {
//        didSet {
//            containderDateTime.layer.cornerRadius = 5
//            containderDateTime.layer.borderWidth = 1
//        }
//    }
    
    @IBOutlet weak var timeCooldown: UILabel!

    
    @IBOutlet weak var locationName: UITextView!
    @IBOutlet weak var month: UILabel!
    
    @IBOutlet weak var dayOfWeek: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var placeAppointment: UITextView!
    
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
