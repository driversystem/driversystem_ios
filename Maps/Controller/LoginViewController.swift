//
//  LoginViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var InputEmail: UITextField! {
        didSet {
            InputEmail.tintColor = UIColor.lightGray
            InputEmail.layer.cornerRadius = InputEmail.frame.size.height/2
            InputEmail.clipsToBounds = true        }
    }
    
    @IBOutlet weak var InputPassword: UITextField! {
        didSet {
            InputPassword.tintColor = UIColor.lightGray
            InputPassword.layer.cornerRadius = InputPassword.frame.size.height/2
            InputPassword.clipsToBounds = true
            let imageView = UIImageView()
            let image = UIImage(named: "ic_avatar")
            imageView.image = image
            InputPassword.leftView = imageView
        }
    }
    
//    @IBOutlet weak var btnLogin: UIButton! {
//        didSet {
//            btnLogin.layer.cornerRadius = btnLogin.frame.size.height/2
//            btnLogin.clipsToBounds = true
//        }
//    }
    
    @IBOutlet weak var btnLogin: LoadingButton! {
        didSet {
            btnLogin.layer.cornerRadius = btnLogin.frame.size.height/2
            btnLogin.clipsToBounds = true
        }
    }
    
    let url = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/auth/email")!
//    https://driversystemapp.herokuapp.com/api-user/v1/report/
//    let urlUpdateCurrentLocation = URL(string: "https://carmapfordeveloper.herokuapp.com/api-user/v1/user/updateCurrentLocation")!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        
        let emailInput: String = InputEmail.text!
        let passwordInput: String = InputPassword.text!
        
//        btnLogin.isEnabled = false
        btnLogin.titleLabel?.textColor = UIColor.gray
        btnLogin.showLoading()
        loginUser(url: url, email: emailInput, password: passwordInput)
    }
    
    func loginUser(url: URL, email: String?, password: String?) {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let jsonLogin = "email="+email!+"&password="+password!
        request.httpBody = jsonLogin.data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    
                    print(data)
                    print(dataString)
                    let jsonData = Data(dataString.utf8)
                    do {
                        _ = try JSONDecoder().decode(ListInvalidRegister.self, from: jsonData)
                        print("invalid")
                        
                    } catch {
                        do {
                            let getUserLogin = try JSONDecoder().decode(ResponseLogin.self, from: jsonData)
                            
                            let user = getUserLogin.user
                            let userToken = getUserLogin.token
                            
                            print(dataString)
                            saveUserToUserDefault(User: user)
                            saveUserTokenToUserDefault(token: userToken)
                    
                            DispatchQueue.main.async {
                                let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
                                let scr = storyboard.instantiateViewController(identifier: "mainActivity") as! ViewController
                                scr.modalPresentationStyle = .fullScreen
                                scr.modalTransitionStyle = .crossDissolve
                                self.present(scr, animated: true, completion: nil)
                            }
                        }
                        catch {
                            
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
