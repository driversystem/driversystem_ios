//
//  TesttViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/16/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import HMSegmentedControl

class TesttViewController: UIViewController {

    
    @IBOutlet weak var sc: HMSegmentedControl!
    
    @IBAction func change(_ sender: Any) {
        print(sc.selectedSegmentIndex)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
            

        sc.sectionTitles = ["One Day", "Two", "Three"]
        
        sc.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.green]
        

        sc.selectionStyle = .fullWidthStripe
        sc.selectionIndicatorLocation = .bottom
        
        sc.isVerticalDividerEnabled = true
        sc.verticalDividerWidth = 2
        sc.verticalDividerColor = UIColor.green
        
//        sc.backgroundColor = UIColor.white
        sc.selectionIndicatorColor = .green
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
