//
//  ViewController.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit
import CoreLocation
import GoogleMapsUtils
import FirebaseStorage
import GoogleSignIn
import UserNotifications

class ViewController: UIViewController, UIViewControllerTransitioningDelegate, GMSAutocompleteTableDataSourceDelegate, UISearchControllerDelegate, UISearchBarDelegate, UINavigationControllerDelegate, UNUserNotificationCenterDelegate {
  
//    Mapping Outlet from MainStoryBoard
    
    @IBOutlet weak var mapView: GMSMapView!
        
    @IBOutlet weak var btnUserCommunication: UIImageView!
    
    @IBOutlet weak var btnNotifyActivity: UIImageView!
    
    @IBOutlet weak var btnCurrentGPS: UIImageView! {
        didSet {
            btnCurrentGPS.backgroundColor = UIColor(white: 1, alpha: 0)
            btnCurrentGPS.backgroundColor = UIColor.clear
            btnCurrentGPS.isOpaque = false
        }
    }
    
    @IBAction func cameraToCurrentLocation(_ sender: Any) {
        CATransaction.begin()
        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
        mapView.animate(toZoom: 15)
        mapView.animate(toLocation: self.locationManager.location!.coordinate)
        CATransaction.commit()

    }
    
    //    View Of Information Location Dialog
        
    @IBOutlet weak var informationOfLocationDialog: UIView! {
        didSet {
//            informationOfLocationDialog.layer.cornerRadius = 5
//            informationOfLocationDialog.layer.shadowColor = UIColor.black.cgColor
//            informationOfLocationDialog.layer.shadowOpacity = 1
//            informationOfLocationDialog.layer.shadowOffset = .zero
//            informationOfLocationDialog.layer.shadowRadius = 10
        }
    }
    
    var isShowParkingAndPetrol = false
    
    @IBOutlet weak var parkingIcon: UIImageView!
    
    @IBOutlet weak var petrolIcon: UIImageView!
    
    @IBAction func quickNavigation(_ sender: Any) {
        if(isShowParkingAndPetrol == false ) {
            UIView.transition(with: parkingIcon,
                                      duration: 0.5,
                                      options: .curveEaseOut,
                                      animations: {
                                        self.parkingIcon.alpha = 1
                                      }) { (true) in }
            UIView.transition(with: petrolIcon,
                                      duration: 0.5,
                                      options: .curveEaseOut,
                                      animations: {
                                        self.petrolIcon.alpha = 1
                                      }) { (true) in }
            isShowParkingAndPetrol = true
        }
        else {
            UIView.transition(with: parkingIcon,
                                      duration: 0.5,
                                      options: .curveEaseOut,
                                      animations: {
                                        self.parkingIcon.alpha = 0
                                      }) { (true) in }
            UIView.transition(with: petrolIcon,
                                      duration: 0.5,
                                      options: .curveEaseOut,
                                      animations: {
                                        self.petrolIcon.alpha = 0
                                      }) { (true) in }
            isShowParkingAndPetrol = false
        }

    }
    
    var isShowParking = false
    
    var isShowPetrol = false
    
    @IBAction func showParking(_ sender: Any) {
        if(isShowParking == false) {
            let config = URLSessionConfiguration.default
                    let session = URLSession(configuration: config)
                    
            //        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&alternatives=true&mode=driving&language=vi&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!

                    let url = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\((locationManager.location?.coordinate.latitude)!),\((locationManager.location?.coordinate.longitude)!)&radius=1000&type=parking&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!
                    print(url)

                    let task = session.dataTask(with: url, completionHandler: {
                        (data, response, error) in
                        if error != nil {
                            print(error!.localizedDescription)
                        } else {
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    print(dataString)
                                    let jsonData = Data(dataString.utf8)
                                    let responseGetParking = try JSONDecoder().decode(ResponseGetParking.self, from: jsonData)
                                    for result in responseGetParking.results {
                                        let marker = GMSMarker()
                                        let position = CLLocationCoordinate2D(latitude: result.geometry.location.lat,longitude: result.geometry.location.lng)
                                        marker.position = position
                                        marker.icon = UIImage(named: "parking-btn")
                                        marker.title = "parking"
                                        marker.userData = result
                                        marker.map = self.mapView
                                        self.listMarkerParking.append(marker)
                                        self.isShowParking = true
            //                            self.listMarkersOfReportNearly[signalMarker.id]?.position = position
            //                            self.listMarkersOfReportNearly[signalMarker.id]?.icon = UIImage(named: "icon"+signalMarker.type!)
            //                            self.listMarkersOfReportNearly[signalMarker.id]?.title = "report"
            //                            self.listMarkersOfReportNearly[signalMarker.id]?.map = self.mapView
            //                            self.listMarkersOfReportNearly[signalMarker.id]?.userData = signalMarker
                                        
                                    }
                                }
                                catch {
                                    print("can not decode data")
                                }
                            }
                        }
                    })
                    task.resume()
                    
        }
        else {
            isShowParking = false
            for marker in listMarkerParking {
                marker.map = nil
            }
            listMarkerParking.removeAll()
        }
    }
    
    var listMarkerParking = [GMSMarker()]
    
    var listMarkerPetrol = [GMSMarker()]

    @IBAction func showPetrol(_ sender: Any) {
        if(isShowPetrol == false) {
            let config = URLSessionConfiguration.default
                    let session = URLSession(configuration: config)
                    
            //        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&alternatives=true&mode=driving&language=vi&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!

                    let url = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\((locationManager.location?.coordinate.latitude)!),\((locationManager.location?.coordinate.longitude)!)&radius=1000&type=gas_station&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!
                    print(url)

                    let task = session.dataTask(with: url, completionHandler: {
                        (data, response, error) in
                        if error != nil {
                            print(error!.localizedDescription)
                        } else {
                            if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                do {
                                    print(dataString)
                                    let jsonData = Data(dataString.utf8)
                                    let responseGetParking = try JSONDecoder().decode(ResponseGetParking.self, from: jsonData)
                                    for result in responseGetParking.results {
                                        let marker = GMSMarker()
                                        let position = CLLocationCoordinate2D(latitude: result.geometry.location.lat,longitude: result.geometry.location.lng)
                                        marker.position = position
                                        marker.icon = UIImage(named: "petrol-btn")
                                        marker.title = "parking"
                                        marker.userData = result
                                        marker.map = self.mapView
                                        self.listMarkerPetrol.append(marker)
                                        self.isShowPetrol = true
                                    }
                                }
                                catch {
                                    print("can not decode data")
                                }
                            }
                        }
                    })
                    task.resume()
                    
        }
        else {
            isShowPetrol = false
            for marker in listMarkerPetrol {
                marker.map = nil
            }
            listMarkerPetrol.removeAll()
        }
    }
    
    @IBOutlet weak var addressLabel: UITextView!
        
    @IBOutlet weak var namePlaceLabel: UITextView!
    
    @IBOutlet weak var btnDirectionOfInformationOfLocationDialog: UIButton! {
        didSet {
            btnDirectionOfInformationOfLocationDialog.layer.cornerRadius = 10
            btnDirectionOfInformationOfLocationDialog.clipsToBounds = true
            let whiteColor:UIColor = UIColor.white
            btnDirectionOfInformationOfLocationDialog.layer.borderWidth = 2
            btnDirectionOfInformationOfLocationDialog.layer.borderColor = whiteColor.cgColor
        }
    }
    
    @IBAction func swipeDown(_ sender: Any) {
        markerSearch.map = nil
        UIView.transition(with: informationOfLocationDialog,
                          duration: 0.5,
                          options: .curveEaseOut,
                          animations: {
                            self.informationOfLocationDialog.center.y = self.view.frame.height + (self.informationOfLocationDialog.frame.height)/2
                            self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true
                            self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y + self.informationOfLocationDialog.frame.height
                            self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
                          }) { (true) in
//                            locationLat = nil
//                            locationLng = nil
//                            locationNameSpecified = nil

        }
    }
    
    @IBAction func showDirection(_ sender: Any) {
//        updateRoute = true
        UIView.transition(with: informationOfLocationDialog,
                          duration: 0.3,
                            options: .curveEaseOut,
                            animations: {
                                self.informationOfLocationDialog.center.y = self.view.frame.height + self.informationOfLocationDialog.frame.height/2
                                self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true
                                self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y + self.informationOfLocationDialog.frame.height
                                self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
                            }) { (true) in
                                UIView.transition(with: self.directionDialog,
                                                  duration: 0.5,
                                                  options: .curveEaseInOut,
                                                  animations: {
                                                    self.directionDialog.center.y = self.view.frame.height - self.directionDialog.frame.height/2
                                                    self.directionDialog.translatesAutoresizingMaskIntoConstraints = true
                                                    self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y - self.directionDialog.frame.height
                                                    self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
                                                  }) { (true) in
                                                    self.listInstruction.isHidden = false
                                                }
                            }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.mapView.camera = GMSCameraPosition(target: self.locationManager.location!.coordinate, zoom: 30, bearing: 0, viewingAngle: 0)
            self.mapView.animate(toLocation: self.locationManager.location!.coordinate)
            self.drawRoute(src: self.locationManager.location!.coordinate, dst: self.destination!)
        }
                                           
    }

    @IBAction func groupTravelling(_ sender: Any) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+groupOfAppointment!+"/members/location"
        let userToken = loadUserTokenFromUserDefault()
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")
        
        UIView.transition(with: informationOfLocationDialog,
                          duration: 0.3,
                          options: .curveEaseOut,
                          animations: {
                            self.informationOfLocationDialog.center.y = self.view.frame.height + self.informationOfLocationDialog.frame.height/2    }) { (true) in
                                UIView.transition(with: self.directionDialog,
                                                  duration: 0.5,
                                                  options: .curveEaseInOut,
                                                  animations: {
                                                    self.directionDialog.center.y = self.view.frame.height - self.directionDialog.frame.height/2    }) { (true) in
                                }
        }
        directionDialog.translatesAutoresizingMaskIntoConstraints = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.mapView.camera = GMSCameraPosition(target: self.locationManager.location!.coordinate, zoom: 30, bearing: 0, viewingAngle: 0)
            self.mapView.animate(toLocation: self.locationManager.location!.coordinate)
            self.drawRoute(src: self.locationManager.location!.coordinate, dst: self.destination!)
        }

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        let jsonData = Data(dataString.utf8)
                        let responseMembersOfAppoinment = try JSONDecoder().decode(ResponseGetMemberOfAppointment.self, from: jsonData)
                        let listMembers = responseMembersOfAppoinment.members
                        for member in listMembers {
                            let lng = member.currentLocation.coordinates![0]
                            let lat = member.currentLocation.coordinates![1]
                            let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                            let isExistMember = self.listMarkersOfNearlyDriver[member.id]
                            DispatchQueue.main.async {
                                 if(isExistMember==nil) {
                                    let marker = GMSMarker()
                                    self.listMarkerOfMemberOfGroups.updateValue(marker, forKey: member.id)
                                    self.listMarkerOfMemberOfGroups[member.id]?.position = position
                                    let imageUrl = URL(string: member.avatar)
                                    do {
                                        let imageData:Data = try Data(contentsOf: imageUrl!)
                                        let image = UIImage(data: imageData)
                                        self.listMarkerOfMemberOfGroups[member.id]?.icon =  self.drawImageWithProfilePic(pp: image!, image: UIImage(named: "markerOfMember")!)
                                    }
                                    catch {
                                        print("khong load dc anh")
                                    }
                                    self.listMarkerOfMemberOfGroups[member.id]?.title = "member"
                                    self.listMarkerOfMemberOfGroups[member.id]?.map = self.mapView
                                    self.listMarkerOfMemberOfGroups[member.id]?.userData = member
                                } else {
                                    self.listMarkerOfMemberOfGroups[member.id]?.position = position
                                    let imageUrl = URL(string: member.avatar)
                                    do {
                                        let imageData:Data = try Data(contentsOf: imageUrl!)
                                        let image = UIImage(data: imageData)
                                        self.listMarkerOfMemberOfGroups[member.id]?.icon =  self.drawImageWithProfilePic(pp: image!, image: UIImage(named: "markerOfMember")!)
                                    }
                                    catch {
                                        print("khong load dc anh")
                                    }
                                    self.listMarkerOfMemberOfGroups[member.id]?.title = "member"
                                    self.listMarkerOfMemberOfGroups[member.id]?.map = self.mapView
                                    self.listMarkerOfMemberOfGroups[member.id]?.userData = member
                                }
                            }
                        }
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }
    
    func getLocationOfMemberInAppointmentGroupTravelling(groupIDOfAppointment: String) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/group/"+groupIDOfAppointment+"/members/location"
        let userToken = loadUserTokenFromUserDefault()
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
                print("co loi gi khong ")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        let jsonData = Data(dataString.utf8)
                        let responseMembersOfAppoinment = try JSONDecoder().decode(ResponseGetMemberOfAppointment.self, from: jsonData)
                        let listMembers = responseMembersOfAppoinment.members
                        for member in listMembers {
                            let lng = member.currentLocation.coordinates![0]
                            let lat = member.currentLocation.coordinates![1]
                            let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                            let isExistMember = self.listMarkersOfNearlyDriver[member.id]
                            DispatchQueue.main.async {
                                 if(isExistMember==nil) {
                                    let marker = GMSMarker()
                                    self.listMarkerOfMemberOfGroups.updateValue(marker, forKey: member.id)
                                    self.listMarkerOfMemberOfGroups[member.id]?.position = position
                                    let imageUrl = URL(string: member.avatar)
                                    do {
                                        let imageData:Data = try Data(contentsOf: imageUrl!)
                                        let image = UIImage(data: imageData)
                                        self.listMarkerOfMemberOfGroups[member.id]?.icon =  self.drawImageWithProfilePic(pp: image!, image: UIImage(named: "markerOfMember")!)
                                    }
                                    catch {
                                        print("khong load dc anh")
                                    }
                                    self.listMarkerOfMemberOfGroups[member.id]?.title = "member"
                                    self.listMarkerOfMemberOfGroups[member.id]?.map = self.mapView
                                    self.listMarkerOfMemberOfGroups[member.id]?.userData = member
                                } else {
                                    self.listMarkerOfMemberOfGroups[member.id]?.position = position
                                    let imageUrl = URL(string: member.avatar)
                                    do {
                                        let imageData:Data = try Data(contentsOf: imageUrl!)
                                        let image = UIImage(data: imageData)
                                        self.listMarkerOfMemberOfGroups[member.id]?.icon =  self.drawImageWithProfilePic(pp: image!, image: UIImage(named: "markerOfMember")!)
                                    }
                                    catch {
                                        print("khong load dc anh")
                                    }
                                    self.listMarkerOfMemberOfGroups[member.id]?.title = "member"
                                    self.listMarkerOfMemberOfGroups[member.id]?.map = self.mapView
                                    self.listMarkerOfMemberOfGroups[member.id]?.userData = member
                                }
                            }
                        }
                    }
                    catch {
                         print("can not decode data")
                    }
                 }
             }
        }
        task.resume()
    }

    func drawImageWithProfilePic(pp: UIImage, image: UIImage) -> UIImage {

        let imgView = UIImageView(image: image)
        imgView.backgroundColor = UIColor(white: 1, alpha: 0)
        imgView.backgroundColor = UIColor.clear
        imgView.isOpaque = false
        
        
        
        
        let picImgView = UIImageView(image: pp)
        picImgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)

        imgView.addSubview(picImgView)
        picImgView.center.x = imgView.center.x
        picImgView.center.y = imgView.center.y - 7
        picImgView.layer.cornerRadius = picImgView.frame.width/2
        picImgView.clipsToBounds = true
        imgView.setNeedsLayout()
        picImgView.setNeedsLayout()

        let newImage = imageWithView(view: imgView)
        return newImage
    }
    
    func imageWithView(view: UIView) -> UIImage {
        var image: UIImage?
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        return image ?? UIImage()
    }
    
    //    View Of Marker Report Dialog

    @IBOutlet weak var informationOfReportMarkerDialog: UIView!
    
    @IBOutlet weak var typeLabelMarkerOfReportMarkerDialog: UILabel!
    
    @IBOutlet weak var distanceLabelMarkerOfReportMarkerDialog: UILabel!
    
    @IBOutlet weak var imageOfReportMarkerDialog: UIImageView!
    
    @IBOutlet weak var btnCloseReportMarkerDialog: UIView! {
        didSet {
            btnCloseReportMarkerDialog.layer.cornerRadius = 10
            let whiteColor:UIColor = UIColor.white
            btnCloseReportMarkerDialog.layer.borderWidth = 2
            btnCloseReportMarkerDialog.layer.borderColor = whiteColor.cgColor
        }
    }
    
    @IBOutlet weak var btnLike: UIView! {
        didSet {
            btnLike.layer.cornerRadius = 10
            btnLike.addBorder(side: .right, thickness: 2, color: .white)
        }
    }
    
    @IBOutlet weak var btnUpVoteReportMarkerDialog: UIView! {
        didSet {
            btnUpVoteReportMarkerDialog.layer.cornerRadius = 10
            let whiteColor:UIColor = UIColor.white
            btnUpVoteReportMarkerDialog.layer.borderWidth = 2
            btnUpVoteReportMarkerDialog.layer.borderColor = whiteColor.cgColor
        }
    }
    
    @IBAction func closeInformationOfReportMarkerDialog(_ sender: Any) {
        UIView.transition(with: informationOfReportMarkerDialog,
                          duration: 0.3,
        options: .curveEaseOut,
        animations: {
          self.informationOfReportMarkerDialog.center.y = 0 - self.informationOfReportMarkerDialog.frame.height/2
            self.informationOfReportMarkerDialog.translatesAutoresizingMaskIntoConstraints = true
        }) { (true) in }
    }
    
    @IBAction func doUpVote(_ sender: Any) {
        let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/report/"+selectedMarkerReport!+"/updateNumReport"
        let url = URL(string: subUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "PUT"

        let task = URLSession.shared.dataTask(with: request) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print(dataString)
                 }
             }
        }
        task.resume()
    }
    

    
    @IBOutlet weak var upperContainerInformationOfReportMarkerDialog: UIView! {
        didSet {
            upperContainerInformationOfReportMarkerDialog.addBorder(side: .bottom, thickness: 1, color: .white)
        }
    }

    // View Of Information Driver and Greeting Driver

    @IBOutlet weak var greetingDialog: UIView!

    @IBOutlet weak var avatarOfDriverOnGreetingDialog: UIImageView! {
        didSet {
            avatarOfDriverOnGreetingDialog.layer.cornerRadius = avatarOfDriverOnGreetingDialog.frame.height/2
            avatarOfDriverOnGreetingDialog.clipsToBounds = true
        }
    }
    
    @IBAction func closeGreetingView(_ sender: Any) {
        UIView.transition(with: greetingDialog,
                          duration: 0.5,
                            options: .curveEaseOut,
                            animations: {
                              self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
                            }) { (true) in }
        avatarOfDriverOnGreetingDialog.image = UIImage(named: "ic_avatar")
    }

    @IBOutlet weak var nameOfDriverOnGreetingDialog: UILabel!
    
    @IBOutlet weak var btnGreeting: UIButton! {
        didSet {
            btnGreeting.layer.cornerRadius = 5
        }
    }
    

    @IBAction func greetingAnotherDriver(_ sender: Any) {
        print("chao hoi nhau")
        if(flagUserNearlyDisplay==false) {
            let userPick = selectedMarkerUser
            let user = loadUserFromUserDefault()
//            print(user?.socketID)
            a.onSayHello(email: (user?.email)!, name: user!.name, receive_id: userPick!.socketID!, msg: "hello")
            UIView.transition(with: greetingDialog,
            duration: 0.5,
              options: .curveEaseOut,
              animations: {
                self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
              }) { (true) in
                self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
            }
        }
        else {
            let user = loadUserFromUserDefault()
            a.onSayHello(email: (user?.email)!, name: user!.name, receive_id: receiveSocketId, msg: "hello")
            flagUserNearlyDisplay = false
            UIView.transition(with: greetingDialog,
            duration: 0.5,
              options: .curveEaseOut,
              animations: {
                self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
              }) { (true) in
                self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
            }
        }
    }
    
    @IBAction func hadenpha(_ sender: Any) {
        print("ha den pha")
        let userPick = selectedMarkerUser
        let user = loadUserFromUserDefault()
        a.onWarnStrongLight(email: (user?.email)!, name: user!.name, receive_id: userPick!.socketID!, msg: "hello")
        UIView.transition(with: greetingDialog,
        duration: 0.5,
          options: .curveEaseOut,
          animations: {
            self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
          }) { (true) in
            self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    @IBAction func giamtocdo(_ sender: Any) {
        print("giam toc do")
        let userPick = selectedMarkerUser
        let user = loadUserFromUserDefault()
        a.onWarnSlowDown(email: (user?.email)!, name: user!.name, receive_id: userPick!.socketID!, msg: "hello")
        UIView.transition(with: greetingDialog,
        duration: 0.5,
          options: .curveEaseOut,
          animations: {
            self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
          }) { (true) in
            self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    @IBAction func cogiamsat(_ sender: Any) {
        print("co giam sat")
        let userPick = selectedMarkerUser
        let user = loadUserFromUserDefault()
        a.onWarnWatcher(email: (user?.email)!, name: user!.name, receive_id: userPick!.socketID!, msg: "hello")
        UIView.transition(with: greetingDialog,
        duration: 0.5,
          options: .curveEaseOut,
          animations: {
            self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
          }) { (true) in
            self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    @IBAction func quaydauxe(_ sender: Any) {
        print("nen quay dau xe")
        let userPick = selectedMarkerUser
        let user = loadUserFromUserDefault()
        a.onWarnTurnAround(email: (user?.email)!, name: user!.name, receive_id: userPick!.socketID!, msg: "hello")
        UIView.transition(with: greetingDialog,
        duration: 0.5,
          options: .curveEaseOut,
          animations: {
            self.greetingDialog.center.y = self.view.frame.height + self.greetingDialog.frame.height/2
          }) { (true) in
            self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    // View Of Direction Dialog
    @IBOutlet weak var directionDialog: UIView! {
        didSet {
            let grayColor:UIColor = UIColor.gray
//            directionDialog.layer.borderWidth = 1
            directionDialog.addBorder(side: .top, thickness: 1, color: grayColor)

//            directionDialog.layer.cornerRadius = 5
//            directionDialog.layer.shadowColor = UIColor.black.cgColor
//            directionDialog.layer.shadowOpacity = 1
//            directionDialog.layer.shadowOffset = .zero
//            directionDialog.layer.shadowRadius = 10
        }
    }
    
//    @IBOutlet weak var instructionLabelOfDirectView: UILabel!
    
    @IBOutlet weak var totalTimeToGo: UILabel!
    
    @IBOutlet weak var reaminDistance: UILabel!
    
    @IBOutlet weak var labelDetailOrHide: UILabel!
    
    @IBOutlet weak var listOfInstructionTableView: UITableView! {
        didSet {
            listOfInstructionTableView.backgroundColor = UIColor.white
        }
    }
    

    
    @IBAction func showDetailDirection(_ sender: Any) {
        if(labelDetailOrHide.text == "Ẩn") {
            UIView.transition(with: listInstruction,
            duration: 0.5,
              options: .curveEaseOut,
              animations: {
                self.listInstruction.center.y = self.view.frame.height + self.directionDialog.frame.height + self.listInstruction.frame.height/2
              }) { (true) in }
            listInstruction.translatesAutoresizingMaskIntoConstraints = true
            labelDetailOrHide.text = "Chi tiết các bước"
            onShowListInstruction = false
//            directionDialog.isUserInteractionEnabled = true
        }
        else {
            UIView.transition(with: listInstruction,
            duration: 0.5,
              options: .curveEaseOut,
              animations: {
                self.listInstruction.center.y = self.view.frame.height - self.directionDialog.frame.height - self.listInstruction.frame.height/2
              }) { (true) in }
                
            listInstruction.translatesAutoresizingMaskIntoConstraints = true
            labelDetailOrHide.text = "Ẩn"
            onShowListInstruction = true
//            directionDialog.isUserInteractionEnabled = false
        }
    }
    
    var onShowListInstruction = false
    
    @IBOutlet weak var listInstruction: UITableView!
    
    @IBOutlet weak var btnStartDrivingModeDirectionDialog: UIButton! {
        didSet {
            btnStartDrivingModeDirectionDialog.layer.cornerRadius = 10
            btnStartDrivingModeDirectionDialog.clipsToBounds = true
            let whiteColor:UIColor = UIColor.white
            btnStartDrivingModeDirectionDialog.layer.borderWidth = 2
            btnStartDrivingModeDirectionDialog.layer.borderColor = whiteColor.cgColor
        }
    }
    
    @IBAction func startDrivingMode(_ sender: Any) {
        updateRoute = true
    }
    
    @IBAction func closeStartDrivingMode(_ sender: Any) {
        if(onGroupTravelMode==false) {
            updateRoute = false
            mapView.animate(toViewingAngle: 0)
            for p in polyLineDirection {
    //            p.map = nil
                for eachP in p {
                    eachP.map = nil
                }
            }
            polyLineDirection.removeAll()
            if(onShowListInstruction == false ) {
                UIView.transition(with: self.directionDialog,
                  duration: 0.5,
                  options: .curveEaseInOut,
                  animations: {
                    self.directionDialog.center.y = self.view.frame.height + self.directionDialog.frame.height/2
                    self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y + self.directionDialog.frame.height
                  }) { (true) in
                    self.directionDialog.translatesAutoresizingMaskIntoConstraints = true
                    self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
                }
            }
            else {
                UIView.transition(with: listInstruction,
                duration: 0.5,
                  options: .curveEaseOut,
                  animations: {
                    self.listInstruction.center.y = self.view.frame.height + self.directionDialog.frame.height + self.listInstruction.frame.height/2
                  }) { (true) in
                    self.listInstruction.isHidden = true
                    self.listInstruction.translatesAutoresizingMaskIntoConstraints = true
                    self.labelDetailOrHide.text = "Chi tiết các bước"
                    UIView.transition(with: self.directionDialog,
                      duration: 0.5,
                      options: .curveEaseInOut,
                      animations: {
                        self.directionDialog.center.y = self.view.frame.height + self.directionDialog.frame.height/2
                        self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y + self.directionDialog.frame.height
                      }) { (true) in
                        self.directionDialog.translatesAutoresizingMaskIntoConstraints = true
                        self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
                    }
                }

            }
        }
        else {
            let alert:UIAlertController = UIAlertController(title: "Bạn có muốn rời di chuyển nhóm không ?", message: "Vui lòng xác nhận", preferredStyle: .alert)
            let btnYes:UIAlertAction = UIAlertAction(title: "Đồng ý", style: .destructive) { (btn) in
                if(self.onShowListInstruction == false ) {
                    UIView.transition(with: self.directionDialog,
                      duration: 0.5,
                      options: .curveEaseInOut,
                      animations: {
                        self.directionDialog.center.y = self.view.frame.height + self.directionDialog.frame.height/2
                      }) { (true) in
                    }
                    self.directionDialog.translatesAutoresizingMaskIntoConstraints = true
                }
                else {
                    UIView.transition(with: self.listInstruction,
                    duration: 0.5,
                      options: .curveEaseOut,
                      animations: {
                        self.listInstruction.center.y = self.view.frame.height + self.directionDialog.frame.height + self.listInstruction.frame.height/2
                      }) { (true) in
                        self.listInstruction.isHidden = true
                        self.listInstruction.translatesAutoresizingMaskIntoConstraints = true
                        self.labelDetailOrHide.text = "Chi tiết các bước"
                        UIView.transition(with: self.directionDialog,
                          duration: 0.5,
                          options: .curveEaseInOut,
                          animations: {
                            self.directionDialog.center.y = self.view.frame.height + self.directionDialog.frame.height/2
                            self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y + self.directionDialog.frame.height
                          }) { (true) in
                        }
                        self.directionDialog.translatesAutoresizingMaskIntoConstraints = true
                    }

                }
                self.onGroupTravelMode = false
                self.updateRoute = false
                self.mapView.animate(toViewingAngle: 0)
                self.bannerTravelMode.isHidden = true
                for p in self.polyLineDirection {
                    for eachP in p {
                        eachP.map = nil
                    }
                }
                self.polyLineDirection.removeAll()
            }
            let btnNo:UIAlertAction = UIAlertAction(title: "Không", style: .destructive) { (btn) in
                alert.dismiss(animated: true)
            }
            alert.addAction(btnYes)
            alert.addAction(btnNo)
            self.present(alert, animated: true, completion: nil)
        }

        

        
        
        
//        directStartView.isHidden = true
    }

//    View of Menu Side Bar
    
    @IBOutlet weak var menuSideBar: UIView!

    @IBOutlet weak var containerAvatarAndBtnChangeAvatarMenuSideBar: UIView! {
        didSet {
            containerAvatarAndBtnChangeAvatarMenuSideBar.layer.cornerRadius = containerAvatarAndBtnChangeAvatarMenuSideBar.frame.height/2

        }
    }

    @IBOutlet weak var avatarOfUserOfMenuSideBar: UIImageView! {
        didSet {
            avatarOfUserOfMenuSideBar.layer.cornerRadius = avatarOfUserOfMenuSideBar.frame.height/2
            avatarOfUserOfMenuSideBar.clipsToBounds = true
            let user = loadUserFromUserDefault()
            if(user?.avatar != nil ) {
                let imageUrl = URL(string: (user?.avatar)!)
                if(imageUrl != nil) {
                    do {
                        let imageData:Data = try Data(contentsOf: imageUrl!)
                        avatarOfUserOfMenuSideBar.image = UIImage(data: imageData)
                    }
                    catch {
                        print("khong load dc anh")
                    }
                }
            }
        }
    }

    @IBOutlet weak var btnChangeAvatarOfMenuSideBar: UIImageView! {
        didSet {
            btnChangeAvatarOfMenuSideBar.layer.cornerRadius = btnChangeAvatarOfMenuSideBar.frame.height/2
        }
    }

    @IBOutlet weak var nameOfUserOfMenuSideBar: UILabel! {
        didSet {
            let user = loadUserFromUserDefault()
            nameOfUserOfMenuSideBar.text = user?.name
        }
    }

    @IBOutlet weak var emailOfUserOfMenuSideBar: UILabel! {
        didSet {
            let user = loadUserFromUserDefault()
//            print(GIDSignIn.sharedInstance()?.currentUser.profile.email)
            if(user?.authProvider == "google") {
//                emailOfUserOfMenuSideBar.text = GIDSignIn.sharedInstance()?.currentUser.profile.email
//                emailOfUserOfMenuSideBar.addBorder(side: .bottom, thickness: 2, color: .white)
            }
            else {
                emailOfUserOfMenuSideBar.text = user?.email
                emailOfUserOfMenuSideBar.addBorder(side: .bottom, thickness: 2, color: .white)
            }
        }
    }

    @IBOutlet weak var switchShowDriverOfMenuSideBar: UISwitch! {
        didSet {
            let isOtherDriver = defaults.bool(forKey: "switchOtherDriver")

            if isOtherDriver {
                switchShowDriverOfMenuSideBar.isOn = true

            } else {
                switchShowDriverOfMenuSideBar.isOn = false
            }
        }
    }

    @IBOutlet weak var switchShowSignalMarketOfMenuSideBar: UISwitch! {
        didSet {
            let isSignal = defaults.bool(forKey: "switchShowSignal")

            if isSignal {
                switchShowSignalMarketOfMenuSideBar.isOn = true
            } else {
                switchShowSignalMarketOfMenuSideBar.isOn = false
            }
        }
    }

    @IBOutlet weak var switchShowHeavyTraffic: UISwitch! {
        didSet {
            let isHeavyTraffic = defaults.bool(forKey: "switchHeavyTraffic")

            if isHeavyTraffic {
                switchShowHeavyTraffic.isOn = true
                mapView.isTrafficEnabled = true
            } else {
                switchShowHeavyTraffic.isOn = false
                mapView.isTrafficEnabled = false
            }
        }
    }

    @IBOutlet weak var btnLogoutOfMenuSideBar: UIButton! {
        didSet {
            btnLogoutOfMenuSideBar.layer.cornerRadius = 10
            btnLogoutOfMenuSideBar.clipsToBounds = true
        }
    }

    @IBAction func changeAvatar(_ sender: Any) {
        let alert:UIAlertController = UIAlertController(title: "Chọn ?", message: "", preferredStyle: .actionSheet)
        let btnTakePhoto:UIAlertAction = UIAlertAction(title: "Chụp ảnh", style: .destructive) { (btn) in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .camera
            self.present(picker, animated: true)
        }
        let btnChooseFromLibrary:UIAlertAction = UIAlertAction(title: "Bộ sưu tập", style: .destructive) { (btn) in
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true)
        }
        let btnHuy:UIAlertAction = UIAlertAction(title: "Hủy", style: .destructive) { (btn) in
            alert.dismiss(animated: true)
        }

        alert.addAction(btnTakePhoto)
        alert.addAction(btnChooseFromLibrary)
        alert.addAction(btnHuy)

        present(alert, animated: true, completion: nil)
    }

    @IBAction func closeMenuSideBar(_ sender: Any) {
        UIView.transition(with: menuSideBar,
                          duration: 0.3,
        options: .curveEaseOut,
        animations: {
          self.menuSideBar.center.x = 0 - self.menuSideBar.frame.width/2
        }) { (true) in
//            self.navigationController?.isNavigationBarHidden = false
//            self.btnCurrentGPS.isHidden = false
            self.menuSideBar.translatesAutoresizingMaskIntoConstraints = true
        }
    }

    @IBAction func showDriverMarker(_ sender: Any) {
        if switchShowDriverOfMenuSideBar.isOn {
            defaults.set(true, forKey: "switchOtherDriver")
        } else {
            defaults.set(false, forKey: "switchOtherDriver")
            for (_, marker) in listMarkersOfNearlyDriver {
                marker.map = nil
            }
            listMarkersOfNearlyDriver.removeAll()
        }
    }

    @IBAction func showSignalMarker(_ sender: Any) {
        if switchShowSignalMarketOfMenuSideBar.isOn {
            defaults.set(true, forKey: "switchShowSignal")
        } else {
            defaults.set(false, forKey: "switchShowSignal")
            for (_, marker) in listMarkersOfReportNearly {
                marker.map = nil
            }
            listMarkersOfReportNearly.removeAll()
        }
    }

    @IBAction func showHeavyTraffic(_ sender: Any) {
        if switchShowHeavyTraffic.isOn {
            defaults.set(true, forKey: "switchHeavyTraffic")
            mapView.isTrafficEnabled = true
        } else {
            defaults.set(false, forKey: "switchHeavyTraffic")
            mapView.isTrafficEnabled = false
        }
    }

    @IBAction func logOut(_ sender: Any) {
        let defaults = UserDefaults.standard
        let user = loadUserFromUserDefault()
        if(user?.authProvider == "google") {
            GIDSignIn.sharedInstance()?.signOut()
        }
        defaults.removeObject(forKey: "User")
        defaults.removeObject(forKey: "User-Token")
        defaults.removeObject(forKey: "ListAppointment")
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "loginScreen") as! LoginViewController
        scr.modalPresentationStyle = .fullScreen
        scr.modalTransitionStyle = .crossDissolve
        self.present(scr, animated: true, completion: nil)
    }
    
    var dialogImage = UIImageView()
    
    @IBAction func zoomAvatarOfUserMenuSideBar(_ sender: Any) {
//        let imageView = self.view as! UIImageView
        dialogImage = UIImageView(image: avatarOfUserOfMenuSideBar.image)
        dialogImage.frame = UIScreen.main.bounds
        dialogImage.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dialogImage.contentMode = .scaleAspectFit
        dialogImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        dialogImage.addGestureRecognizer(tap)
        self.view.addSubview(dialogImage)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        dialogImage.removeFromSuperview()
    }
    
    
//    Search Bar and Header
    @IBOutlet weak var searchPlace: UISearchBar! {
        didSet {
            searchPlace.tintColor = UIColor.black
            searchPlace.searchTextField.textColor = UIColor.black
        }
    }
   
    @IBOutlet weak var btnMenuUser: UIImageView! {
        didSet {
//            btnMenuUser.addBorder(side: .right, thickness: 1, color: .black)
        }
    }
    
    @IBOutlet weak var listSearchPlace: UITableView! {
        didSet {
            listSearchPlace.backgroundColor = UIColor.white
        }
    }
    
    var tableDataSource: GMSAutocompleteTableDataSource?
    
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didAutocompleteWith place: GMSPlace) {
        view.endEditing(true)
        listSearchPlace.isHidden = true
        searchPlace.showsCancelButton = false
        self.destination = place.coordinate
        self.mapView.camera = GMSCameraPosition(target: place.coordinate.self, zoom: 15, bearing: 0, viewingAngle: 0)
        self.markerSearch = GMSMarker(position: place.coordinate.self)
        self.markerSearch.title = place.name
        self.markerSearch.map = self.mapView
        addressLabel.text = place.formattedAddress
        namePlaceLabel.text = place.name
        UIView.transition(with: informationOfLocationDialog,
                          duration: 0.5,
        options: .curveEaseOut,
        animations: {
            self.informationOfLocationDialog.center.y = self.view.frame.height - self.informationOfLocationDialog.frame.height/2
            self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true
            self.btnCurrentGPS.center.y = self.btnCurrentGPS.center.y - self.informationOfLocationDialog.frame.height
            self.btnCurrentGPS.translatesAutoresizingMaskIntoConstraints = true
        }) { (true) in
        }
    }
      
    func tableDataSource(_ tableDataSource: GMSAutocompleteTableDataSource, didFailAutocompleteWithError error: Error) {
        print("hihi")
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        listSearchPlace.isHidden = false
        searchPlace.showsCancelButton = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableDataSource?.sourceTextHasChanged(searchText)
        DispatchQueue.main.async {
            self.listSearchPlace.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        listSearchPlace.isHidden = true
        searchPlace.endEditing(true)
        searchPlace.text = ""
        tableDataSource?.sourceTextHasChanged("")
        DispatchQueue.main.async {
            self.listSearchPlace.reloadData()
        }
    }
    
    @IBAction func showMenuSideBarOfUser(_ sender: Any) {
//        btnCurrentGPS.isHidden = true
        UIView.transition(with: menuSideBar,
                          duration: 0.3,
        options: .curveEaseOut,
        animations: {
            self.menuSideBar.center.x = self.menuSideBar.frame.width/2
            self.menuSideBar.translatesAutoresizingMaskIntoConstraints = true

        }) { (true) in }
    }

    //    Declare variable
    let defaults = UserDefaults.standard

    let a = SocketIOManager1()

    let urlUpdateCurrentLocation = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/user/location/current")!
    
    var userAccount:User?
       
    var userToken:String!
    
    var destination: CLLocationCoordinate2D?
        
    var polyLineDirection:[[GMSPolyline]] = []
    
    var polyLineDirectionTemp:[[GMSPolyline]] = []
    
    var listRoutesInstruction:[[String]] = []
    
    var listRoutesDistance:[[String]] = []
    
    var listOfInstruction:[String] = []
    
    var listOfDistance:[String] = []
    
    var listTotalOfTime:[String] = []
    
    var listTotalOfDistance:[String] = []
        
//    var listPolyLineDirection:
    
    var appTimeInterval: Timer!

//    Flag
    
    var updateRoute = false
    
    var groupMode = false;

//    Mutable Flag
    
    var selectedMarkerReport: String!
    
    var selectedMarkerUser: User!
    
    var selectedAppointmentFromDetailAppoinment:AppointmentSpecified?
        
    var travellingMode = false
    
//    Declare for google search
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    
    var searchController: UISearchController?
        
    var resultView: UITextView?
    
//    Declare for google map
    
    var marker = GMSMarker()
    
    var markerSearch = GMSMarker()
    
    var listMarkersOfNearlyDriver:[String:GMSMarker] = [:]
    
    var listMarkersOfReportNearly:[String:GMSMarker] = [:]
    
    var listMarkerOfMemberOfGroups:[String:GMSMarker] = [:]
    
    // Declare GPS of device
    
    let locationManager = CLLocationManager()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self

        loadDataUserFromLocalStorage()
        
        initMapAndGPS()
        
        initConstraintDialog()
        
        initSearchBarAndPlaceTableDataSource()
        
        initListInstruction()
        
        registerSocket()

        if(locationLat != nil) {
            let cor = CLLocationCoordinate2D(latitude: locationLat!, longitude: locationLng!)
            destination = cor
            mapView.camera = GMSCameraPosition(target: cor, zoom: 15, bearing: 0, viewingAngle: 0)
            addressLabel.text = locationNameSpecified
            markerSearch = GMSMarker(position: cor)
            markerSearch.title = "hello"
            markerSearch.map = mapView
            DispatchQueue.main.async {
                UIView.transition(with: self.informationOfLocationDialog,
                duration: 1,
                options: .curveEaseOut,
                animations: {
                  self.informationOfLocationDialog.center.y = self.view.frame.height - self.informationOfLocationDialog.frame.height/2
                    self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true

                }) { (true) in }
            }
        }
        
        if(selectedAppointmentFromDetailAppoinment != nil) {
            let appointmentCoor = CLLocationCoordinate2D(latitude: (selectedAppointmentFromDetailAppoinment?.location?.coordinates![1])!, longitude: (selectedAppointmentFromDetailAppoinment?.location?.coordinates![0])!)
            self.destination = appointmentCoor
            print("doan nay chay khong day dkm")
            print(appointmentCoor)
            DispatchQueue.main.async {
                self.mapView.camera = GMSCameraPosition(target: appointmentCoor, zoom: 15, bearing: 0, viewingAngle: 0)
                self.mapView.animate(toLocation: appointmentCoor)
            }
            print("chay k ")
            self.markerSearch = GMSMarker(position: appointmentCoor)
            self.markerSearch.map = self.mapView
            addressLabel.text = selectedAppointmentFromDetailAppoinment?.locationAddress
            namePlaceLabel.text = selectedAppointmentFromDetailAppoinment?.locationName
            selectedAppointmentFromDetailAppoinment = nil
            UIView.transition(with: informationOfLocationDialog,
                              duration: 0.5,
            options: .curveEaseOut,
            animations: {
                self.informationOfLocationDialog.center.y = self.view.frame.height - self.informationOfLocationDialog.frame.height/2
                self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true
            }) { (true) in
            }
        }
        
    }
    
    @objc func runCode() {
        print("dkhghjghjfghkffghm")
    }
    func loadDataUserFromLocalStorage() {
        self.userAccount = loadUserFromUserDefault()
        self.userToken = loadUserTokenFromUserDefault()
    }
    
    func initMapAndGPS() {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        mapView.delegate = self
    }
    
    func initSearchBarAndPlaceTableDataSource() {
        searchPlace.delegate = self
        tableDataSource = GMSAutocompleteTableDataSource()
        tableDataSource?.delegate = self
        
        tableDataSource?.primaryTextColor = .black
        tableDataSource?.secondaryTextColor = .red
        tableDataSource?.primaryTextHighlightColor = .gray
        
        listSearchPlace.dataSource = tableDataSource
        listSearchPlace.delegate = tableDataSource
    }
    
    func initConstraintDialog() {
        informationOfReportMarkerDialog.translatesAutoresizingMaskIntoConstraints = true
        greetingDialog.translatesAutoresizingMaskIntoConstraints = true
        menuSideBar.translatesAutoresizingMaskIntoConstraints = true
        directionDialog.translatesAutoresizingMaskIntoConstraints = true

    }
    
    func initListInstruction() {
        listInstruction.dataSource = self
        listInstruction.delegate = self
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("asdasd")
        autoOpenGroupMode()
        appTimeInterval = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(updateFrequency), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        appTimeInterval.invalidate()
    }
    
    @objc func doAfterFindLocation() {
        print("I've been notified")
        destination = locationDestination
    }
    
    //Func for socket
    func registerSocket() {
        NotificationCenter.default.addObserver(self,
        selector: #selector(doAfterFindLocation),
        name: NSNotification.Name(rawValue: myFindLocationKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveHelloSocket),
        name: NSNotification.Name(rawValue: myHelloSocKetKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveStrongLightSocket),
        name: NSNotification.Name(rawValue: myStrongLightSocketKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveWatcherSocket),
        name: NSNotification.Name(rawValue: myWatcherSocketKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveSlowDownSocket),
        name: NSNotification.Name(rawValue: mySlowDownSocketKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveTurnAroundSocket),
        name: NSNotification.Name(rawValue: myTurnAroundSocketKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(receiveThanksSocket),
        name: NSNotification.Name(rawValue: myThanksSocketKey),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(showUserCommunity),
        name: NSNotification.Name(rawValue: myShowUserCommunityKey),
        object: nil)
    }
    
    var appointmentTravelMode:Appointment?
    
    func autoOpenGroupMode() {
        let appointmentTravel = findTheNearlestAppointmentTravel()
        print(appointmentTravel)
        if(appointmentTravel != nil) {
            for (timeInterVal,appointmentInfo) in appointmentTravel!{
                print(timeInterVal)
                print(appointmentInfo)
                let timer = Timer.scheduledTimer(timeInterval: timeInterVal, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
                appointmentTravelMode = appointmentInfo
                RunLoop.current.add(timer, forMode: .common)
            }
        }
    }
    
    @IBOutlet weak var bannerTravelMode: UIView!
    
    var onGroupTravelMode = false
    
    @objc func timerAction() {
        print("-----------")
        let coordinateOfAppointmentTravelMode = CLLocationCoordinate2D(latitude: (appointmentTravelMode?.location?.coordinates![1])!, longitude: (appointmentTravelMode?.location?.coordinates![0])!)
        destination = coordinateOfAppointmentTravelMode
        bannerTravelMode.isHidden = false
        onGroupTravelMode = true
        getLocationOfMemberInAppointmentGroupTravelling(groupIDOfAppointment: (appointmentTravelMode?.group)!)
        UIView.transition(with: self.directionDialog,
          duration: 0.5,
          options: .curveEaseInOut,
          animations: {
            self.directionDialog.center.y = self.view.frame.height - self.directionDialog.frame.height/2
          }) { (true) in
            self.listInstruction.isHidden = false
            self.mapView.camera = GMSCameraPosition(target: self.locationManager.location!.coordinate, zoom: 25, bearing: 0, viewingAngle: 0)
            self.mapView.animate(toLocation: self.locationManager.location!.coordinate)
            self.markerSearch = GMSMarker(position: self.destination!)
            self.markerSearch.map = self.mapView
            self.drawRoute(src: self.locationManager.location!.coordinate, dst: self.destination!)
        }
        directionDialog.translatesAutoresizingMaskIntoConstraints = true
    }

    @objc func receiveHelloSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpHello") as! PopUpEventHelloViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }

    @objc func receiveStrongLightSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpStrongLight") as! PopUpEventStrongLightViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }
    
    @objc func receiveWatcherSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpWatcher") as! PopUpEventWatcherViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }
    
    @objc func receiveSlowDownSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpSlowDown") as! PopUpEventSlowDownViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }
    
    @objc func receiveTurnAroundSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpTurnAround") as! PopUpEventTurnAroundViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }
    
    @objc func receiveThanksSocket() {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        let scr = storyboard.instantiateViewController(identifier: "popUpThanks") as! PopUpEventThanksViewController
        scr.modalPresentationStyle = .overFullScreen
        self.present(scr, animated: false, completion: nil)
    }
    
    var flagUserNearlyDisplay = false

    @objc func showUserCommunity() {
        flagUserNearlyDisplay = true
        let imageUrl = URL(string: url)
        if(imageUrl != nil) {
            do {
                let imageData:Data = try Data(contentsOf: imageUrl!)
                self.avatarOfDriverOnGreetingDialog.image = UIImage(data: imageData)
            }
            catch {
                print("khong load dc anh")
            }
        }
        self.nameOfDriverOnGreetingDialog.text = nameUser
        UIView.transition(with: self.greetingDialog,
            duration: 0.5,
        options: .curveEaseOut,
        animations: {
          self.greetingDialog.center.y = self.view.frame.height - self.greetingDialog.frame.height/2
            self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true

        }) { (true) in }
    }
    
    @objc func updateFrequency() {
        
        let latUser = locationManager.location?.coordinate.latitude
        let lngUser = locationManager.location?.coordinate.longitude
        
        let latitudeNum = Double(latUser!)
        let longitudeNum = Double(lngUser!)
        
        updateUserLocationToServer(lat: latUser!, lng: lngUser!)
                
        if(switchShowDriverOfMenuSideBar.isOn == true) {
            getUserNearly(latitudeNum: latitudeNum, longitudeNum: longitudeNum)
        }

        if(switchShowSignalMarketOfMenuSideBar.isOn == true) {
            getReportMearly(latitudeNum: latitudeNum, longitudeNum: longitudeNum)
        }

      }
    
    func updateUserLocationToServer(lat: CLLocationDegrees, lng: CLLocationDegrees) {
        let latitudeNum = lat
        let longitudeNum = lng
        
        self.userAccount?.currentLocation?.coordinates![0] = longitudeNum
        self.userAccount?.currentLocation?.coordinates![1] = latitudeNum
        
        saveUserToUserDefault(User: userAccount!)
        
        let jsoncoordinate:[String:[Double]] = [
            "coordinates" : [longitudeNum,latitudeNum]
        ]
        let jsonData:[String:[String:[Double]]] = [
            "currentLocation" : jsoncoordinate
        ]
        let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)

        var request = URLRequest(url: urlUpdateCurrentLocation)
        request.httpMethod = "PUT"
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        request.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let _ = String(data: data, encoding: .utf8) {
                }
            }
        }
        task.resume()
    }
    
    func getUserNearly(latitudeNum: Double,longitudeNum: Double) {
        let latitudeString = String(latitudeNum)
        let longitudeString = String(longitudeNum)
        let urlNearly = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/user/nearby?"+"lng="+longitudeString+"&lat="+latitudeString+"&radius=50000")!
        print(urlNearly)
        var requestNearly = URLRequest(url: urlNearly)
        requestNearly.httpMethod = "GET"
        requestNearly.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let taskUser = URLSession.shared.dataTask(with: requestNearly) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        let responseGetUserNearlyData = Data(dataString.utf8)
                        let responseGetUserNearly = try JSONDecoder().decode(ResponseGetUserNearly.self, from: responseGetUserNearlyData)
                        let listUserNearly = responseGetUserNearly.users
                        saveListUserNearlyToUserDefault(ListUser: listUserNearly)
                        for userNearly in listUserNearly {
                            let lng = userNearly.currentLocation?.coordinates![0]
                            let lat = userNearly.currentLocation?.coordinates![1]
                            let position = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
                            let isExistUserNearly = self.listMarkersOfNearlyDriver[userNearly.id]
                            if(isExistUserNearly==nil) {
                                let marker = GMSMarker()
                                self.listMarkersOfNearlyDriver.updateValue(marker, forKey: userNearly.id)
                                self.listMarkersOfNearlyDriver[userNearly.id]?.position = position
                                self.listMarkersOfNearlyDriver[userNearly.id]?.icon = UIImage(named: "ic_marker_bus_44dp")
                                self.listMarkersOfNearlyDriver[userNearly.id]?.title = "driver"
                                self.listMarkersOfNearlyDriver[userNearly.id]?.map = self.mapView
                                self.listMarkersOfNearlyDriver[userNearly.id]?.userData = userNearly
                            } else {
                                self.listMarkersOfNearlyDriver[userNearly.id]?.position = position
                                self.listMarkersOfNearlyDriver[userNearly.id]?.icon = UIImage(named: "ic_marker_bus_44dp")
                                self.listMarkersOfNearlyDriver[userNearly.id]?.title = "driver"
                                self.listMarkersOfNearlyDriver[userNearly.id]?.map = self.mapView
                                self.listMarkersOfNearlyDriver[userNearly.id]?.userData = userNearly
                            }
                        }
                    }
                    catch {
                        print("Can not decode data")
                    }
                }
            }
        }
        taskUser.resume()
    }
    
    func getReportMearly(latitudeNum: Double,longitudeNum: Double) {
        let latitudeString = String(latitudeNum)
        let longitudeString = String(longitudeNum)
        let urlNearly = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/report/nearby?userID="+userAccount!.id+"&lng="+longitudeString+"&lat="+latitudeString+"&radius=500000")!
//            print(urlNearly)
        var requestNearly = URLRequest(url: urlNearly)
        requestNearly.httpMethod = "GET"
        requestNearly.setValue(userToken, forHTTPHeaderField: "x-access-token")
        let taskUser = URLSession.shared.dataTask(with: requestNearly) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        print(dataString)
                        let listMarkerSignalNearlyData = Data(dataString.utf8)
                        let listMarkerSignalNearly = try JSONDecoder().decode(ResponseReport.self, from: listMarkerSignalNearlyData)
                        let listSignalMarkers = listMarkerSignalNearly.reports
                        for signalMarker in listSignalMarkers {
                            let lng = signalMarker.geometry.coordinates![0]
                            let lat = signalMarker.geometry.coordinates![1]
                            let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                            let isExistMarker = self.listMarkersOfReportNearly[signalMarker.id]
                            if(isExistMarker==nil) {
                                let marker = GMSMarker()
                                self.listMarkersOfReportNearly.updateValue(marker, forKey: signalMarker.id)
                                self.listMarkersOfReportNearly[signalMarker.id]?.position = position
                                self.listMarkersOfReportNearly[signalMarker.id]?.icon = UIImage(named: "icon"+signalMarker.type!)
                                self.listMarkersOfReportNearly[signalMarker.id]?.title = "report"
                                self.listMarkersOfReportNearly[signalMarker.id]?.map = self.mapView
                                self.listMarkersOfReportNearly[signalMarker.id]?.userData = signalMarker
                            } else {
                                self.listMarkersOfReportNearly[signalMarker.id]?.position = position
                                self.listMarkersOfReportNearly[signalMarker.id]?.icon = UIImage(named: "icon"+signalMarker.type!)
                                self.listMarkersOfReportNearly[signalMarker.id]?.title = "report"
                                self.listMarkersOfReportNearly[signalMarker.id]?.map = self.mapView
                                self.listMarkersOfReportNearly[signalMarker.id]?.userData = signalMarker
                            }

                        }
                    }
                    catch {
                        print("Can not decode data")
                    }
                }
            }
        }
        taskUser.resume()
    }

    func drawRoute(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D){
        
        for p in polyLineDirection {
//            p.map = nil
            for eachP in p {
                eachP.map = nil
            }
        }
        polyLineDirection.removeAll()

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&alternatives=true&mode=driving&language=vi&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!
        print(url)

        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {

                        let preRoutes = json["routes"] as! NSArray
                        let count = preRoutes.count
                        if(count == 0) {
                            DispatchQueue.main.async {
                                self.totalTimeToGo.text = "Không có đường đi"
                                self.reaminDistance.text = "Không có đường đi"
                                self.btnStartDrivingModeDirectionDialog.isEnabled = false
                            }
                        }
                        else {
                            for n in 0...count-1 {
                                let routes = preRoutes[n] as! NSDictionary
                                let preLegs = routes.value(forKey: "legs") as! NSArray
                                let legs = preLegs[0] as! NSDictionary
                                let distance = legs.value(forKey: "distance") as! NSDictionary
                                let duration = legs.value(forKey: "duration") as! NSDictionary
                                
                                let distanceText = distance.value(forKey: "text") as! String
                                let durationText = duration.value(forKey: "text") as! String
                                
                                self.listTotalOfDistance.append(distanceText)
                                self.listTotalOfTime.append(durationText)
                                
                                if(n==0) {
                                    DispatchQueue.main.async {
                                        self.totalTimeToGo.text = durationText
                                        self.reaminDistance.text = distanceText
                                    }
                                }
                                let steps:NSArray = legs.value(forKey: "steps") as! NSArray
                                var i = 0
                                let listPoly = [GMSPolyline()]
                                let listInstruct = [String()]
                                let listDistance = [String()]
                                for step in steps {
                                    let prePolyline = step as! NSDictionary
                                    let polyline = prePolyline.value(forKey: "polyline") as! NSDictionary
                                    let tempPolyline = polyline.object(forKey: "points") as! String
                                    let htlmInstruction = prePolyline.value(forKey: "html_instructions") as! String
                                    let distanceOfEachInstruction = prePolyline.value(forKey: "distance") as! NSDictionary
                                    let distanceOfEachInstructionText = distanceOfEachInstruction.value(forKey: "text") as! String

                                    self.listRoutesInstruction.append(listInstruct)
                                    self.listRoutesDistance.append(listDistance)
                                    
                                    self.listRoutesInstruction[n].append(htlmInstruction)
                                    self.listRoutesDistance[n].append(distanceOfEachInstructionText)
                                    
                                    if(n==0) {
                                        self.listOfInstruction.append(htlmInstruction)
                                        self.listOfDistance.append(distanceOfEachInstructionText)
                                    }
                                    
                                    let path = GMSPath(fromEncodedPath: tempPolyline)
                                    let polyline1 = GMSPolyline()
                                    self.polyLineDirection.append(listPoly)
                                    self.polyLineDirection[n].append(polyline1)
                                    self.polyLineDirection[n][i].path = path
                                    self.polyLineDirection[n][i].isTappable = true
                                    self.polyLineDirection[n][i].geodesic = true
                                    self.polyLineDirection[n][i].strokeWidth = 5.0
                                    print("-----------")
                                    print(n)
                                    if(n==0) {
                                        self.polyLineDirection[n][i].strokeColor = UIColor.blue
                                        self.polyLineDirection[n][i].zIndex = 1
                                        print("xanh")
                                    }
                                    else {
                                        self.polyLineDirection[n][i].strokeColor = UIColor.gray
                                        self.polyLineDirection[n][i].zIndex = 0
                                        print("xam")
                                    }
                                    self.polyLineDirection[n][i].map = self.mapView
                                    i+=1
                                }
                                for poly in self.polyLineDirection[n] {
                                    let polyData:[[GMSPolyline]:Int] = [self.polyLineDirection[n]:n]
                                    poly.userData = polyData
                                }
        
//                            else {
//                                var i = 0
//                                for step in steps {
//                                    let prePolyline = step as! NSDictionary
//                                    let polyline = prePolyline.value(forKey: "polyline") as! NSDictionary
//                                    let tempPolyline = polyline.object(forKey: "points") as! String
//                                    let htlmInstruction = prePolyline.value(forKey: "html_instructions") as? String
//                                    let distanceOfEachInstruction = prePolyline.value(forKey: "distance") as! NSDictionary
//                                    let distanceOfEachInstructionText = distanceOfEachInstruction.value(forKey: "text") as! String
//                                    self.listOfInstruction.append(htlmInstruction!)
//                                    self.listOfDistance.append(distanceOfEachInstructionText)
//                                    let path = GMSPath(fromEncodedPath: tempPolyline)
//                                    let polyline1 = GMSPolyline()
//                                    self.polyLineDirectionTemp[n].append(polyline1)
//                                    self.polyLineDirectionTemp[n][i].path = path
//                                    self.polyLineDirectionTemp[n][i].strokeWidth = 10.0
//                                    self.polyLineDirectionTemp[n][i].strokeColor = UIColor.blue
//                                    self.polyLineDirectionTemp[n][i].map = self.mapView
//                                    i+=1
//                                }
//                                for polyLine in self.polyLineDirection {
//                                    polyLine.map = nil
//                                }
//                                self.polyLineDirection.removeAll()
//                                self.polyLineDirection = self.polyLineDirectionTemp
//                            }
                        }

                        }
                    }

                    DispatchQueue.main.async {
                        self.listInstruction.reloadData()
                    }
//                    for poly in self.polyLineDirection[0] {
//                        poly.strokeColor = UIColor.blue
//                    }
                }
                
                catch {
                    print("parsing error")
                }
            }
        })
        task.resume()
        
        CATransaction.begin()
        CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
        mapView.animate(toZoom: 25)
        mapView.animate(toLocation: self.locationManager.location!.coordinate)
        CATransaction.commit()
        
    }
    
    func reDraw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D) {
        
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=driving&language=vi&key=AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")!
//        let task = session.dataTask(with: url, completionHandler: {
//            (data, response, error) in
//            if error != nil {
//                print(error!.localizedDescription)
//            } else {
//                do {
//                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
//                        let preRoutes = json["routes"] as! NSArray
//                        let routes = preRoutes[0] as! NSDictionary
//                        let preLegs = routes.value(forKey: "legs") as! NSArray
//                        let legs = preLegs[0] as! NSDictionary
//                        let steps:NSArray = legs.value(forKey: "steps") as! NSArray
//                        if(self.polyLineDirection==[]) {
//                            var i = 0
//                            for step in steps {
//                                let prePolyline = step as! NSDictionary
//                                let polyline = prePolyline.value(forKey: "polyline") as! NSDictionary
//                                let tempPolyline = polyline.object(forKey: "points") as! String
//                                let htlmInstruction = prePolyline.value(forKey: "html_instructions") as? String
//                                DispatchQueue.main.sync {
//                                    if(i==0) {
//                                        let encodedData = htlmInstruction!.data(using: String.Encoding.utf8)!
//                                        do {
//                                            let attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
//                                            self.instructionLabelOfDirectView.attributedText = attributedString
//                                        } catch let error as NSError {
//                                            print(error.localizedDescription)
//                                        } catch {
//                                            print("error")
//                                        }
//                                    }
//                                }
//                                let path = GMSPath(fromEncodedPath: tempPolyline)
//                                let polyline1 = GMSPolyline()
//                                self.polyLineDirection.append(polyline1)
//                                self.polyLineDirection[i].path = path
//                                self.polyLineDirection[i].strokeWidth = 10.0
//                                self.polyLineDirection[i].strokeColor = UIColor.blue
//                                self.polyLineDirection[i].map = self.mapView
//                                i+=1
//                            }
//
//                        }
//                        else {
//                            var polyLineDirectionTemp:[GMSPolyline] = []
//                            var i = 0
//                            for step in steps {
//                                let prePolyline = step as! NSDictionary
//                                let polyline = prePolyline.value(forKey: "polyline") as! NSDictionary
//                                let tempPolyline = polyline.object(forKey: "points") as! String
//                                let htlmInstruction = prePolyline.value(forKey: "html_instructions") as? String
//                                DispatchQueue.main.sync {
//                                    if(i==0) {
//                                        let encodedData = htlmInstruction!.data(using: String.Encoding.utf8)!
//                                        do {
//                                            let attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
//                                            self.instructionLabelOfDirectView.attributedText = attributedString
//                                        } catch let error as NSError {
//                                            print(error.localizedDescription)
//                                        } catch {
//                                            print("error")
//                                        }
//                                    }
//                                }
//                                let path = GMSPath(fromEncodedPath: tempPolyline)
//                                let polyline1 = GMSPolyline()
//                                polyLineDirectionTemp.append(polyline1)
//                                polyLineDirectionTemp[i].path = path
//                                polyLineDirectionTemp[i].strokeWidth = 10.0
//                                polyLineDirectionTemp[i].strokeColor = UIColor.blue
//                                polyLineDirectionTemp[i].map = self.mapView
//                                i+=1
//                            }
//                            for polyLine in self.polyLineDirection {
//                                polyLine.map = nil
//                            }
//                            self.polyLineDirection.removeAll()
//                            self.polyLineDirection = polyLineDirectionTemp
//                        }
//                    }
//                }
//                catch {
//                    print("parsing error")
//                }
//            }
//        })
//        task.resume()

    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
//    }
    
}
// Delegates to handle events for the location manager.
// MARK: - CLLocationManagerDelegate

extension ViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedAlways else {
      return
    }
    // 4
    locationManager.startUpdatingLocation()
    locationManager.startUpdatingHeading()
//    print(selectedAppointmentFromDetailAppoinment)
//    if(selectedAppointmentFromDetailAppoinment == nil) {
    mapView.camera = GMSCameraPosition(target: locationManager.location!.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    mapView.animate(toLocation: locationManager.location!.coordinate)

    

    
    //5
    mapView.isMyLocationEnabled = true
    
  }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if(updateRoute == true) {
            mapView.animate(toZoom: 20)
            mapView.animate(toBearing: newHeading.magneticHeading)
            mapView.animate(toViewingAngle: 45)
        }
    }
  // 6
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
    if(updateRoute == true) {
        mapView.animate(toLocation: location.coordinate)
        if(destination != nil) {
            reDraw(src: location.coordinate, dst: destination!)
        }
        print("reDraw")
    }
    
    if(groupMode == true) {
        print("drive mode")
    }
  
//
//    }
    //update current location of user's using
//    let latitudeNum = location.coordinate.latitude
//    let longitudeNum = location.coordinate.longitude
//    print(latitudeNum)
//    print(longitudeNum)
//    updateUserLocationToServer(lat: location.coordinate.latitude, lng: location.coordinate.longitude)

    // 8
//    locationManager.stopUpdatingLocation()
  }
    
  func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        
  }
    
    
}

//extension ViewController: GMSAutocompleteResultsViewControllerDelegate {
//  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
//                         didAutocompleteWith place: GMSPlace) {
//    searchController?.isActive = false
//    // Do something with the selected place.
//
//
//
//    self.destination = place.coordinate
//    self.mapView.camera = GMSCameraPosition(target: place.coordinate.self, zoom: 15, bearing: 0, viewingAngle: 0)
//    self.markerSearch = GMSMarker(position: place.coordinate.self)
//    self.markerSearch.title = place.name
//    self.markerSearch.map = self.mapView
//    addressLabel.text = place.formattedAddress
//    UIView.transition(with: informationOfLocationDialog,
//                      duration: 0.5,
//    options: .curveEaseOut,
//    animations: {
//        self.informationOfLocationDialog.center.y = self.view.frame.height - self.informationOfLocationDialog.frame.height/2
//        self.informationOfLocationDialog.translatesAutoresizingMaskIntoConstraints = true
//    }) { (true) in
//    }
//
//
//  }
//
//  func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
//                         didFailAutocompleteWithError error: Error){
//    // TODO: handle the error.
//    print("Error: ", error.localizedDescription)
//  }
//
//  func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
////    print("tapped")
//
//  }
//
//  func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
////    print("Untapped")
//
//  }
//}

extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if(marker.title == "driver") {
            DispatchQueue.main.async {
                let user = marker.userData as! User
                self.selectedMarkerUser = user
                mapView.animate(toLocation: CLLocationCoordinate2D(latitude: (user.currentLocation?.coordinates![1])!, longitude: (user.currentLocation?.coordinates![0])!))
                if(user.avatar != nil) {
                    let imageUrl = URL(string: user.avatar!)
                    if(imageUrl != nil) {
                        do {
                            let imageData:Data = try Data(contentsOf: imageUrl!)
                            self.avatarOfDriverOnGreetingDialog.image = UIImage(data: imageData)
                        }
                        catch {
                            print("khong load dc anh")
                        }
                    }
                }
                self.nameOfDriverOnGreetingDialog.text = user.name
                UIView.transition(with: self.greetingDialog,
                    duration: 0.5,
                options: .curveEaseOut,
                animations: {
                  self.greetingDialog.center.y = self.view.frame.height - self.greetingDialog.frame.height/2
                    self.greetingDialog.translatesAutoresizingMaskIntoConstraints = true

                }) { (true) in }
            }

            
        }
        
        if(marker.title == "report") {
            let detailMarker = marker.userData as! Report
            typeLabelMarkerOfReportMarkerDialog.text = detailMarker.type
            imageOfReportMarkerDialog.image = UIImage(named: detailMarker.type!)
            selectedMarkerReport = detailMarker.id
            if(detailMarker.type == "hazard") {
                typeLabelMarkerOfReportMarkerDialog.text = "NGUY HIỂM"
            }
            else if(detailMarker.type == "trafficjam") {
                typeLabelMarkerOfReportMarkerDialog.text = "KẸT XE"
            }
            else if(detailMarker.type == "crash") {
                typeLabelMarkerOfReportMarkerDialog.text = "TAI NẠN"
            }
            else {
                typeLabelMarkerOfReportMarkerDialog.text = "GIÚP ĐỠ"
            }
            if(detailMarker.distance! < Double(1000)) {
                distanceLabelMarkerOfReportMarkerDialog.text = "Cách " + String(format: "%.1f", detailMarker.distance!)+"m"
            }
            else {
                let distanceKm = detailMarker.distance!/1000
                distanceLabelMarkerOfReportMarkerDialog.text = "Cách " + String(format: "%.1f", distanceKm)+"km"
            }
            imageOfReportMarkerDialog.image = UIImage(named: detailMarker.type!)
            UIView.transition(with: informationOfReportMarkerDialog,
                              duration: 0.3,
            options: .curveEaseOut,
            animations: {
              self.informationOfReportMarkerDialog.center.y = self.informationOfReportMarkerDialog.frame.height/2
                self.informationOfReportMarkerDialog.translatesAutoresizingMaskIntoConstraints = true

            }) { (true) in }
        }
//        print(marker.title!)
        if(marker.title == "member") {
            print("hello")
        }
        
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        let poly = overlay.userData as! [[GMSPolyline]:Int]
        for p in polyLineDirection {
            for eachP in p {
                eachP.strokeColor = UIColor.gray
                eachP.zIndex = 0
             }
        }
        for (pol,n) in poly {
            for p in pol {
                p.strokeColor = UIColor.blue
                p.zIndex = 1
            }
            listOfInstruction = listRoutesInstruction[n]
            listOfDistance = listRoutesDistance[n]
            DispatchQueue.main.async {
                self.listInstruction.reloadData()
                self.reaminDistance.text = self.listTotalOfDistance[n]
                self.totalTimeToGo.text = self.listTotalOfTime[n]
            }
        }
        
        

    }
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//    }
    
}

extension ViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let image = info[.editedImage] as? UIImage else { return }

            let imageName = UUID().uuidString
            let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)

            if let jpegData = image.jpegData(compressionQuality: 0.8) {
                try? jpegData.write(to: imagePath)
            }
            print("hinh anh la ")
            print(imageName)
            print(imagePath)
            let imageFile = image.jpegData(compressionQuality: 0.8)
            let storage = Storage.storage()

                // Create a storage reference from our storage service
            let storageRef = storage.reference()
            let user = loadUserFromUserDefault()
            
            let imagePathOnFirebase = "images/User/UserAvatar/"+user!.id
            let riversRef = storageRef.child(imagePathOnFirebase)
            
            _ = riversRef.putData(imageFile!, metadata: nil) { (metadata, error) in
              guard let metadata = metadata else {
                // Uh-oh, an error occurred!
                return
              }
              // Metadata contains file metadata such as size, content-type.
                _ = metadata.size
              // You can also access to download URL after upload.
              riversRef.downloadURL { (url, error) in
                guard url != nil else {
                  // Uh-oh, an error occurred!
                  return
                }
                let urlString = url!.absoluteString
                let jsonData:[String:String] = [
                     "avatar" : urlString
                 ]
                let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)
                
                let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/profile/picture"
                
                let userToken = loadUserTokenFromUserDefault()
                let url1 = URL(string: subUrl)
                var request = URLRequest(url: url1!)
                request.httpMethod = "POST"
                request.setValue(userToken, forHTTPHeaderField: "x-access-token")
                request.allHTTPHeaderFields = [
                   "Content-Type": "application/json",
                   "Accept": "application/json"
                ]
                
                let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                    if error != nil {
                        print("/error")
                        print("co loi gi khong ")
                    }
                    else {
                        if let response = response as? HTTPURLResponse {
                            print("statusCode: \(response.statusCode)")
                        }
                        if let data = data, let _ = String(data: data, encoding: .utf8) {
                            print("up anh dc chua")
                         }
                     }
                }
                task.resume()
              }
            }
//            avatarOfUserOfMenuSideBar.image = image
            dismiss(animated: true)

        }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

}

extension ViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfInstruction.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListOfInstructionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "listInstruction") as! ListOfInstructionTableViewCell
        let encodedData = listOfInstruction[indexPath.row].data(using: String.Encoding.utf8)!
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            print(attributedString.string)
//            cell.instruction.attributedText.attribute(.font, at: 20, effectiveRange: nil)
            cell.instruction.text = attributedString.string
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error")
        }
        cell.distance.text = String(listOfDistance[indexPath.row])
        return cell
    }


}
