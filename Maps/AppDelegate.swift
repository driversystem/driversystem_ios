//
//  AppDelegate.swift
//  Maps
//
//  Created by Phạm Viết Lực on 3/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let navi = storyboard.instantiateViewController(identifier: "navigation") as! UINavigationController
//        let scr = storyboard.instantiateViewController(identifier: "mainActivity") as! ViewController
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyCRJbSPJkLOQYy0zUul1n6KK_USWkXr4uI")
        GMSPlacesClient.provideAPIKey("AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY")
        FirebaseApp.configure()
        GIDSignIn.sharedInstance()?.clientID = "804226360917-og9hb3u5smb5vr9pjee1mk8bftkmuor1.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
//       AIzaSyCRJbSPJkLOQYy0zUul1n6KK_USWkXr4uI
//       AIzaSyADSsssGnDH1zx1ybVDg_FtzBfTt7JV6lY
        
        
        
        return true
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      return GIDSignIn.sharedInstance().handle(url)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
//      let userId = user.userID                  // For client-side use only!
//      let idToken = user.authentication.idToken // Safe to send to the server
//      let fullName = user.profile.name
//      let givenName = user.profile.givenName
//      let familyName = user.profile.familyName
//      let email = user.profile.email
        let rqBody = [
            "authProvider" : "google",
            "providerUID" : user.userID,
            "name" : user.profile.name,
         ] as [String : String]
        let dataJson = try! JSONSerialization.data(withJSONObject: rqBody, options: .prettyPrinted)
        let url = URL(string: "https://driversystemapp.herokuapp.com/api-user/v1/auth/provider_auth")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
            if error != nil {
                print("/error")
            }
            else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print(dataString)
                    let jsonData = Data(dataString.utf8)
                    do {
                        let responseSignInByGoogle = try JSONDecoder().decode(ResponseSignInByGoogle.self, from: jsonData)
//                        responseSignInByGoogle.success 
                        saveUserTokenToUserDefault(token: responseSignInByGoogle.token)
                        saveUserToUserDefault(User: responseSignInByGoogle.user!)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: signInGoogleKey), object: nil)
                    }
                    catch {
                        print("can not decode data")
                    }

                }
            }
        }
        task.resume()
 
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }


}

