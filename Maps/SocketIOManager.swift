//
//  SocketIOManager.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/14/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
import SocketIO



let myHelloSocKetKey = "myHelloSocketKey"
let myStrongLightSocketKey = "myStrongLightSocketKey"
let myWatcherSocketKey = "myWatcherSocketKey"
let mySlowDownSocketKey = "mySlowDownSocketKey"
let myTurnAroundSocketKey = "myTurnAroundSocketKey"
let myThanksSocketKey = "myThanksSocketKey"
var receiveID = ""
var emailOfSocket = ""

class SocketIOManager1: NSObject {

    static let manager = SocketManager(socketURL: URL(string: "https://driversystemapp.herokuapp.com")!, config: [.log(false), .compress,])
        
    override init() {
        
        let socket = SocketIOManager1.manager.defaultSocket
        
        socket.on("connect") {data, ack in
            let subUrl = "https://driversystemapp.herokuapp.com/api-user/v1/user/updateSocketID"
            let userToken = loadUserTokenFromUserDefault()

            let jsonData:[String:String] = [
                "socketID" :socket.sid
            ]
            let dataJson = try! JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted)


            let url = URL(string: subUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "PUT"
            request.setValue(userToken, forHTTPHeaderField: "x-access-token")
            request.allHTTPHeaderFields = [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]

            let task = URLSession.shared.uploadTask(with: request, from: dataJson) {  (data,response,error) in
                if error != nil {
                    print("/error")
                    print("co loi gi khong ")
                }
                else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {            do {
                            print("update socket")
                            print(dataString)
                            let jsonData = Data(dataString.utf8)
                            let updateSocket = try JSONDecoder().decode(ResponseUpdateSocketId.self, from: jsonData)
                            let user = updateSocket.user
                            saveUserToUserDefault(User: user)
                        }  catch {
                            
                        }
                    }
                }
            }
            task.resume()
        }

        socket.on("chat message") {(data, ack) in
            print(data)
            print("chat message")
        }
        socket.on("event_hello_socket") { (data, ack) in
            print(data.count)
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: myHelloSocKetKey), object: nil)
        }
        
        socket.on("event_warn_strong_light_socket") { data, ack in
            print(data)
            print("event_warn_strong_light_server")
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: myStrongLightSocketKey), object: nil)
        }

        socket.on("event_warn_watcher_socket") {data, ack in
            print(data)
            print("event_warn_watcher_server")
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: myWatcherSocketKey), object: nil)
        }

        socket.on("event_warn_slow_down_socket") {data, ack in
            print(data)
            print("event_warn_slow_down_server")
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: mySlowDownSocketKey), object: nil)
        }

        socket.on("event_warn_turn_around_socket") {data, ack in
            print(data)
            print("event_warn_turn_around_server")
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: myTurnAroundSocketKey), object: nil)
        }

        socket.on("event_warn_thank_socket") {data, ack in
            print(data)
            print("event_warn_thank_server")
            receiveID = data[2] as! String
            emailOfSocket = data[0] as! String
            NotificationCenter.default.post(name: Notification.Name(rawValue: myThanksSocketKey), object: nil)
        }

    }

    func establishConnection() {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.connect()
    }

    func closeConnection() {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.disconnect()
    }
    
    func onSayHello(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        print(socket.sid)
        socket.emit("event_hello_server",email,name,socket.sid,receive_id,msg)
    }
    
    func onWarnStrongLight(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_strong_light_server",email,name,socket.sid,receive_id,"strong light")
    }
    
    func onWarnWatcher(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_watcher_server",email,name,socket.sid,receive_id,"watcher")
    }
    
    func onWarnSlowDown(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_slow_down_server",email,name,socket.sid,receive_id,"slow down")
    }
    
    func onWarnTurnAround(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_turn_around_server",email,name,socket.sid,receive_id,"turn around")
    }
    
    func onWarnThanks(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_thank_server",email,name,socket.sid,receive_id,"thank")
    }
    
    func onWarnReportOther(email: String, name: String, receive_id: String, msg: String) {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_warn_thank_socket",email,name,socket.sid,receive_id,msg)
    }
    
    func hello() {
        let socket = SocketIOManager1.manager.defaultSocket
        socket.emit("event_hello_server","123 ","1233",socket.sid,socket.sid,"hello")
    }
    
}
