//
//  Invitation.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/19/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct Invitation : Codable {
    let id, groupName: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case groupName
    }
}
