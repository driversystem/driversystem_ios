//
//  User.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/21/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct User : Codable {

    let role: String
    let authProvider: String?
    let providerUID: String?
//    let location: String?
//    let vehicle : [Vehicle]?
    let groups : [String]?
    let password: String?
//    let collections: [Collection]?
    let invitations: [String]?
    let id, name: String
    let email: String?
    let birthDate: String?
    let phoneNumber: String?
    let v: Int
    let socketID: String?
    var currentLocation: Geometry?
    let status: Bool?
    let distance: Double?
    let avatar:String?
    let socketIsActive: Bool?
    let invisible: Bool?
    let favouriteGroups: [String]?
    let pendingInvitations: [String]?
    let acceptedAppointments: [String]?

    enum CodingKeys: String, CodingKey {
        case role, authProvider, groups, invitations,providerUID
        case id = "_id"
        case password, favouriteGroups, pendingInvitations
        case email, name, birthDate, phoneNumber, socketIsActive, invisible
        case v = "__v"
        case socketID, currentLocation, status, distance, avatar, acceptedAppointments
    }
}

typealias ListUsers = [User]

