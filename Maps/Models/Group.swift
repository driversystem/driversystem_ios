//
//  Group.swift
//  Maps
//
//  Created by Phạm Viết Lực on 5/25/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct Group: Codable {
    var groupModerators: [Member]?
    var appointments: [Appointment]?
    var pending: [String]?
    let id: String?
    let groupOwner: Member?
    let v: Int?
    let groupName: String
    let description: String?
    var members:[Member]?
    var avatar: String?
    var photos: [String]?

    enum CodingKeys: String, CodingKey {
        case groupModerators, members, appointments, pending
        case id = "_id"
        case groupOwner
        case v = "__v"
        case groupName
        case description
        case avatar
        case photos
    }
}

typealias Groups = [Group]
