//
//  Appointment.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct Appointment: Codable {
    let id: String
//    let accepted: Members?
    let group : String?
    let time, reminderTime: String
    let reminderMsg: String?
    let location: Geometry?
    let locationName: String?
    let memberStatus: [String]?
    let properties: PropertiesOfAppointment?
    let locationAddress: String?
    let v: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case group, time, reminderTime, reminderMsg, location, memberStatus, locationName, locationAddress
        case v = "__v"
        case properties
    }
}
typealias Appointments = [Appointment]
