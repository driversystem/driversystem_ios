//
//  AppointmentSpecified.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct AppointmentSpecified: Codable {
    let id: String
    let accepted: Members?
    let group : Group?
    let time, reminderTime: String
    let reminderMsg: String?
    let location: Geometry?
    let locationName: String?
    let memberStatus: [String]?
    let locationAddress:String?
    let v: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case group, time, reminderTime, reminderMsg, location, memberStatus, locationName, accepted, locationAddress
        case v = "__v"
    }
}
