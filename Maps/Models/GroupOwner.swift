//
//  GroupOwner.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/26/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct GroupOwner: Codable {
    let name, id: String

    enum CodingKeys: String, CodingKey {
        case name
        case id = "_id"
    }
}
