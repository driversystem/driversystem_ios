//
//  PropertiesOfAppointment.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/10/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
 
struct PropertiesOfAppointment: Codable {
    var groupTravel: Bool
    enum CodingKeys: String, CodingKey {
        case groupTravel
    }
}
