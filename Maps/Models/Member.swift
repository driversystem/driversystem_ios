//
//  Member.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct Member: Codable {
    let name: String?
    let id: String
    let email: String?
    let confidenceScore: Double?
    let appointments: [String]?
    var avatar: String?
    var phoneNumber: String?

    enum CodingKeys: String, CodingKey {
        case name
        case id = "_id"
        case email
        case confidenceScore
        case appointments
        case avatar
        
    }
}
typealias Members = [Member]
