//
//  CurrentLocation.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/21/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation


// MARK: - CurrentLocation
struct Geometry: Codable {
    var coordinates: [Double]?
    var id, type: String?

    enum CodingKeys: String, CodingKey  {
        case coordinates
        case id = "_id"
        case type
    }
    func toString() -> String {
        
        let lat:String = String(format: "%.5f", self.coordinates![1])
        let lng:String = String(format: "%.5f", self.coordinates![0])
//        let jsoncoordinate:[String:[Double]] = [
//            "coordinates" : [self.coordinates![0],self.coordinates![1]]
//        ]
        let str = "{coordinates=[\(lng),\(lat)]&_id=\(id!)}"
        return str
    }
}
