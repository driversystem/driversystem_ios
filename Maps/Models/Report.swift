//
//  Report.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/25/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct Report: Codable {
    let id: String
    let byteImageFile, byteAudioFile, description, type : String?
    let geometry: Geometry
    let userID: String
    let numReport, numDelete: Int
    let status: Bool
    let phoneNumber: String?
    let v: Int
    let distance: Double?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type, geometry, userID, numReport, numDelete, status, phoneNumber
        case v = "__v"
        case byteImageFile, byteAudioFile
        case description = "description"
        case distance
    }
    
}
