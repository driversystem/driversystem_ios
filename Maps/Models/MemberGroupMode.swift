//
//  MemberGroupMode.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct MemberGroupMode : Codable {
    let name, id, socketID: String
    let currentLocation: Geometry
    let avatar: String

    enum CodingKeys: String, CodingKey {
        case name
        case id = "_id"
        case socketID, currentLocation, avatar
    }
}
