//
//  GroupTableViewCell.swift
//  Maps
//
//  Created by Phạm Viết Lực on 5/25/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameGroup: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
