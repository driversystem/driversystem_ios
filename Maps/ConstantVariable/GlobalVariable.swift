//
//  GlobalVariable.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/26/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
import GooglePlaces


let domainApi = "https://driversystemapp.herokuapp.com/api-user/v1/"

let domainApiUser = domainApi+"user/"

let domainApiUserUpdateCurrentLocation = domainApiUser+"location/current"

let myFindLocationKey = "com.bobthedeveloper.notificationKey1"

let myGroupModeKey = "myGroupModeKey"

var locationLat:Double?

var locationLng:Double?

var locationNameSpecified:String?

var locationAddress:String?

var locationDestination = CLLocationCoordinate2D()

var groupOfAppointment:String?

let myRemoveGroupKey = "com.bobthedeveloper.notificationKey"

let myJoinGroupKey = "com.bobthedeveloper.notificationKey"

var groupTravellingMode = false

let signInGoogleKey = "signInGoogleKey"
