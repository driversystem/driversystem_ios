//
//  ResponseGetUserNearly.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/30/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseGetUserNearly: Codable {
    let success: Bool
    let users: ListUsers
}
