//
//  ResponseGetDetailGroupSpecified.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/26/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct ResponseGetDetailGroupSpecified: Codable {
    let success: Bool
    let group: Group
}
