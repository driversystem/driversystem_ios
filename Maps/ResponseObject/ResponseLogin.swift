//
//  ResponseLogin.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/22/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseLogin: Codable {
    let success: Bool
    let message, token: String
    let user: User
}
