//
//  ResponseInviteMember.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/30/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct ResponseInviteMember: Codable {
    let success: Bool
    let message: String?
}
