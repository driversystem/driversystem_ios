//
//  InvalidRegister.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/22/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct IsInvalidRegister: Codable {
    let location, param, msg, value: String
}

typealias ListInvalidRegister = [IsInvalidRegister]
