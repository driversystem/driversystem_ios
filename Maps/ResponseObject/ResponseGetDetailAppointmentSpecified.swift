//
//  ResponseGetDetailAppointmentSpecified.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/7/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseGetDetailAppointmentSpecified : Codable {
    let success: Bool
    let appointment: AppointmentSpecified
}
