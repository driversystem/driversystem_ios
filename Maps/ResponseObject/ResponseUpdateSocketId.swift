//
//  ResponseUpdateSocketId.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/16/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseUpdateSocketId: Codable {
    let success: Bool
    let user: User
}
