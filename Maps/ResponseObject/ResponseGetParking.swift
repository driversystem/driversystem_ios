//
//  ResponseGetParking.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/20/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseGetParking: Codable {
    let htmlAttributions: [String]
    let results: [Place]
    let status: String

    enum CodingKeys: String, CodingKey {
        case htmlAttributions = "html_attributions"
        case results, status
    }
}

struct Place: Codable {
    let businessStatus: String
    let geometry: Geometry1
    let icon: String
    let name: String
//    let openingHours: OpeningHours?
    let placeID: String
//    let plusCode: String?
    let rating: Double?
    let reference, scope: String
    let types: [String]
    let userRatingsTotal: Int?
//    let vicinity: String

    enum CodingKeys: String, CodingKey {
        case businessStatus = "business_status"
        case geometry, icon, name
        case placeID = "place_id"
        case rating, reference, scope, types
        case userRatingsTotal = "user_ratings_total"
    }
}

struct Geometry1: Codable {
    let location: Location
    let viewport: Viewport
}

struct Location: Codable {
    let lat, lng: Double
}

struct Viewport: Codable {
    let northeast, southwest: Location
}
