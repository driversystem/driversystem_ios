//
//  ResponseGetAppointmentOfUser.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/1/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseGetAppointmentOfUser: Codable {
    let success: Bool
    let appointments: [Appointment]
}
