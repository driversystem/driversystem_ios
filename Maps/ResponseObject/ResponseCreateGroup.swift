//
//  ResponseCreateGroup.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/3/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseCreateGroup: Codable {
    let success: Bool
    let group: Group?
}
