//
//  InvalidLogin.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/22/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct InvalidLogin: Codable {
    let success: Bool
    let message: String
}
