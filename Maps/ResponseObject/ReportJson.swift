//
//  ReportJson.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/25/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation


struct ReportJson: Codable {
    let type: String
    let subtype1, subtype2: String
    let description: String
    let geometry: Geometry
    let userID: String
    let numReport, numDelete: Int
    let status: Bool
    let phoneNumber: String
    let byteImageFile, byteAudioFile : String


    enum CodingKeys: String, CodingKey {
        case type, geometry, userID, numReport, numDelete, status, phoneNumber
        case byteImageFile, byteAudioFile
        case description = "description"
        case subtype2, subtype1
    }

    func toString() -> String {
        print(self.type)
        let str = "type=\(self.type)&subtype1=\(self.subtype1)&subtype2=\(self.subtype2)&description=\(self.description)&userID=\(self.userID)&numReport=1&numDelete=1&status=true&phoneNumber=\(self.phoneNumber)&byteImageFile=\(self.byteImageFile)&byteAudioFile=\(self.byteAudioFile)&geometry=\(self.geometry.toString())"
        return str
    }

}
