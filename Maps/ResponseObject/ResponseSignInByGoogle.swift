//
//  ResponseSignInByGoogle.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/5/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseSignInByGoogle: Codable {
    let success: Bool
    let message: String
    let token: String
    let user: User?
}
