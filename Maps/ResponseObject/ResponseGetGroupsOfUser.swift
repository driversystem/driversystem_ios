//
//  ResponseGetGroupsOfUser.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/24/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct ResponseGetGroupOfUser: Codable {
    let success: Bool
    let groups: Groups
}


