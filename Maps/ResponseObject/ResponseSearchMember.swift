//
//  ResponseSearchMember.swift
//  Maps
//
//  Created by Phạm Viết Lực on 6/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
struct ResponseSearchMember: Codable {
    let success: Bool
    let users: Members
}
