//
//  ResponseReport.swift
//  Maps
//
//  Created by Phạm Viết Lực on 5/30/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseReport: Codable {
    let success: Bool
    let reports: [Report]
}
