//
//  ResponseGetMemberOfAppointment.swift
//  Maps
//
//  Created by Phạm Viết Lực on 7/28/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation

struct ResponseGetMemberOfAppointment: Codable {
    let success: Bool
    let members: [MemberGroupMode]
}
