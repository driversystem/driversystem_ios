//
//  SaveObject.swift
//  Maps
//
//  Created by Phạm Viết Lực on 4/21/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
import UserNotifications

func saveUserToUserDefault(User: User) {
    let defaults = UserDefaults.standard
    
    // Use PropertyListEncoder to encode User
    defaults.set(try? PropertyListEncoder().encode(User), forKey: "User")
}
func loadUserFromUserDefault() -> User? {
    let defaults = UserDefaults.standard
    guard let userData = defaults.object(forKey: "User") as? Data else {
        return nil
    }
    
    // Use PropertyListDecoder to convert Data into Player
    guard let user = try? PropertyListDecoder().decode(User.self, from: userData) else {
        return nil
    }
    return user
}

extension UserDefaults {
    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
        guard let data = self.value(forKey: key) as? Data else { return nil }
        return try? decoder.decode(type.self, from: data)
    }

    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
        let data = try? encoder.encode(object)
        self.set(data, forKey: key)
    }
}

func saveAppointmentToUserDefault(appointment: Appointment, appointmentId: String, time: String,name: String) {
    let defaults = UserDefaults.standard
    
//    let type:[String:Appointment] = [:]
    
    let listAppointData = defaults.object(forKey: "ListAppointment") as? Data
    if(listAppointData == nil) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 7)
        let datetime = formatter.date(from: time)
        
        let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)
        
        var appointTime = DateComponents()
        appointTime.year = day.year
        appointTime.month = day.month
        appointTime.day = day.day
        appointTime.timeZone = TimeZone(secondsFromGMT: 7)
        appointTime.hour = day.hour
        appointTime.minute = day.minute
        let timeApp = Calendar.current.date(from: appointTime)!
        
        let now = NSDate()
        let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
        
        var nowTime = DateComponents()
        nowTime.year = nowD.year
        nowTime.month = nowD.month
        nowTime.day = nowD.day
        nowTime.timeZone = TimeZone(secondsFromGMT: 7)
        nowTime.hour = nowD.hour
        nowTime.minute = nowD.minute
        let nowTimeDate = Calendar.current.date(from: nowTime)!
        let distanceTime = nowTimeDate.distance(to: timeApp)
        if(distanceTime > 0) {
            let center = UNUserNotificationCenter.current()
            
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            }
            
            let content1 = UNMutableNotificationContent()
            content1.title = "Lời nhắc lịch hẹn"
            
            let stringBoby = "Bạn có lịch hẹn ở nhóm vào lúc "+String(day.hour!)+":"+String(day.minute!)
            content1.body = stringBoby
            
            
            let date = Date().addingTimeInterval(distanceTime)
            let dateCom = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
            
            
            let trigger1 = UNCalendarNotificationTrigger(dateMatching: dateCom , repeats: false)
            let uuidString = UUID().uuidString
            let request = UNNotificationRequest(identifier: uuidString, content: content1, trigger: trigger1)
            
            center.add(request) { (error) in
            }
            defaults.set(try? PropertyListEncoder().encode([appointmentId:appointment]), forKey: "ListAppointment")
        }
    }
    else {
        var listAppoint = try? PropertyListDecoder().decode([String : Appointment].self, from: listAppointData!)
        let isExist = listAppoint![appointmentId]
        if(isExist == nil) {
                        
                       
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 7)
            let datetime = formatter.date(from: time)
            
            let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)
            
            var appointTime = DateComponents()
            appointTime.year = day.year
            appointTime.month = day.month
            appointTime.day = day.day
            appointTime.timeZone = TimeZone(secondsFromGMT: 7)
            appointTime.hour = day.hour
            appointTime.minute = day.minute
            let timeApp = Calendar.current.date(from: appointTime)!

            
//            print(stringBoby)
//            print(time)
            
            let now = NSDate()
            let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
            
            var nowTime = DateComponents()
            nowTime.year = nowD.year
            nowTime.month = nowD.month
            nowTime.day = nowD.day
            nowTime.timeZone = TimeZone(secondsFromGMT: 7)
            nowTime.hour = nowD.hour
            nowTime.minute = nowD.minute
            let nowTimeDate = Calendar.current.date(from: nowTime)!
            let distanceTime = nowTimeDate.distance(to: timeApp)
            if(distanceTime > 0) {
                print("lich hen dc set la")
                print(appointment)
                print("lich chua set")
                let center = UNUserNotificationCenter.current()
                
                center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                }
                
                let content1 = UNMutableNotificationContent()
                content1.title = "Lời nhắc lịch hẹn"
                
                let stringBoby = "Bạn có lịch hẹn ở nhóm vào lúc "+String(day.hour!)+":"+String(day.minute!)
                content1.body = stringBoby
                
                
                let date = Date().addingTimeInterval(distanceTime)
                let dateCom = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
                
                
                let trigger1 = UNCalendarNotificationTrigger(dateMatching: dateCom , repeats: false)
                let uuidString = UUID().uuidString
                let request = UNNotificationRequest(identifier: uuidString, content: content1, trigger: trigger1)
                
                center.add(request) { (error) in
                }
                listAppoint!.updateValue(appointment, forKey: appointmentId)
                defaults.set(try? PropertyListEncoder().encode(listAppoint), forKey: "ListAppointment")
            }
            else {
                
            }
        }
        else {
            defaults.set(try? PropertyListEncoder().encode(listAppoint), forKey: "ListAppointment")
        }
    }
    
//    let listApp = defaults.array(forKey: "ListAppointment")
//    if (listAppoint!.count == 0) {
//        defaults.set(try? PropertyListEncoder().encode([appointmentId:appointment]), forKey: "ListAppointment")
//    } else {
////        var listAppoint = defaults.array(forKey: "ListAppointment") as! Appointments
////        listAppoint.append(appointment)
////        listAppoint.updateValue(appointment, forKey: appointmentId)
//        let isExist = listAppoint![appointmentId]
//        if(isExist == nil) {
//            listAppoint!.updateValue(appointment, forKey: appointmentId)
//            defaults.set(try? PropertyListEncoder().encode(listAppoint), forKey: "ListAppointment")
//        }
//        else {
//            defaults.set(try? PropertyListEncoder().encode(listAppoint), forKey: "ListAppointment")
//        }
////        if(listAppoint[appointmentId])
//    }
}

func loadAppointmentToUserDefault() -> [String : Appointment]? {
    let defaults = UserDefaults.standard
    guard let listAppointData = defaults.object(forKey: "ListAppointment") as? Data else {
        return nil
    }
    
    // Use PropertyListDecoder to convert Data into Player
    guard let listAppoint = try? PropertyListDecoder().decode([String : Appointment].self, from: listAppointData) else {
        return nil
    }
    return listAppoint
}



func saveListUserNearlyToUserDefault(ListUser: ListUsers) {
    let defaults = UserDefaults.standard
    
    // Use PropertyListEncoder to encode User
    defaults.set(try? PropertyListEncoder().encode(ListUser), forKey: "ListUserNearly")
}
func loadListUserNearlyFromUserDefault() -> [User]? {
    let defaults = UserDefaults.standard
    guard let userData = defaults.object(forKey: "ListUserNearly") as? Data else {
        return nil
    }
    
    // Use PropertyListDecoder to convert Data into Player
    guard let user = try? PropertyListDecoder().decode(ListUsers.self, from: userData) else {
        return nil
    }
    return user
}

func saveUserTokenToUserDefault(token: String) {
    let defaults = UserDefaults.standard
    
    // Use PropertyListEncoder to encode User
    defaults.set(token,forKey: "User-Token")
}
func loadUserTokenFromUserDefault() -> String? {
    let defaults = UserDefaults.standard
    
    if let token = defaults.string(forKey: "User-Token") {
        return token
    }
    
    return nil
}

func findTheNearlestAppointmentTravel() -> [TimeInterval : Appointment?]? {
    let appointmentDict = loadAppointmentToUserDefault()
    if(appointmentDict==nil) {
        return nil
    }
    else {
        var nearlest:Appointment?
        
        let now = NSDate()
        let nowD = Calendar.current.dateComponents(in: TimeZone(abbreviation: "ICT")!, from: now as Date)
        
        var nowTime = DateComponents()
        nowTime.year = nowD.year
        nowTime.month = nowD.month
        nowTime.day = nowD.day
        nowTime.timeZone = TimeZone(secondsFromGMT: 7)
        nowTime.hour = nowD.hour
        nowTime.minute = nowD.minute
        let nowTimeDate = Calendar.current.date(from: nowTime)!
        var minDistanceTime = 0
        var index = 0
        for (id,appoiment) in appointmentDict! {
            if(index > 0) {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                formatter.timeZone = TimeZone(secondsFromGMT: 7)
                let datetime = formatter.date(from: appoiment.time)
                let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)
                var appointTime = DateComponents()
                appointTime.year = day.year
                appointTime.month = day.month
                appointTime.day = day.day
                appointTime.timeZone = TimeZone(secondsFromGMT: 7)
                appointTime.hour = day.hour
                appointTime.minute = day.minute
                let timeApp = Calendar.current.date(from: appointTime)!
                let distanceTime = nowTimeDate.distance(to: timeApp)
                if(distanceTime > 0 && appoiment.properties?.groupTravel == true) {
                    if(Int(distanceTime) < minDistanceTime) {
                        nearlest = appoiment
                        minDistanceTime = Int(distanceTime)
                    }
                }
            }
            else {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                formatter.timeZone = TimeZone(secondsFromGMT: 7)
                let datetime = formatter.date(from: appoiment.time)
                let day = Calendar.current.dateComponents(in: TimeZone(secondsFromGMT: 7)!, from: datetime!)
                var appointTime = DateComponents()
                appointTime.year = day.year
                appointTime.month = day.month
                appointTime.day = day.day
                appointTime.timeZone = TimeZone(secondsFromGMT: 7)
                appointTime.hour = day.hour
                appointTime.minute = day.minute
                let timeApp = Calendar.current.date(from: appointTime)!
                let distanceTime = nowTimeDate.distance(to: timeApp)
                if(distanceTime > 0 && appoiment.properties?.groupTravel == true) {
                    nearlest = appoiment
                    index += 1
                    minDistanceTime = Int(distanceTime)
                }
            }

            
        }
        if nearlest == nil {
            return nil
        }
        else {
            return [Double(minDistanceTime) : nearlest]
        }
    }
}

//func saveTypeOfReportToUserDefault(typeOfReport: TypeOfReport) {
//    let defaults = UserDefaults.standard
//    
//    defaults.set(try? PropertyListEncoder().encode(typeOfReport), forKey: "Report")
//}
//func loadTypeOfReportToUserDefault()-> TypeOfReport? {
//    let defaults = UserDefaults.standard
//    guard let reportData = defaults.object(forKey: "Report") as? Data else {
//        return nil
//    }
//    
//    // Use PropertyListDecoder to convert Data into Player
//    guard let report = try? PropertyListDecoder().decode(TypeOfReport.self, from: reportData) else {
//        return nil
//    }
//    return report
//    
//}
