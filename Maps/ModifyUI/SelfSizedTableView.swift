//
//  SelfSizedTableView.swift
//  Maps
//
//  Created by Phạm Viết Lực on 8/6/20.
//  Copyright © 2020 Phạm Viết Lực. All rights reserved.
//

import Foundation
import UIKit

class SelfSizedTableView: UITableView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
    super.reloadData()
    self.invalidateIntrinsicContentSize()
    self.layoutIfNeeded()
  }
  
  override var intrinsicContentSize: CGSize {
    let height = min(contentSize.height, maxHeight)
    return CGSize(width: contentSize.width, height: height)
  }
}
